#include "gtest/gtest.h"
#include "DynamicArrays.h"

template <typename T>
class FooTest : public testing::Test 
{
public:
	void SetUp() override
	{
		//value_ = 5;
	}

	using Array = THArray<T>;
	static T shared_;
	T value_;
};

using MyTypes = ::testing::Types<char, int, long, double, std::string, THArray<int> >;
TYPED_TEST_CASE(FooTest, MyTypes);

TYPED_TEST(FooTest, DoesBlah) 
{
	// Inside a test, refer to the special name TypeParam to get the type
	// parameter.  Since we are inside a derived class template, C++ requires
	// us to visit the members of FooTest via 'this'.
	TypeParam n = this->value_;

	// To visit static members of the fixture, add the 'TestFixture::'
	// prefix.
	//n += TestFixture::shared_;

	// To refer to typedefs in the fixture, add the 'typename TestFixture::'
	// prefix.  The 'typename' is required to satisfy the compiler.
	typename TestFixture::Array arr;

	arr.AddValue(n);
	EXPECT_EQ(arr[0], n);
	
	//n = n + n;
	arr.AddValue(n);
	EXPECT_EQ(arr[1], n);
	
}
