#include "gtest/gtest.h"
#include <vector>
#include "DynamicArrays.h"

typedef std::vector<int> TestingType;
typedef THArraySorted<TestingType> THTestArray;

TEST(THArraySortedVector, EmptyArray1)
{
	THTestArray arr;

	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
	EXPECT_NO_THROW(arr.Clear());
	EXPECT_NO_THROW(arr.ClearMem());
	EXPECT_NO_THROW(arr.Hold());
}

TEST(THArraySortedVector, EmptyArray2)
{
	THTestArray arr;

	EXPECT_THROW(arr.GetValue(0), THArrayException);
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(1000), THArrayException);
	EXPECT_THROW(arr.DeleteValue(0), THArrayException);
	EXPECT_THROW(arr.DeleteValue(1), THArrayException);
	EXPECT_THROW(arr.DeleteValue(888), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(0), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(555), THArrayException);
}

TEST(THArraySortedVector, EmptyIndexOf)
{
	THTestArray arr;
	TestingType item1;
	TestingType item2;
	EXPECT_EQ(arr.IndexOf(item1), -1);
	EXPECT_EQ(arr.IndexOf(item2), -1);
	EXPECT_EQ(arr.IndexOfFrom(item1, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(item2, 0), -1);

	item1.push_back(0);
	EXPECT_EQ(arr.IndexOf(item1), -1);
	item1.push_back(1234);
	EXPECT_EQ(arr.IndexOf(item1), -1);

	item2.push_back(666);
	EXPECT_EQ(arr.IndexOfFrom(item1, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(item2, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(item1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(item2, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(item1, 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(item2, 55), -1);

	item1.push_back(444);

	EXPECT_EQ(arr.IndexOfFrom(item1, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(item1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(item1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(item1, 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(item2, 55), -1);
}

TEST(THArraySortedVector, AddValue)
{
	THTestArray arr;
	TestingType item1 = { 3 };

	EXPECT_EQ(arr.AddValue(item1), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), item1);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, item1);
	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(11), THArrayException);
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	TestingType item2 = { 3 };

	EXPECT_EQ(arr.IndexOf(item1), 0);
	EXPECT_EQ(arr.IndexOf(item2), 0);

	TestingType item3;
	EXPECT_EQ(arr.IndexOf(item3), -1);
	item2.push_back(1);
	EXPECT_EQ(arr.IndexOf(item2), -1);
	
	item2.clear();
	item2.push_back(3);
	EXPECT_EQ(arr.IndexOf(item2), 0);
	item2.push_back(3);
	EXPECT_EQ(arr.IndexOf(item2), -1);

	item1.clear();
	EXPECT_THROW(arr.IndexOfFrom(item1, 1), THArrayException); // exception because Start=1 is out of bounds.
	EXPECT_THROW(arr.IndexOfFrom(item2, 2), THArrayException);
}


TEST(THArraySortedVector, AddValue2)
{
	THTestArray arr;
	TestingType v1;
	TestingType v2;

	EXPECT_EQ(arr.Add(&v1), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_EQ(arr.GetValue(0), v2);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, v1);
	EXPECT_EQ(*vv, v2);

	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(2), THArrayException);

	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);
	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.IndexOf(v2), 0);
	EXPECT_EQ(arr.IndexOf(TestingType()), 0);
		
	EXPECT_EQ(arr.IndexOfFrom(v1, 0), 0);
	EXPECT_THROW(arr.IndexOfFrom(v1, 1), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v1, 2), THArrayException);
	EXPECT_EQ(arr.IndexOfFrom(v2, 0), 0);
	EXPECT_THROW(arr.IndexOfFrom(v2, 1), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v2, 2), THArrayException);
}

TEST(THArraySortedVector, AddValue2_1)
{
	THTestArray arr;
	TestingType v1 = { 1 };
	TestingType v2 = { 2 };

	EXPECT_EQ(arr.Add(&v1), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_NE(arr.GetValue(0), v2);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, v1);
	EXPECT_NE(*vv, v2);

	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(2), THArrayException);
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.IndexOf(v2), -1);
	EXPECT_EQ(arr.IndexOf(TestingType(v1)), 0);
	EXPECT_EQ(arr.IndexOf(TestingType()), -1);
	EXPECT_EQ(arr.IndexOf(TestingType(v2)), -1);
	
	TestingType v3 = { 1 };
	EXPECT_EQ(arr.IndexOf(v3), 0);

	EXPECT_EQ(arr.IndexOfFrom(v1, 0), 0);
	EXPECT_THROW(arr.IndexOfFrom(v1, 1), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v1, 2), THArrayException);
	EXPECT_EQ(arr.IndexOfFrom(v2, 0), -1);
	EXPECT_THROW(arr.IndexOfFrom(v2, 1), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v2, 2), THArrayException);
}

TEST(THArraySortedVector, AddValue3)
{
	THTestArray arr;

	TestingType v1 = { 1 }, v2 = { 2 };

	EXPECT_EQ(arr.AddValue(v1), 0u);
	EXPECT_EQ(arr.AddValue(v2), 1u);
	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_EQ(arr.GetValue(1), v2);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, v1);
	vv = arr.GetValuePointer(1);
	EXPECT_EQ(*vv, v2);
	EXPECT_THROW(arr.GetValuePointer(2), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(6), THArrayException);
	EXPECT_THROW(arr.GetValue(2), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.IndexOf(v2), 1);
	
	TestingType v3;
	EXPECT_EQ(arr.IndexOf(v3), -1);
	v3.push_back(99);
	EXPECT_EQ(arr.IndexOf(v3), -1);
	v3.push_back(22);
	EXPECT_EQ(arr.IndexOf(v3), -1);

	EXPECT_EQ(arr.IndexOfFrom(v1, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(v1, 1), -1);
	EXPECT_THROW(arr.IndexOfFrom(v1, 2), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v1, 3), THArrayException);
	EXPECT_EQ(arr.IndexOfFrom(v2, 0), 1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 1), 1);
	EXPECT_THROW(arr.IndexOfFrom(v2, 2), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v2, 99), THArrayException);
}

TEST(THArraySortedVector, AddValue3_1)
{
	THTestArray arr;

	TestingType v1 = { 1,2,3 }, v2 = { 1,2,3 }, v3 = { 1,2 }, v4 = { 3,2,1 }, v5 = {1,98,100};

	EXPECT_EQ(arr.AddValue(v1), 0u);
	EXPECT_EQ(arr.AddValue(v5), 1u);

	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_EQ(arr.GetValue(1), v5);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, v1);
	vv = arr.GetValuePointer(1);
	EXPECT_EQ(*vv, v5);

	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.IndexOf(v2), 0);
	EXPECT_EQ(arr.IndexOf(v3), -1);
	EXPECT_EQ(arr.IndexOf(v4), -1);
	EXPECT_EQ(arr.IndexOf(v5), 1);

	TestingType v6;
	EXPECT_EQ(arr.IndexOf(v6), -1);
	v6.push_back(1);
	EXPECT_EQ(arr.IndexOf(v6), -1);
	v6.push_back(2);
	EXPECT_EQ(arr.IndexOf(v6), -1);
	v6.push_back(3);
	EXPECT_EQ(arr.IndexOf(v6), 0);
}

TEST(THArraySortedVector, AddValue4)
{
	THTestArray arr;
	TestingType v1;
	TestingType v2 = { 1 };
	TestingType v3 = { 2,3 };

	EXPECT_EQ(arr.Add(&v2), 0u);
	EXPECT_EQ(arr.Add(&v1), 0u);
	EXPECT_EQ(arr.Add(&v3), 2u);

	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);

	EXPECT_EQ(arr.GetValue(2), v3);
	EXPECT_EQ(arr.GetValue(1), v2);
	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_THROW(arr.GetValue(3), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.IndexOf(v2), 1);
	EXPECT_EQ(arr.IndexOf(v3), 2);

	TestingType v4;

	EXPECT_EQ(arr.IndexOf(v4), 0);	
	v4.push_back(1);
	EXPECT_EQ(arr.IndexOf(v4), 1);
	v4.push_back(2);
	EXPECT_EQ(arr.IndexOf(v4), -1);
	v4.erase(v4.begin());
	v4.push_back(3);
	EXPECT_EQ(arr.IndexOf(v4), 2);

	v4.clear();
	v4.push_back(3);
	EXPECT_EQ(arr.IndexOf(v4), -1);

	v4.clear();
	v4.push_back(2);
	v4.push_back(3);
	EXPECT_EQ(arr.IndexOf(v4), 2);

	EXPECT_EQ(arr.IndexOfFrom(v1, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(v1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(v1, 2), -1);
	EXPECT_THROW(arr.IndexOfFrom(v1, 3), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v1, 4), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(v2, 0), 1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 1), 1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 2), -1);
	EXPECT_THROW(arr.IndexOfFrom(v2, 3), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v2, 4), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(v3, 0), 2);
	EXPECT_EQ(arr.IndexOfFrom(v3, 1), 2);
	EXPECT_EQ(arr.IndexOfFrom(v3, 2), 2);
	EXPECT_THROW(arr.IndexOfFrom(v3, 3), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v3, 4), THArrayException);
}

TEST(THArraySortedVector, AddValue5)
{
	THTestArray arr;
	TestingType v1 = { 1,2 }, v2 = {1,3};

	EXPECT_EQ(arr.AddValue(v1), 0u);
	EXPECT_EQ(arr.AddValue(v2), 1u);
	EXPECT_EQ(arr[0], v1);
	EXPECT_EQ(arr[1], v2);
	EXPECT_THROW(arr[3], THArrayException);
	EXPECT_THROW(arr[88], THArrayException);

	arr.Clear();
	EXPECT_EQ(arr.AddValue(v2), 0u);
	EXPECT_EQ(arr.AddValue(v1), 0u);
	EXPECT_EQ(arr[0], v1);
	EXPECT_EQ(arr[1], v2);
	EXPECT_THROW(arr[3], THArrayException);
	EXPECT_THROW(arr[88], THArrayException);
}


TEST(THArraySortedVector, AddValue6)
{
	THTestArray arr;
	TestingType v1 = { 3 }, v2 = { 99 }, v3 = { 2 }, v4 = {3};

	EXPECT_EQ(arr.AddValue(v1), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.Capacity(), 4u);
	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.GetValue(0), v1);

	EXPECT_EQ(arr.AddValue(v2), 1u);
	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_EQ(arr.GetValue(1), v2);

	EXPECT_EQ(arr.AddValue(v3), 0u);
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr.GetValue(0), v3);
	EXPECT_EQ(arr.GetValue(1), v1);
	EXPECT_EQ(arr.GetValue(2), v2);

	EXPECT_EQ(arr.AddValue(v4), 1u);
	EXPECT_EQ(arr.Count(), 4u);
	EXPECT_EQ(arr.GetValue(0), v3);
	EXPECT_EQ(arr.GetValue(1), v4);
	EXPECT_EQ(arr.GetValue(2), v4);
	EXPECT_EQ(arr.GetValue(3), v2);


	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, v3);
	vv = arr.GetValuePointer(1);
	EXPECT_EQ(*vv, v1);
	vv = arr.GetValuePointer(2);
	EXPECT_EQ(*vv, v1);
	vv = arr.GetValuePointer(3);
	EXPECT_EQ(*vv, v2);

	EXPECT_THROW(arr.GetValuePointer(4), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(99), THArrayException);

	EXPECT_THROW(arr.GetValue(4), THArrayException);
	EXPECT_THROW(arr.GetValue(5), THArrayException);

	TestingType v11, v12 = { 1 }, v13 = { 4 }, v14 = { 2,2 }, v15 = { 3,3 }, v16 = { 2,3 }, v17 = { 3,2 }, v18 = { 3,99,2 }, v19 = { 3,99 };
	EXPECT_EQ(arr.IndexOf(v2), 3);
	EXPECT_EQ(arr.IndexOf(v11), -1);
	EXPECT_EQ(arr.IndexOf(v12), -1);
	EXPECT_EQ(arr.IndexOf(v3), 0);
	EXPECT_EQ(arr.IndexOf(v13), -1);
	EXPECT_EQ(arr.IndexOf(v1), 2);
	EXPECT_EQ(arr.IndexOf(v4), 2);
	EXPECT_EQ(arr.IndexOf(v14), -1);
	EXPECT_EQ(arr.IndexOf(v15), -1);
	EXPECT_EQ(arr.IndexOf(v16), -1);
	EXPECT_EQ(arr.IndexOf(v17), -1);
	EXPECT_EQ(arr.IndexOf(v18), -1);
	EXPECT_EQ(arr.IndexOf(v19), -1);

	EXPECT_EQ(arr.IndexOfFrom(v11, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(v11, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(v11, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(v11, 3), -1);
	EXPECT_THROW(arr.IndexOfFrom(v11, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v11, 11), THArrayException);


	EXPECT_EQ(arr.IndexOfFrom(v18, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(v18, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(v18, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(v18, 3), -1);
	EXPECT_THROW(arr.IndexOfFrom(v18, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v18, 11), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(v3, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(v3, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(v3, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(v3, 3), -1);
	EXPECT_THROW(arr.IndexOfFrom(v3, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v3, 5), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(v1, 0), 2);
	EXPECT_EQ(arr.IndexOfFrom(v1, 1), 2);
	EXPECT_EQ(arr.IndexOfFrom(v1, 2), 2);
	EXPECT_EQ(arr.IndexOfFrom(v1, 3), -1);
	EXPECT_THROW(arr.IndexOfFrom(v1, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v1, 5), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(v4, 0), 2);
	EXPECT_EQ(arr.IndexOfFrom(v4, 1), 2);
	EXPECT_EQ(arr.IndexOfFrom(v4, 2), 2);
	EXPECT_EQ(arr.IndexOfFrom(v4, 3), -1);
	EXPECT_THROW(arr.IndexOfFrom(v4, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v4, 5), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(v2, 0), 3);
	EXPECT_EQ(arr.IndexOfFrom(v2, 1), 3);
	EXPECT_EQ(arr.IndexOfFrom(v2, 2), 3);
	EXPECT_EQ(arr.IndexOfFrom(v2, 3), 3);
	EXPECT_THROW(arr.IndexOfFrom(v2, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v2, 5), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v2, 99), THArrayException);
}


TEST(THArraySortedVector, ArayCopy)
{
	THTestArray arr1, arr2;
	TestingType item1 = { 1 };
	TestingType item2 = { 2 };
	TestingType item3 = { 2, 3 };
	TestingType item4 = { 4,5,6,7,8,9,0 };

	arr2.AddValue(item3);
	arr2.AddValue(item4);

	arr1 = arr2;

	EXPECT_EQ(arr1.Count(), 2u);
	EXPECT_EQ(arr1.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr1.Capacity(), 2u);
	//EXPECT_NE(arr1.Memory(), nullptr);

	EXPECT_EQ(arr2.Count(), 2u);
	EXPECT_EQ(arr2.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr2.Capacity(), 4u);
	//EXPECT_NE(arr2.Memory(), nullptr);

	EXPECT_EQ(arr1[0], item3);
	EXPECT_EQ(arr1[1], item4);
	EXPECT_THROW(arr1[3], THArrayException);
	EXPECT_THROW(arr1[4], THArrayException);
	EXPECT_THROW(arr1[5], THArrayException);

	EXPECT_EQ(arr2[0], item3);
	EXPECT_EQ(arr2[1], item4);
	EXPECT_THROW(arr2[3], THArrayException);
	EXPECT_THROW(arr2[4], THArrayException);
	EXPECT_THROW(arr2[5], THArrayException);
	
	THTestArray arr3(arr1);
	EXPECT_EQ(arr3.Count(), 2u);
	EXPECT_EQ(arr3.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr3.Capacity(), 2u);
	//EXPECT_NE(arr3.Memory(), nullptr);
	EXPECT_EQ(arr3[0], item3);
	EXPECT_EQ(arr3[1], item4);
	EXPECT_THROW(arr3[3], THArrayException);
	EXPECT_THROW(arr3[4], THArrayException);
	EXPECT_THROW(arr3[5], THArrayException);
}

TEST(THArraySortedVector, Capacity1)
{
	THTestArray arr;

	arr.SetCapacity(500);
	EXPECT_EQ(arr.Capacity(), 500u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(600);
	EXPECT_EQ(arr.Capacity(), 600u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(400);
	EXPECT_EQ(arr.Capacity(), 400u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(0);
	EXPECT_EQ(arr.Capacity(), 0u);
	EXPECT_EQ(arr.Count(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);

	//int v = 2'000'000'000;
	//arr.SetCapacity(v); // try to allocate 2G memory
	//EXPECT_EQ(arr.Capacity(), v);
	//EXPECT_EQ(arr.Count(), 0);

}

TEST(THArraySortedVector, Capacity2)
{
	THTestArray arr;

	for (int i = 0; i < 500; i++)
	{
		TestingType item = { i };
		arr.AddValue(item);
	}

	EXPECT_GT(arr.Capacity(), 500u);

	arr.Hold();
	EXPECT_EQ(arr.Capacity(), 500u);

	arr.SetCapacity(600);
	EXPECT_EQ(arr.Capacity(), 600u);
	EXPECT_EQ(arr.Count(), 500u);
	arr.SetCapacity(400);
	EXPECT_EQ(arr.Capacity(), 400u);
	EXPECT_EQ(arr.Count(), 400u);
	arr.SetCapacity(0);
	EXPECT_EQ(arr.Capacity(), 0u);
	EXPECT_EQ(arr.Count(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
}

TEST(THArraySortedVector, Grow)
{
	THTestArray arr;
	TestingType item;
	EXPECT_EQ(arr.Capacity(), 0u);
	arr.AddValue(item);
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue(item);
	arr.AddValue(item);
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue(item);
	arr.AddValue(item);
	EXPECT_EQ(arr.Capacity(), 8u);
	arr.AddValue(item);
	arr.AddValue(item);
	arr.AddValue(item);
	EXPECT_EQ(arr.Capacity(), 8u);
	arr.AddValue(item);
	EXPECT_EQ(arr.Capacity(), 12u);
	EXPECT_EQ(arr.Count(), 9u);

	for (int i = 0; i < 4; i++)
	{
		TestingType item2 = { i };
		arr.AddValue(item2);
	}
	EXPECT_EQ(arr.Capacity(), 28u);

	for (int i = 0; i < 16; i++)
	{
		TestingType item3 = { i };
		arr.AddValue(item3);
	}

	EXPECT_EQ(arr.Count(), 29u);
	EXPECT_EQ(arr.Capacity(), 44u);

	for (int i = 0; i < 16; i++)
	{
		TestingType item4 = { i };
		arr.AddValue(item4);
	}

	EXPECT_EQ(arr.Count(), 45u);
	EXPECT_EQ(arr.Capacity(), 60u);

	for (int i = 0; i < 16; i++)
	{
		TestingType item5 = { i };
		arr.AddValue(item5);
	}

	EXPECT_EQ(arr.Count(), 61u);
	EXPECT_EQ(arr.Capacity(), 76u);

	for (int i = 0; i < 16; i++)
	{
		TestingType item6 = { i };
		arr.AddValue(item6);
	}

	EXPECT_EQ(arr.Count(), 77u);
	EXPECT_EQ(arr.Capacity(), 95u);

	for (int i = 0; i < 19; i++)
	{
		TestingType item7 = { i };
		arr.AddValue(item7);
	}

	EXPECT_EQ(arr.Count(), 96u);
	EXPECT_EQ(arr.Capacity(), 118u);

	arr.Clear();
	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.Capacity(), 118u);

	arr.GrowTo(100);
	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.Capacity(), 118u);
}


TEST(THArraySortedVector, Zero)
{
	THTestArray arr;

	for (int i = 2; i < 35; i++)
	{
		TestingType item = { i };
		arr.AddValue(item);
	}

	EXPECT_EQ(arr.Count(), 33u);

	for (int i = 0; i < (int)arr.Count(); i++)
	{
		TestingType item = { i + 2 };
		EXPECT_EQ(arr[i], item);
	}

	arr.Zero();

	EXPECT_EQ(arr.Count(), 33u);

	for (uint i = 0; i < arr.Count(); i++)
	{
		TestingType item;
		EXPECT_EQ(arr[i], item);
	}
}


TEST(THArraySortedVector, InsertValue1)
{
	THTestArray arr;

	for (int i = 0; i < 100; i++)
	{

		arr.AddValue({ i + 1000 });
	}

	EXPECT_EQ(arr.Count(), 100u);

	for (int i = 0; i < 100; i++)
	{
		TestingType item = { i + 1000 };
		EXPECT_EQ(arr[i], item);
	}
}

TEST(THArraySortedVector, InsertValue2)
{
	THTestArray arr;

	for (int i = 0; i < 100; i++)
	{
		arr.AddValue({ 1000 - i });
	}

	EXPECT_EQ(arr.Count(), 100u);

	for (int i = 0; i < 100; i++)
	{
		TestingType item = { 1000 - 99 + i };
		EXPECT_EQ(arr[i], item);
	}
}


TEST(THArraySortedVector, DeleteValue1)
{
	THTestArray arr;

	for (int i = 0; i < 10; i++)
	{
		arr.AddValue({ i + 1 });
	}

	EXPECT_EQ(arr.Count(), 10u);
	arr.DeleteValue(0);
	EXPECT_EQ(arr.Count(), 9u);
	for (int i = 0; i < 9; i++)
	{
		TestingType item = { i + 2 };
		EXPECT_EQ(arr[i], item);
	}

	EXPECT_THROW(arr.DeleteValue(arr.Count()), THArrayException); // try to delete out of range element
	EXPECT_EQ(arr.Count(), 9u);

	arr.DeleteValue(arr.Count() - 1); //try to delete last element in the array
	EXPECT_EQ(arr.Count(), 8u);
	for (int i = 0; i < 8; i++)
	{
		TestingType item = { i + 2 };
		EXPECT_EQ(arr[i], item);
	}
}


