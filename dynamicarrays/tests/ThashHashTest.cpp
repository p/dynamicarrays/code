#include "gtest/gtest.h"
#include <vector>
#include "DynamicArrays.h"

typedef unsigned long TestingType1;
typedef THash<unsigned long, unsigned long> TestingType2;
typedef THash<TestingType1, TestingType2> TestHash;

TEST(THashHash, EmptyHash1)
{
	TestHash hash;

	EXPECT_EQ(hash.Count(), 0u);
	EXPECT_EQ(hash.GetKeys().Count(), 0u);
	EXPECT_EQ(hash.GetValues().Count(), 0u);
	//EXPECT_NE(hash.GetKeys(), nullptr);
	//EXPECT_NE(hash.GetValues(), nullptr);
	EXPECT_NO_THROW(hash.Clear());
	EXPECT_NO_THROW(hash.ClearMem());
}

TEST(THashHash, EmptyHash2)
{
	TestHash hash;

	EXPECT_THROW(hash.GetKey(0), THArrayException);
	EXPECT_THROW(hash.GetKey(1), THArrayException);
	EXPECT_THROW(hash.GetValue(3), THArrayException);
	EXPECT_THROW(hash.GetValue(1000), THArrayException);
	EXPECT_NO_THROW(hash.Delete(3));
	EXPECT_NO_THROW(hash.Delete(888));
	EXPECT_EQ(hash.GetValuePointer(0), nullptr);
	EXPECT_EQ(hash.GetValuePointer(44), nullptr);
}

TEST(THashHash, EmptyIndexOf)
{
	TestHash hash;

	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(1), false);
	EXPECT_EQ(hash.IfExists(1234), false);
}

TEST(THashHash, SetValue)
{
	TestHash hash;
	TestingType2 v;
	hash.SetValue(1, v);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(2), false);

	EXPECT_EQ(hash.GetValue(1), v);
	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);

	TestingType2 vv;
	vv.SetValue(99, 99);
	hash.SetValue(1111111, vv);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(2), false);
	EXPECT_EQ(hash.IfExists(111111), false);
	EXPECT_EQ(hash.IfExists(1111111), true);
	EXPECT_EQ(hash.GetValue(1), v);
	EXPECT_EQ(hash.GetValue(1111111), vv);
	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);

	EXPECT_EQ(hash.Count(), 2u);

	TestingType2 vvv;
	vvv.SetValue(55, 55);
	hash.SetValue(1, vvv);
	EXPECT_EQ(hash.Count(), 2u);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(2), false);
	EXPECT_EQ(hash.IfExists(111111), false);
	EXPECT_EQ(hash.IfExists(1111111), true);
	EXPECT_EQ(hash.GetValue(1), vvv);
	EXPECT_EQ(hash.GetValue(1111111), vv);
	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);
}

