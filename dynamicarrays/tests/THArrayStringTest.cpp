#include "gtest/gtest.h"
#include "DynamicArrays.h"

typedef std::string TestingType;
typedef THArray<TestingType> THTestArray;

TEST(THArrayString, EmptyArray1)
{
	THTestArray arr;

	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
	EXPECT_NO_THROW(arr.Clear());
	EXPECT_NO_THROW(arr.ClearMem());
	EXPECT_NO_THROW(arr.Hold());
	EXPECT_NO_THROW(arr.Reverse());
	EXPECT_NO_THROW(arr.Reverse(5));
}

TEST(THArrayString, EmptyArray2)
{
	THTestArray arr;

	EXPECT_THROW(arr.GetValue(0), THArrayException);
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(1000), THArrayException);
	EXPECT_THROW(arr.DeleteValue(0), THArrayException);
	EXPECT_THROW(arr.DeleteValue(1), THArrayException);
	EXPECT_THROW(arr.DeleteValue(888), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(0), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(555), THArrayException);
	EXPECT_THROW(arr.Pop(), THArrayException);
}

TEST(THArrayString, EmptyIndexOf)
{
	THTestArray arr;

	EXPECT_EQ(arr.IndexOf(""), -1);
	EXPECT_EQ(arr.IndexOf(" "), -1);
	EXPECT_EQ(arr.IndexOf("1234"), -1);

	EXPECT_EQ(arr.IndexOfFrom("", 0), -1);
	EXPECT_EQ(arr.IndexOfFrom("1", 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(" ", 0), -1);
	EXPECT_EQ(arr.IndexOfFrom("\n", 0), -1);
	EXPECT_EQ(arr.IndexOfFrom("", 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(" ", 1), -1);
	EXPECT_EQ(arr.IndexOfFrom("2", 1), -1);
	EXPECT_EQ(arr.IndexOfFrom("\t", 1), -1);
	EXPECT_EQ(arr.IndexOfFrom("", 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(" ", 55), -1);
	EXPECT_EQ(arr.IndexOfFrom("2", 55), -1);
	EXPECT_EQ(arr.IndexOfFrom("666", 55), -1);
}

TEST(THArrayString, AddValue)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue("a"), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), "a");
	EXPECT_NE(arr.GetValue(0), "A");

	std::string* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, "a");
	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(11), THArrayException);
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	EXPECT_EQ(arr.IndexOf("a"), 0);
	EXPECT_EQ(arr.IndexOf("0"), -1);
	EXPECT_EQ(arr.IndexOf(" "), -1);
	EXPECT_EQ(arr.IndexOf("\r"), -1);
	EXPECT_EQ(arr.IndexOf("4"), -1);
	EXPECT_EQ(arr.IndexOfFrom("a", 0), 0);
	EXPECT_EQ(arr.IndexOfFrom("a", 1), -1);
	EXPECT_EQ(arr.IndexOfFrom("a", 2), -1);
	EXPECT_EQ(arr.IndexOfFrom("", 0), -1);
}

TEST(THArrayString, AddValue2)
{
	THTestArray arr;
	TestingType v = "555'555'555";
	EXPECT_EQ(arr.Add(&v), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), v);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, v);

	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(55555), THArrayException);

	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);
	EXPECT_EQ(arr.IndexOf(v), 0);
	EXPECT_EQ(arr.IndexOf("0"), -1);
	EXPECT_EQ(arr.IndexOf(""), -1);
	EXPECT_EQ(arr.IndexOf(" "), -1);
	EXPECT_EQ(arr.IndexOf("4"), -1);
	EXPECT_EQ(arr.IndexOfFrom(v, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(v, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(v, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom("0", 0), -1);
}

TEST(THArrayString, AddValue3)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue("3"), 0u);
	EXPECT_EQ(arr.AddValue("99"), 1u);
	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), "3");
	EXPECT_EQ(arr.GetValue(1), "99");

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, "3");
	vv = arr.GetValuePointer(1);
	EXPECT_EQ(*vv, "99");
	EXPECT_THROW(arr.GetValuePointer(2), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(6), THArrayException);

	EXPECT_THROW(arr.GetValue(2), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);
	EXPECT_EQ(arr.IndexOf("3"), 0);
	EXPECT_EQ(arr.IndexOf("99"), 1);
	EXPECT_EQ(arr.IndexOf("98"), -1);
	EXPECT_EQ(arr.IndexOf("100"), -1);
	EXPECT_EQ(arr.IndexOf("2"), -1);
	EXPECT_EQ(arr.IndexOf("4"), -1);
	EXPECT_EQ(arr.IndexOfFrom("3", 0), 0);
	EXPECT_EQ(arr.IndexOfFrom("3", 1), -1);
	EXPECT_EQ(arr.IndexOfFrom("3", 2), -1);
	EXPECT_EQ(arr.IndexOfFrom("3", 3), -1);
	EXPECT_EQ(arr.IndexOfFrom("99", 0), 1);
	EXPECT_EQ(arr.IndexOfFrom("99", 1), 1);
	EXPECT_EQ(arr.IndexOfFrom("99", 2), -1);
	EXPECT_EQ(arr.IndexOfFrom("99", 99), -1);
}

TEST(THArrayString, AddValue4)
{
	THTestArray arr;
	TestingType v1 = "333";
	TestingType v2 = "?555'555'555";
	EXPECT_EQ(arr.Add(&v1), 0u);
	EXPECT_EQ(arr.Add(&v2), 1u);

	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);

	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_EQ(arr.GetValue(1), v2);
	EXPECT_THROW(arr.GetValue(2), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.IndexOf(v2), 1);
	EXPECT_EQ(arr.IndexOf(""), -1);
	EXPECT_EQ(arr.IndexOf("1"), -1);
	EXPECT_EQ(arr.IndexOf("2"), -1);
	EXPECT_EQ(arr.IndexOf("4"), -1);
	EXPECT_EQ(arr.IndexOfFrom(v1, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(v1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(v1, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 0), 1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 1), 1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 2), -1);
}

TEST(THArrayString, AddValue5)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue("8"), 0u);
	EXPECT_EQ(arr.AddValue("88"), 1u);
	EXPECT_EQ(arr[0], "8");
	EXPECT_EQ(arr[1], "88");
	EXPECT_THROW(arr[3], THArrayException);
	EXPECT_THROW(arr[88], THArrayException);
}

TEST(THArrayString, ArayCopy)
{
	THTestArray arr1, arr2;

	arr1.AddValue("7");
	arr1.AddValue("77");

	arr2.AddValue("6");
	arr2.AddValue("66");

	arr1 = arr2;

	EXPECT_EQ(arr1.Count(), 2u);
	EXPECT_EQ(arr1.Capacity(), 2u);
	//EXPECT_NE(arr1.Memory(), nullptr);

	EXPECT_EQ(arr2.Count(), 2u);
	EXPECT_EQ(arr2.Capacity(), 4u);
	//EXPECT_NE(arr2.Memory(), nullptr);

	EXPECT_EQ(arr1[0], "6");
	EXPECT_EQ(arr1[1], "66");
	EXPECT_THROW(arr1[3], THArrayException);
	EXPECT_THROW(arr1[4], THArrayException);
	EXPECT_THROW(arr1[5], THArrayException);

	EXPECT_EQ(arr2[0], "6");
	EXPECT_EQ(arr2[1], "66");
	EXPECT_THROW(arr2[3], THArrayException);
	EXPECT_THROW(arr2[4], THArrayException);
	EXPECT_THROW(arr2[5], THArrayException);
}

TEST(THArrayString, Capacity1)
{
	THTestArray arr;

	arr.SetCapacity(500);
	EXPECT_EQ(arr.Capacity(), 500u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(600);
	EXPECT_EQ(arr.Capacity(), 600u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(400);
	EXPECT_EQ(arr.Capacity(), 400u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(0);
	EXPECT_EQ(arr.Capacity(), 0u);
	EXPECT_EQ(arr.Count(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);

	//int v = 2'000'000'000;
	//arr.SetCapacity(v); // try to allocate 2G memory
	//EXPECT_EQ(arr.Capacity(), v);
	//EXPECT_EQ(arr.Count(), 0);

}

TEST(THArrayString, Capacity2)
{
	THTestArray arr;

	for (uint i = 0; i < 500; i++)
	{
		arr.AddValue(std::to_string(i + 777));
	}

	EXPECT_GT(arr.Capacity(), 500u);

	arr.Hold();
	EXPECT_EQ(arr.Capacity(), 500u);

	arr.SetCapacity(600);
	EXPECT_EQ(arr.Capacity(), 600u);
	EXPECT_EQ(arr.Count(), 500u);
	arr.SetCapacity(400);
	EXPECT_EQ(arr.Capacity(), 400u);
	EXPECT_EQ(arr.Count(), 400u);
	arr.SetCapacity(0);
	EXPECT_EQ(arr.Capacity(), 0u);
	EXPECT_EQ(arr.Count(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
}

TEST(THArrayString, Grow)
{
	THTestArray arr;

	EXPECT_EQ(arr.Capacity(), 0u);
	arr.AddValue("11");
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue("22");
	arr.AddValue("33");
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue("44");
	arr.AddValue("11");
	EXPECT_EQ(arr.Capacity(), 8u);
	arr.AddValue("55");
	arr.AddValue("66");
	arr.AddValue("77");
	EXPECT_EQ(arr.Capacity(), 8u);
	arr.AddValue("22");
	EXPECT_EQ(arr.Capacity(), 12u);
	EXPECT_EQ(arr.Count(), 9u);

	for (uint i = 0; i < 4; i++)
	{
		arr.AddValue(std::to_string(i + 11));
	}
	EXPECT_EQ(arr.Capacity(), 28u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(std::to_string(i + 11));
	}

	EXPECT_EQ(arr.Count(), 29u);
	EXPECT_EQ(arr.Capacity(), 44u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(std::to_string(i + 11));
	}

	EXPECT_EQ(arr.Count(), 45u);
	EXPECT_EQ(arr.Capacity(), 60u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(std::to_string(i + 11));
	}

	EXPECT_EQ(arr.Count(), 61u);
	EXPECT_EQ(arr.Capacity(), 76u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(std::to_string(i + 11));
	}

	EXPECT_EQ(arr.Count(), 77u);
	EXPECT_EQ(arr.Capacity(), 95u);

	for (uint i = 0; i < 19; i++)
	{
		arr.AddValue(std::to_string(i + 11));
	}

	EXPECT_EQ(arr.Count(), 96u);
	EXPECT_EQ(arr.Capacity(), 118u);

	arr.Clear();
	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.Capacity(), 118u);

	arr.GrowTo(100);
	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.Capacity(), 118u);
}

TEST(THArrayString, PushPop)
{
	THTestArray arr;

	arr.Push("?500");
	EXPECT_EQ(arr.Capacity(), 4u);
	EXPECT_EQ(arr.Count(), 1u);

	arr.Push("600");
	EXPECT_EQ(arr.Capacity(), 4u);
	EXPECT_EQ(arr.Count(), 2u);

	arr.Push("");
	EXPECT_EQ(arr.Capacity(), 4u);
	EXPECT_EQ(arr.Count(), 3u);

	EXPECT_EQ(arr.Pop(), "");
	EXPECT_EQ(arr.Count(), 2u);

	EXPECT_EQ(arr.Pop(), "600");
	EXPECT_EQ(arr.Count(), 1u);

	EXPECT_EQ(arr.Pop(), "?500");
	EXPECT_EQ(arr.Count(), 0u);

	EXPECT_THROW(arr.Pop(), THArrayException);

	arr.InsertValue(0, "555");
	EXPECT_EQ(arr.Pop(), "555");
	EXPECT_EQ(arr.Count(), 0u);

}

TEST(THArrayString, PopFront)
{
	THTestArray arr;

	arr.Push("500");
	arr.Push("600");
	arr.Push("700");
	arr.InsertValue(0, "800");
	arr.InsertValue(1, "900");
	arr.InsertValue(arr.Count(), "200"); 

	EXPECT_EQ(arr.Capacity(), 8u);
	EXPECT_EQ(arr.Count(), 6u);

	EXPECT_EQ(arr[0], "800");
	EXPECT_EQ(arr[1], "900");
	EXPECT_EQ(arr[2], "500");
	EXPECT_EQ(arr[3], "600");
	EXPECT_EQ(arr[4], "700");
	EXPECT_EQ(arr[5], "200");

	EXPECT_EQ(arr.PopFront(), "800");
	EXPECT_EQ(arr.Count(), 5u);

	EXPECT_EQ(arr.PopFront(), "900");
	EXPECT_EQ(arr.Count(), 4u);

	EXPECT_EQ(arr.PopFront(), "500");
	EXPECT_EQ(arr.Count(), 3u);

	EXPECT_EQ(arr.PopFront(), "600");
	EXPECT_EQ(arr.Count(), 2u);

	EXPECT_EQ(arr.PopFront(), "700");
	EXPECT_EQ(arr.Count(), 1u);

	EXPECT_EQ(arr.PopFront(), "200");
	EXPECT_EQ(arr.Count(), 0u);

	EXPECT_THROW(arr.PopFront(), THArrayException);

	arr.InsertValue(0, "555");
	EXPECT_EQ(arr.PopFront(), "555");
	EXPECT_EQ(arr.Count(), 0u);
}


TEST(THArrayString, Zero)
{
	THTestArray arr;

	for (int i = 2; i < 35; i++)
	{
		arr.InsertValue(arr.Count(), std::to_string(i));
	}

	EXPECT_EQ(arr.Count(), 33u);

	for (int i = 0; i < (int)arr.Count(); i++)
	{
		EXPECT_EQ(arr[i], std::to_string(i + 2));
	}

	arr.Zero();

	EXPECT_EQ(arr.Count(), 33u);

	for (uint i = 0; i < arr.Count(); i++)
	{
		EXPECT_EQ(arr[i], "");
	}
}


TEST(THArrayString, InsertValue1)
{
	THTestArray arr;

	for (int i = 0; i < 100; i++)
	{
		arr.InsertValue(0, std::to_string(i + 1000));
	}

	EXPECT_EQ(arr.Count(), 100u);

	for (int i = 0; i < 100; i++)
	{
		EXPECT_EQ(arr[i], std::to_string(1099 - i));
	}
}

TEST(THArrayString, DeleteValue1)
{
	THTestArray arr;

	for (int i = 0; i < 10; i++)
	{
		arr.AddValue(std::to_string(i + 1));
	}

	EXPECT_EQ(arr.Count(), 10u);
	arr.DeleteValue(0);
	EXPECT_EQ(arr.Count(), 9u);
	for (int i = 0; i < 9; i++)
	{
		EXPECT_EQ(arr[i], std::to_string(i + 2));
	}

	EXPECT_THROW(arr.DeleteValue(arr.Count()), THArrayException); // try to delete out of range element
	EXPECT_EQ(arr.Count(), 9u);

	arr.DeleteValue(arr.Count() - 1); //try to delete last element in the array
	EXPECT_EQ(arr.Count(), 8u);
	for (int i = 0; i < 8; i++)
	{
		EXPECT_EQ(arr[i], std::to_string(i + 2));
	}
}

TEST(THArrayString, Reverse1)
{
	THTestArray arr;

	EXPECT_NO_THROW(arr.Reverse());
	EXPECT_EQ(arr.Count(), 0u);
	arr.Reverse(0);
	arr.Reverse(1);
	arr.Reverse(10);

	for (int i = 0; i < 10; i++)
	{
		arr.AddValue(std::to_string(i + 1));
	}
	EXPECT_NO_THROW(arr.Reverse());

	EXPECT_EQ(arr.Count(), 10u);
	for (int i = 0; i < 10; i++)
	{
		EXPECT_EQ(arr[i], std::to_string(10 - i));
	}

	arr.Reverse(5); // note: element with index 5 is included into reverse.

	EXPECT_EQ(arr.Count(), 10u);
	for (int i = 0; i <= 5; i++)
	{
		EXPECT_EQ(arr[i], std::to_string(i + 5));
	}

	for (int i = 6; i < 10; i++)
	{
		EXPECT_EQ(arr[i], std::to_string(10 - i));
	}
}


TEST(THArrayString, testCommonArray2)
{
	std::string str = "";
	THArray<std::string> arr;

	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 0ul);

	arr.ClearMem();
	str = "\n";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 0ul);

	arr.ClearMem();
	str = "\n\n";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 0ul);

	arr.ClearMem();
	str = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 0ul);

	arr.ClearMem();
	str = " ";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 1ul);
	EXPECT_EQ(arr[0], " ");

	arr.ClearMem();
	str = "a\n";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 1ul);
	EXPECT_EQ(arr[0], "a");

	arr.ClearMem();
	str = "\na";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 1ul);
	EXPECT_EQ(arr[0], "a");

	arr.ClearMem();
	str = "a\nb";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 2ul);
	EXPECT_EQ(arr[0], "a");
	EXPECT_EQ(arr[1], "b");

	arr.ClearMem();
	str = "\n\n\na";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 1ul);
	EXPECT_EQ(arr[0], "a");

	arr.ClearMem();
	str = "\na\nb\n";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 2ul);
	EXPECT_EQ(arr[0], "a");
	EXPECT_EQ(arr[1], "b");

	arr.ClearMem();
	str = "a\nb\nsss\nuuuuu \n1234567890 ";
	StringToArray(str, arr);
	EXPECT_EQ(arr.Count(), 5ul);
	EXPECT_EQ(arr[0], "a");
	EXPECT_EQ(arr[1], "b");
	EXPECT_EQ(arr[2], "sss");
	EXPECT_EQ(arr[3], "uuuuu ");
	EXPECT_EQ(arr[4], "1234567890 ");
}

// AddValue, GetValue
TEST(THArrayString, testStringArray1)
{
	THArray<std::string> sar;

	sar.AddValue("");
	sar.AddValue("1");
	sar.AddValue("1000000");
	sar.AddValue("-9999999");

	EXPECT_EQ(sar.GetValue(0), "");
	EXPECT_EQ(sar.GetValue(1), "1");
	EXPECT_EQ(sar.GetValue(2), "1000000");
	EXPECT_EQ(sar.GetValue(3), "-9999999");
	EXPECT_EQ(sar.Count(), 4ul);
}

// IndexOf
TEST(THArrayString, testStringArray2)
{
	THArray<std::string> sar;

	sar.AddValue(std::string("aa"));
	sar.AddValue(std::string("qwrt zxcvbnm"));

	EXPECT_EQ(sar.IndexOf("aa"), 0);
	EXPECT_EQ(sar.IndexOf(std::string("aa")), 0);
	EXPECT_EQ(sar.IndexOf("qwrt zxcvbnm"), 1);
	EXPECT_EQ(sar.IndexOf(std::string("qwrt zxcvbnm")), 1);
	EXPECT_EQ(sar.IndexOf(std::string("qwrt zxcvbn")), -1);
	EXPECT_EQ(sar.IndexOf(std::string("wrt zxcvbnm")), -1);
	EXPECT_EQ(sar.IndexOf("a"), -1);
	EXPECT_EQ(sar.IndexOf(""), -1);

	sar.Clear();

	EXPECT_EQ(sar.IndexOf("aa"), -1);
	EXPECT_EQ(sar.IndexOf(std::string("aa")), -1);
	EXPECT_EQ(sar.IndexOf("qwrt zxcvbnm"), -1);
	EXPECT_EQ(sar.IndexOf(std::string("qwrt zxcvbnm")), -1);
	EXPECT_EQ(sar.IndexOf(std::string("qwrt zxcvbn fghhgffd")), -1);

	EXPECT_EQ(sar.Count(), 0ul);
}

// Count, DeleteValue
TEST(THArrayString, testStringArray3)
{
	THArray<std::string> sar;

	EXPECT_EQ(sar.Count(), 0ul);

	sar.AddValue(std::string("98"));
	sar.AddValue("45");
	sar.AddValue(std::string("66"));

	EXPECT_EQ(sar.Count(), 3ul);

	sar.DeleteValue(0);
	EXPECT_EQ(sar.Count(), 2ul);

	sar.DeleteValue(0);
	EXPECT_EQ(sar.Count(), 1ul);

	EXPECT_THROW(sar.DeleteValue(1), THArrayException);

	EXPECT_EQ(sar.Count(), 1ul);

	sar.DeleteValue(0);
	EXPECT_EQ(sar.Count(), 0ul);
	EXPECT_THROW(sar.DeleteValue(0), THArrayException);
}

// Capacity, Clear, ClearMem
TEST(THArrayString, testStringArray4)
{
	THArray<std::string> sar;

	sar.AddValue("7");
	sar.AddValue("-9");
	sar.AddValue("987654321");

	EXPECT_EQ(sar[0], "7");
	EXPECT_EQ(sar[1], "-9");
	EXPECT_EQ(sar[2], "987654321");

	EXPECT_EQ(sar.Count(), 3ul);

	EXPECT_EQ(sar.Capacity(), 4ul);

	sar.Clear();

	EXPECT_EQ(sar.Capacity(), 4ul);

	sar.AddValue("2");
	sar.AddValue("4");
	sar.AddValue("101");
	sar.AddValue("101");

	EXPECT_EQ(sar[2], "101");
	EXPECT_EQ(sar[3], "101");
	EXPECT_EQ(sar.Count(), 4ul);
	EXPECT_EQ(sar.Capacity(), 4ul);

	sar.ClearMem();

	EXPECT_EQ(sar.Count(), 0ul);
	EXPECT_EQ(sar.Capacity(), 0ul);
}

// Reverse
TEST(THArrayString, testStringArray5)
{
	THArray<std::string> sar;

	sar.AddValue("7");
	sar.AddValue("9");
	sar.AddValue("-1000");
	sar.AddValue("0");

	EXPECT_EQ(sar[0], "7");
	EXPECT_EQ(sar[1], "9");

	sar.Reverse();

	EXPECT_EQ(sar[0], "0");
	EXPECT_EQ(sar[1], "-1000");
	EXPECT_EQ(sar[2], "9");
	EXPECT_EQ(sar[3], "7");
	EXPECT_EQ(sar.Count(), 4ul);


	sar.Clear();
	EXPECT_EQ(sar.Count(), 0ul);

	sar.AddValue("7");
	sar.AddValue("9");
	sar.AddValue("-1000");
	sar.AddValue("0");
	sar.AddValue("78");

	EXPECT_EQ(sar.Count(), 5ul);

	sar.Reverse();

	EXPECT_EQ(sar[0], "78");
	EXPECT_EQ(sar[1], "0");
	EXPECT_EQ(sar[2], "-1000");
	EXPECT_EQ(sar[3], "9");
	EXPECT_EQ(sar[4], "7");
}

// Reverse(Index)
TEST(THArrayString, testStringArray6)
{
	THArray<std::string> sar;

	sar.AddValue("4");
	sar.AddValue("8");
	sar.AddValue("654");
	sar.AddValue("-5");
	sar.AddValue("0");

	sar.Reverse(0);

	EXPECT_EQ(sar[0], "4");
	EXPECT_EQ(sar[1], "8");
	EXPECT_EQ(sar[2], "654");
	EXPECT_EQ(sar[3], "-5");
	EXPECT_EQ(sar[4], "0");


	sar.Reverse(2);

	EXPECT_EQ(sar.Count(), 5ul);
	EXPECT_EQ(sar[0], "654");
	EXPECT_EQ(sar[1], "8");
	EXPECT_EQ(sar[2], "4");
	EXPECT_EQ(sar[3], "-5");
	EXPECT_EQ(sar[4], "0");

	sar.Reverse(1);

	EXPECT_EQ(sar[0], "8");
	EXPECT_EQ(sar[1], "654");
	EXPECT_EQ(sar[2], "4");
	EXPECT_EQ(sar[3], "-5");
	EXPECT_EQ(sar[4], "0");

	sar.Reverse(3);

	EXPECT_EQ(sar[0], "-5");
	EXPECT_EQ(sar[1], "4");
	EXPECT_EQ(sar[2], "654");
	EXPECT_EQ(sar[3], "8");
	EXPECT_EQ(sar[4], "0");
}

// InsertValue
TEST(THArrayString, testStringArray7)
{
	THArray<std::string> sar;

	sar.AddValue("7");
	sar.AddValue("9");

	sar.InsertValue(0, std::string("11"));

	EXPECT_EQ(sar.Count(), 3ul);
	EXPECT_EQ(sar[0], "11");
	EXPECT_EQ(sar[1], "7");
	EXPECT_EQ(sar[2], "9");

	sar.InsertValue(2, std::string("33"));

	EXPECT_EQ(sar.Count(), 4ul);
	EXPECT_EQ(sar[0], "11");
	EXPECT_EQ(sar[1], "7");
	EXPECT_EQ(sar[2], "33");
	EXPECT_EQ(sar[3], "9");

	EXPECT_THROW(sar.InsertValue(5, "iuytrewq"), THArrayException);

	EXPECT_EQ(sar.Count(), 4ul);

	sar.InsertValue(4, std::string("44"));

	EXPECT_EQ(sar.Count(), 5ul);
	EXPECT_EQ(sar[4], "44");

	sar.Clear();

	std::string a = "string";
	sar.Insert(0, &a);

	a = "next string";
	sar.Insert(1, &a);

	EXPECT_EQ(sar.Count(), 2ul);
	EXPECT_EQ(sar[0], "string");
	EXPECT_EQ(sar[1], "next string");
}

// Zero, SetCapacity, Hold
TEST(THArrayString, testStringArray8)
{
	THArray<std::string> sar;

	sar.AddValue("1");
	sar.AddValue("10");
	sar.AddValue("100");
	sar.AddValue("-12345");

	sar.Zero();

	EXPECT_EQ(sar[0], "");
	EXPECT_EQ(sar[1], "");
	EXPECT_EQ(sar[2], "");
	EXPECT_EQ(sar[3], "");

	sar.SetCapacity(8);

	EXPECT_EQ(sar.Capacity(), 8ul);

	sar.Hold();

	EXPECT_EQ(sar.Capacity(), sar.Count());
}

// AddValue, AddFillValues
TEST(THArrayString, testStringArray9)
{
	THArray<std::string> sar;

	std::string* buf = new std::string[20];

	buf[0] = "99";
	buf[1] = "88";
	buf[2] = "-77";
	buf[3] = "66";
	buf[4] = "-555";

	sar.AddValue("1");
	sar.AddValue("10");
	sar.AddValue("-12345");

	for (int i = 0; i < 20; i++)
		sar.Add(&buf[i]);

	EXPECT_EQ(sar.Count(), 23ul);
	EXPECT_EQ(sar[0], "1");
	EXPECT_EQ(sar[1], "10");
	EXPECT_EQ(sar[2], "-12345");
	EXPECT_EQ(sar[3], "99");
	EXPECT_EQ(sar[4], "88");
	EXPECT_EQ(sar[5], "-77");
	EXPECT_EQ(sar[6], "66");
	EXPECT_EQ(sar[7], "-555");

	sar.AddFillValues(10);

	EXPECT_EQ(sar.Count(), 33ul);
	//EXPECT_EQ(sar[22] == 0); value with index 22 is not defined (see initialization of buf[] array above)
	EXPECT_EQ(sar[23], "");
	EXPECT_EQ(sar[24], "");
	EXPECT_EQ(sar[25], "");
	EXPECT_EQ(sar[30], "");
	EXPECT_EQ(sar[31], "");
	EXPECT_EQ(sar[32], "");

	EXPECT_THROW(sar[33], THArrayException);
}

// IndexOf<Compare>(i)
TEST(THArrayString, testStringArray10)
{
	THArray<std::string> sar;

	sar.AddValue("1");
	sar.AddValue("7");
	sar.AddValue("88");
	sar.AddValue("");
	sar.AddValue("2");

	EXPECT_EQ(sar.IndexOf("88"), 2);

	using cmpstr = Compare<std::string>;
	EXPECT_EQ(sar.IndexOf<cmpstr>("1"), 0);
	EXPECT_EQ(sar.IndexOf<cmpstr>(""), 3);
	EXPECT_EQ(sar.IndexOf<cmpstr>("2"), 4);
	EXPECT_EQ(sar.IndexOf<cmpstr>("22"), -1);
	EXPECT_EQ(sar.IndexOf<cmpstr>("88"), 2);
}


template<class C>
class CompareCaseSensitive : public Compare<C>
{
public:
	bool eq(const std::string& a, const std::string& b) { return a == b; };
	bool lt(const std::string& a, const std::string& b) { return a < b; };
	bool mt(const std::string& a, const std::string& b) { return a > b; };
};

// IndexOfFrom, IndexOfFrom(i, v, Compare)
TEST(THArrayString, testStringArray11)
{
	THArray<std::string> sar;

	sar.AddValue("1");
	sar.AddValue("-8");
	sar.AddValue("7");
	sar.AddValue("long string");

	EXPECT_EQ(sar.IndexOfFrom("-8", 0), 1);
	EXPECT_EQ(sar.IndexOfFrom("-8", 1), 1);
	EXPECT_EQ(sar.IndexOfFrom("-8", 2), -1);
	EXPECT_EQ(sar.IndexOfFrom("-8", 3), -1);
	EXPECT_EQ(sar.IndexOfFrom("-8", 6), -1);

	sar.ClearMem();

	sar.AddValue("1");
	sar.AddValue("LONG STRING");
	sar.AddValue("7");
	sar.AddValue("long string");
	sar.AddValue("LONG STRING");
	sar.AddValue("2");
	sar.AddValue("9876");

	EXPECT_EQ(sar.IndexOfFrom("LONG STRING", 0), 1);
	EXPECT_EQ(sar.IndexOfFrom("LONG STRING", 1), 1);
	EXPECT_EQ(sar.IndexOfFrom("LONG STRING", 2), 4);
	EXPECT_EQ(sar.IndexOfFrom("LONG STRING", 3), 4);
	EXPECT_EQ(sar.IndexOfFrom("LONG STRING", 4), 4);
	EXPECT_EQ(sar.IndexOfFrom("LONG STRING", 5), -1);
	EXPECT_EQ(sar.IndexOfFrom("LONG STRING", 50), -1);

	sar.ClearMem();

	using cmpstr = Compare<std::string>;

	sar.AddValue("1");
	sar.AddValue("long string");
	sar.AddValue("7");
	sar.AddValue("cccccccccc");

	EXPECT_EQ(sar.IndexOfFrom<cmpstr>("long string", 0), 1);
	EXPECT_EQ(sar.IndexOfFrom<cmpstr>("long string", 1), 1);
	EXPECT_EQ(sar.IndexOfFrom<cmpstr>("long string", 2), -1);
	EXPECT_EQ(sar.IndexOfFrom<cmpstr>("long string", 3), -1);
	EXPECT_EQ(sar.IndexOfFrom<cmpstr>("long string", 6), -1);

	sar.ClearMem();

	sar.AddValue("1");
	sar.AddValue("long string");
	sar.AddValue("7");
	sar.AddValue("LONG STRING");
	sar.AddValue("long string");
	sar.AddValue("LONG STRING");
	sar.AddValue("9876");

	using cmpcase = CompareCaseSensitive<std::string>;

	EXPECT_EQ(sar.IndexOfFrom<cmpcase>("long string", 0), 1);
	EXPECT_EQ(sar.IndexOfFrom<cmpcase>("long string", 1), 1);
	EXPECT_EQ(sar.IndexOfFrom<cmpcase>("long string", 2), 4);
	EXPECT_EQ(sar.IndexOfFrom<cmpcase>("long string", 3), 4);
	EXPECT_EQ(sar.IndexOfFrom<cmpcase>("long string", 4), 4);
	EXPECT_EQ(sar.IndexOfFrom<cmpcase>("long string", 5), -1);
	EXPECT_EQ(sar.IndexOfFrom<cmpcase>("long string", 50), -1);
}

// Push, Pop
TEST(THArrayString, testStringArray12)
{
	THArray<std::string> sar;

	sar.Push("34567");
	sar.Push("7");
	sar.Push("long string");
	sar.Push("");
	sar.Push("2");

	EXPECT_EQ(sar.IndexOf("34567"), 0);
	EXPECT_EQ(sar.IndexOf("long string"), 2);
	EXPECT_EQ(sar.IndexOf("1"), -1);
	EXPECT_EQ(sar.IndexOf(""), 3);
	EXPECT_EQ(sar.IndexOf("2"), 4);
	EXPECT_EQ(sar.IndexOf(" 2"), -1);

	EXPECT_EQ(sar.Pop(), "2");

	EXPECT_EQ(sar.IndexOf("34567"), 0);
	EXPECT_EQ(sar.IndexOf("long string"), 2);
	EXPECT_EQ(sar.IndexOf("1"), -1);
	EXPECT_EQ(sar.IndexOf(""), 3);
	EXPECT_EQ(sar.IndexOf("2"), -1);
	EXPECT_EQ(sar.IndexOf(" 2"), -1);

	EXPECT_EQ(sar.Pop(), "");

	EXPECT_EQ(sar.IndexOf("34567"), 0);
	EXPECT_EQ(sar.IndexOf("long string"), 2);
	EXPECT_EQ(sar.IndexOf("1"), -1);
	EXPECT_EQ(sar.IndexOf(""), -1);
	EXPECT_EQ(sar.IndexOf("2"), -1);
	EXPECT_EQ(sar.IndexOf(" 2"), -1);

	EXPECT_EQ(sar.Pop(), "long string");

	EXPECT_EQ(sar.IndexOf("34567"), 0);
	EXPECT_EQ(sar.IndexOf("long string"), -1);
	EXPECT_EQ(sar.IndexOf("1"), -1);
	EXPECT_EQ(sar.IndexOf(""), -1);
	EXPECT_EQ(sar.IndexOf("2"), -1);
	EXPECT_EQ(sar.IndexOf(" 2"), -1);

	EXPECT_EQ(sar.Pop(), "7");

	EXPECT_EQ(sar.Count(), 1ul);

	EXPECT_EQ(sar.IndexOf("34567"), 0);

	EXPECT_EQ(sar.Pop(), "34567");

	EXPECT_THROW(sar.Pop(), THArrayException);
}

// Push, Swap
TEST(THArrayString, testStringArray13)
{
	THArray<std::string> sar;

	sar.Push("765");
	sar.Push("754");
	sar.Push("12");
	sar.Push("long string");
	sar.Push("");

	sar.Swap(1, 1);
	sar.Swap(0, 0);

	EXPECT_EQ(sar.IndexOf("765"), 0);
	EXPECT_EQ(sar.IndexOf("754"), 1);
	EXPECT_EQ(sar.IndexOf("12"), 2);
	EXPECT_EQ(sar.IndexOf("long string"), 3);
	EXPECT_EQ(sar.IndexOf(""), 4);

	sar.Swap(0, 1);

	EXPECT_EQ(sar.IndexOf("765"), 1);
	EXPECT_EQ(sar.IndexOf("754"), 0);

	EXPECT_THROW(sar.Swap(3, 5), THArrayException);

	EXPECT_EQ(sar.IndexOf("765"), 1);
	EXPECT_EQ(sar.IndexOf("754"), 0);
	EXPECT_EQ(sar.IndexOf("12"), 2);
	EXPECT_EQ(sar.IndexOf("long string"), 3);
	EXPECT_EQ(sar.IndexOf(""), 4);

}

// Memory
TEST(THArrayString, testStringArray14)
{
	THArray<std::string> sar;

	//std::string* mem = sar.Memory();
	//EXPECT_EQ(mem, nullptr);

	sar.AddValue("aa321");
	//mem = sar.Memory();
	//EXPECT_NE(mem, nullptr);

	printf("PASSED\n");
}

// GetValuePointer
TEST(THArrayString, testStringArray15)
{
	THArray<std::string> sar;

	sar.AddValue("eee321");
	sar.AddValue("1asd987");

	EXPECT_EQ(sar.Count(), 2ul);

	std::string* pValue = sar.GetValuePointer(0);
	*pValue = "333";
	EXPECT_EQ(sar[0], "333");

	pValue = sar.GetValuePointer(1);
	*pValue = "111";
	EXPECT_EQ(sar[1], "111");

	pValue = sar.GetValuePointer(0);
	EXPECT_EQ(*pValue, "333");
	*pValue = "";
	EXPECT_EQ(sar[0], "");

	pValue = sar.GetValuePointer(0);
	EXPECT_EQ(*pValue, "");

	EXPECT_THROW(sar.GetValuePointer(2),THArrayException);
	
	EXPECT_THROW(sar.GetValuePointer(5), THArrayException);
	
}

TEST(THArrayString, testStringArray16)
{
	THArray<std::string> sar;

	sar.AddValue("eee321");
	sar.AddValue("1asd987");
	sar.AddValue("bbbbbbb");
	sar.AddValue("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

	EXPECT_EQ(sar.Count(), 4ul);

	THArray<std::string> sarcopy;

	sarcopy.AddValue("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
	sarcopy.AddValue("77777777777777777\n\n\n\\");

	EXPECT_EQ(sarcopy.Count(), 2ul);

	sarcopy = sar;

	EXPECT_EQ(sarcopy[0], "eee321");
	EXPECT_EQ(sarcopy[1], "1asd987");
	EXPECT_EQ(sarcopy[2], "bbbbbbb");
	EXPECT_EQ(sarcopy[3], "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

}
