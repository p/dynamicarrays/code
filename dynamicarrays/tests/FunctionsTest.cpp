#include "gtest/gtest.h"
#include "DynamicArrays.h"

TEST(Functions, StringToArray1)
{
	THArrayString arr;
	StringToArray("", arr);
	EXPECT_EQ(arr.Count(), 0u);

	arr.Clear();
	StringToArray("\n", arr);
	EXPECT_EQ(arr.Count(), 0u);

	arr.Clear();
	StringToArray(" ", arr);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr[0], " ");

	arr.Clear();
	StringToArray(" \n", arr);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr[0], " ");

	arr.Clear();
	StringToArray("\n ", arr);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr[0], " ");
}

TEST(Functions, StringToArray2)
{
	THArrayString arr;
	
	StringToArray(",,,,,", arr, ',');
	EXPECT_EQ(arr.Count(), 0u);

	arr.Clear();
	StringToArray(" ,,,,, ", arr, ',');
	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr[0], " ");
	EXPECT_EQ(arr[1], " ");

	arr.Clear();
	StringToArray("1,2,3", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "1");
	EXPECT_EQ(arr[1], "2");
	EXPECT_EQ(arr[2], "3");

	arr.Clear();
	StringToArray("1,,2,,3,", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "1");
	EXPECT_EQ(arr[1], "2");
	EXPECT_EQ(arr[2], "3");

	arr.Clear();
	StringToArray(",1,2,,3", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "1");
	EXPECT_EQ(arr[1], "2");
	EXPECT_EQ(arr[2], "3");

	arr.Clear();
	StringToArray(",,1,,2,3", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "1");
	EXPECT_EQ(arr[1], "2");
	EXPECT_EQ(arr[2], "3");

	arr.Clear();
	StringToArray(",,,1,2,,,3,", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "1");
	EXPECT_EQ(arr[1], "2");
	EXPECT_EQ(arr[2], "3");

	arr.Clear();
	StringToArray("1,2,,3,", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "1");
	EXPECT_EQ(arr[1], "2");
	EXPECT_EQ(arr[2], "3");

	arr.Clear();
	StringToArray("1,,2,3,,,,", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "1");
	EXPECT_EQ(arr[1], "2");
	EXPECT_EQ(arr[2], "3");
}
 
TEST(Functions, StringToArray3)
{
	THArrayString arr;

	StringToArray("12,34,56", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "12");
	EXPECT_EQ(arr[1], "34");
	EXPECT_EQ(arr[2], "56");

	arr.Clear();
	StringToArray("1,,23,5,", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "1");
	EXPECT_EQ(arr[1], "23");
	EXPECT_EQ(arr[2], "5");

	arr.Clear();
	StringToArray(",123,4,55", arr, ',');
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr[0], "123");
	EXPECT_EQ(arr[1], "4");
	EXPECT_EQ(arr[2], "55");

}

TEST(Functions, testStripFileExt)
{
	EXPECT_EQ(StripFileExt(""), "");
	EXPECT_EQ(StripFileExt("."), "");
	EXPECT_EQ(StripFileExt(".."), ".");
	EXPECT_EQ(StripFileExt("..."), "..");
	EXPECT_EQ(StripFileExt("...."), "...");
	EXPECT_EQ(StripFileExt("1.234"), "1");
	EXPECT_EQ(StripFileExt("a"), "a");
	EXPECT_EQ(StripFileExt("aa"), "aa");
	EXPECT_EQ(StripFileExt("1234"), "1234");

	EXPECT_EQ(StripFileExt("1234."), "1234");
	EXPECT_EQ(StripFileExt("123.4"), "123");
	EXPECT_EQ(StripFileExt("12.34"), "12");
	EXPECT_EQ(StripFileExt("1.234"), "1");
	EXPECT_EQ(StripFileExt(".1234"), "");

	EXPECT_EQ(StripFileExt("12.3.4"), "12.3");
	EXPECT_EQ(StripFileExt(".12.34"), ".12");
	EXPECT_EQ(StripFileExt("g.hjk."), "g.hjk");
	EXPECT_EQ(StripFileExt("qwertyuioplkjhgfdsa.bvnb"), "qwertyuioplkjhgfdsa");
}

TEST(Functions, testStripFileExt2)
{
	EXPECT_EQ(StripFileExt("c:\\aaa\\bbb\\pp.log"), "c:\\aaa\\bbb\\pp");
	EXPECT_EQ(StripFileExt("c:\\aaa\\bbb\\pp."), "c:\\aaa\\bbb\\pp");

	EXPECT_EQ(StripFileExt(""), "");
	EXPECT_EQ(StripFileExt("\\"), "\\");
	EXPECT_EQ(StripFileExt("/"), "/");
	EXPECT_EQ(StripFileExt("c:"), "c:");
	EXPECT_EQ(StripFileExt("\\sssss.ss\\dsd\\ffffff"), "\\sssss.ss\\dsd\\ffffff");

	EXPECT_EQ(StripFileExt("\\sssss.ss\\dsd\\ffffff."), "\\sssss.ss\\dsd\\ffffff");

	EXPECT_EQ(StripFileExt("\\sssss.ss\\dsd\\ffffff.a"), "\\sssss.ss\\dsd\\ffffff");

	EXPECT_EQ(StripFileExt("\\sssss.ss\\dsd\\a. rrffffff "), "\\sssss.ss\\dsd\\a");
	
	EXPECT_EQ(StripFileExt("/dfdfd/dfdf/gfg/dffff/"), "/dfdfd/dfdf/gfg/dffff/");

	EXPECT_EQ(StripFileExt("/dfdfd/dfdf/gfg/d4444.rrr.ttt.y"), "/dfdfd/dfdf/gfg/d4444.rrr.ttt");

	EXPECT_EQ(StripFileExt("c:\\.aaa"), "c:\\");

	EXPECT_EQ(StripFileExt(".bbb"), "");

	EXPECT_EQ(StripFileExt("/.deps"), "/");

	EXPECT_EQ(StripFileExt("app.log.fgh"), "app.log");

	EXPECT_EQ(StripFileExt("app.log.fgh."), "app.log.fgh");

	EXPECT_EQ(StripFileExt("a.b"), "a");

	EXPECT_EQ(StripFileExt("a"), "a");
}

TEST(Functions, testStringReplace)
{
	EXPECT_EQ(StringReplace("", "", "").size(), 0ul);
	EXPECT_EQ(StringReplace("", "a", "b").size(), 0ul);
	EXPECT_EQ(StringReplace("", "", "bbbb").size(), 0ul);
	EXPECT_EQ(StringReplace("", "zzz", "bbb").size(), 0ul);
	EXPECT_EQ(StringReplace("", "zzz", "").size(), 0ul);
	EXPECT_EQ(StringReplace("gaaah vvvvb aa aaa hghg4aaa54", "aaa", "ZZZZ"), "gZZZZh vvvvb aa ZZZZ hghg4ZZZZ54");
	EXPECT_EQ(StringReplace("gaaah vvvvb aa aaa hghg4aaa", "aaa", ""), "gh vvvvb aa  hghg4");

	EXPECT_EQ(StringReplace("", "\r", "\n"), "");
	EXPECT_EQ(StringReplace("\r\r", "\r", ""), "");
	EXPECT_EQ(StringReplace("\r", "\r", "\n"), "\n");
	EXPECT_EQ(StringReplace("\r\n", "\r", ""), "\n");
	EXPECT_EQ(StringReplace("\r\n", "\r", "\n"), "\n\n");
	EXPECT_EQ(StringReplace("\r\n", "\r\n", "\n"), "\n");
	EXPECT_EQ(StringReplace("\r\n\r", "\r\n", "\n"), "\n\r");
	EXPECT_EQ(StringReplace("\r\n\n\r", "\r\n", "\n"), "\n\n\r");
	EXPECT_EQ(StringReplace("\r\n dfdf fgfg\r\n", "\r\n", "\n"), "\n dfdf fgfg\n");
}

TEST(Functions, testExtractFileName)
{
	EXPECT_EQ(ExtractFileName("c:\\aaa\\bbb\\pp.log"), "pp.log");
	EXPECT_EQ(ExtractFileName("\\sssss.ss\\dsd\\ffffff"), "ffffff");
	EXPECT_EQ(ExtractFileName("/dfdfd/dfdf/gfg/dffff/"), "");
	EXPECT_EQ(ExtractFileName("app.log.fgh"), "app.log.fgh");
}


TEST(Functions, testExtractFileDir1)
{
	EXPECT_EQ(ExtractFileDir("\\sssss.ss\\dsd\\ffffff"), "\\sssss.ss\\dsd\\");

	EXPECT_EQ(ExtractFileDir("\\sssss.ss\\dsd\\ffffff."), "\\sssss.ss\\dsd\\");

	EXPECT_EQ(ExtractFileDir("\\sssss.ss\\dsd\\ffffff.a"), "\\sssss.ss\\dsd\\");

	EXPECT_EQ(ExtractFileDir("\\sssss.ss\\dsd\\a. rrffffff "), "\\sssss.ss\\dsd\\");

	EXPECT_EQ(ExtractFileDir("/dfdfd/dfdf/gfg/dffff/"), "/dfdfd/dfdf/gfg/dffff/");

	EXPECT_EQ(ExtractFileDir("c:/dfdfd/dfdf/gfg/d4444.rrr.ttt.y"), "c:/dfdfd/dfdf/gfg/");

	EXPECT_EQ(ExtractFileDir("app.log.fgh"), "");
	
	EXPECT_EQ(ExtractFileDir("/app.log.fgh."), "/");

	EXPECT_EQ(ExtractFileDir("a.b/"), "a.b/");

	EXPECT_EQ(ExtractFileDir(""), "");
	EXPECT_EQ(ExtractFileDir("a"), "");

	EXPECT_EQ(ExtractFileDir("/"), "/");

	EXPECT_EQ(ExtractFileDir("\\"), "\\");
}


char mytoupper(int c) // to eliminate compile warning "warning C4244: '=': conversion from 'int' to 'char', possible loss of data"
{
	return (char)toupper(c);
}

std::string ToUpper(const std::string& str) // TODO what if we have \t in the beginning (end) of string?
{
	std::string dest;// = "1234567890";
	dest.resize(str.size());
	transform(str.begin(), str.end(), dest.begin(), ::mytoupper);
	return dest;
}

// can be removed, see CompareNCaseParamTest
TEST(Functions, testEqualNCase)
{
	#define  EqualNCaseTest EqualNCase<std::string>

	EXPECT_FALSE(EqualNCaseTest("�", "�")); // non latin symbols
	EXPECT_FALSE(EqualNCaseTest("A", "�")); // latin and non-latin symbols
	EXPECT_FALSE(EqualNCaseTest("f", ""));
	EXPECT_FALSE(EqualNCaseTest("", "5"));
	EXPECT_FALSE(EqualNCaseTest("\n", "\r"));
	EXPECT_FALSE(EqualNCaseTest("", " "));
	EXPECT_FALSE(EqualNCaseTest(" ", ""));
	EXPECT_FALSE(EqualNCaseTest("  ", ""));
	EXPECT_FALSE(EqualNCaseTest("", " "));
	EXPECT_FALSE(EqualNCaseTest(" ", "  "));
	EXPECT_FALSE(EqualNCaseTest("\n\n", "\n"));
	EXPECT_FALSE(EqualNCaseTest("\n\r", "\n"));
	EXPECT_FALSE(EqualNCaseTest("abc", "ab"));
	EXPECT_FALSE(EqualNCaseTest("ab", "abc"));
	EXPECT_FALSE(EqualNCaseTest(" ab", "ab"));
	EXPECT_FALSE(EqualNCaseTest("ab\n", "ab"));

	EXPECT_TRUE(EqualNCaseTest("", ""));
	EXPECT_TRUE(EqualNCaseTest(" ", " "));
	EXPECT_TRUE(EqualNCaseTest("\r", "\r"));
	EXPECT_TRUE(EqualNCaseTest("\n", "\n"));
	EXPECT_TRUE(EqualNCaseTest("ab", "AB"));
	EXPECT_TRUE(EqualNCaseTest("ABC", "ABc"));
	EXPECT_TRUE(EqualNCaseTest("ANDREY", "Andrey"));
	//EXPECT_TRUE(EqualNCase("����", ToUpper("����")));
	EXPECT_TRUE(EqualNCaseTest("����", "����"));
	//EXPECT_TRUE(EqualNCase("����", "����"));
	//EXPECT_TRUE(EqualNCase("����", "����"));
	EXPECT_TRUE(EqualNCaseTest("�", "�"));
	EXPECT_TRUE(EqualNCaseTest("\0", "\0"));
}

// can be removed, see CompareNCaseParamTest
TEST(Functions, testEqualNCaseWSTRING)
{
#define EqualNCaseTestW EqualNCase<std::wstring>

	EXPECT_FALSE(EqualNCaseTestW(L"�", L"�")); // non latin symbols
	EXPECT_FALSE(EqualNCaseTestW(L"A", L"�")); // latin and non-latin symbols
	EXPECT_FALSE(EqualNCaseTestW(L"f", L""));
	EXPECT_FALSE(EqualNCaseTestW(L"", L"5"));
	EXPECT_FALSE(EqualNCaseTestW(L"\n", L"\r"));
	EXPECT_FALSE(EqualNCaseTestW(L"", L" "));
	EXPECT_FALSE(EqualNCaseTestW(L" ", L""));
	EXPECT_FALSE(EqualNCaseTestW(L"  ", L""));
	EXPECT_FALSE(EqualNCaseTestW(L"", L" "));
	EXPECT_FALSE(EqualNCaseTestW(L" ", L"  "));
	EXPECT_FALSE(EqualNCaseTestW(L"\n\n", L"\n"));
	EXPECT_FALSE(EqualNCaseTestW(L"\n\r", L"\n"));
	EXPECT_FALSE(EqualNCaseTestW(L"abc", L"ab"));
	EXPECT_FALSE(EqualNCaseTestW(L"ab", L"abc"));
	EXPECT_FALSE(EqualNCaseTestW(L" ab", L"ab"));
	EXPECT_FALSE(EqualNCaseTestW(L"ab\n", L"ab"));

	EXPECT_TRUE(EqualNCaseTestW(L"", L""));
	EXPECT_TRUE(EqualNCaseTestW(L" ", L" "));
	EXPECT_TRUE(EqualNCaseTestW(L"\r", L"\r"));
	EXPECT_TRUE(EqualNCaseTestW(L"\n", L"\n"));
	EXPECT_TRUE(EqualNCaseTestW(L"ab", L"AB"));
	EXPECT_TRUE(EqualNCaseTestW(L"ABC", L"ABc"));
	EXPECT_TRUE(EqualNCaseTestW(L"ANDREY", L"Andrey"));
	//EXPECT_TRUE(EqualNCase("����", ToUpper("����")));
	EXPECT_TRUE(EqualNCaseTestW(L"����", L"����"));
	//EXPECT_TRUE(EqualNCase("����", "����"));
	//EXPECT_TRUE(EqualNCase("����", "����"));
	EXPECT_TRUE(EqualNCaseTestW(L"�", L"�"));
	EXPECT_TRUE(EqualNCaseTestW(L"\0", L"\0"));
}


//can be removed see CompareNCaseParamTest
TEST(Functions, testCompareNCase)
{
#define CompareNCaseTest CompareNCase<std::string>

	EXPECT_EQ(CompareNCaseTest("�", "�"), 1); // non latin symbols
	EXPECT_EQ(CompareNCaseTest("A", "�"), -1); // latin and non-latin symbols
	EXPECT_EQ(CompareNCaseTest("f", ""), 1);
	EXPECT_EQ(CompareNCaseTest("", "5"), -1);
	EXPECT_EQ(CompareNCaseTest("\n", "\r"), -1);
	EXPECT_EQ(CompareNCaseTest("", " "), -1);
	EXPECT_EQ(CompareNCaseTest(" ", ""), 1);
	EXPECT_EQ(CompareNCaseTest("  ", ""), 1);
	EXPECT_EQ(CompareNCaseTest("", " "), -1);
	EXPECT_EQ(CompareNCaseTest(" ", "  "), -1);
	EXPECT_EQ(CompareNCaseTest("\n\n", "\n"), 1);
	EXPECT_EQ(CompareNCaseTest("\n\r", "\n"), 1);
	EXPECT_EQ(CompareNCaseTest("abc", "ab"), 1);
	EXPECT_EQ(CompareNCaseTest("ab", "abc"), -1);
	EXPECT_EQ(CompareNCaseTest(" ab", "ab"), -1);
	EXPECT_EQ(CompareNCaseTest("ab\n", "ab"), 1);

	EXPECT_EQ(CompareNCaseTest("", ""), 0);
	EXPECT_EQ(CompareNCaseTest(" ", " "), 0);
	EXPECT_EQ(CompareNCaseTest("\r", "\r"), 0);
	EXPECT_EQ(CompareNCaseTest("\n", "\n"), 0);
	EXPECT_EQ(CompareNCaseTest("ABC", "ABC"), 0);
	EXPECT_EQ(CompareNCaseTest("����", "����"), 0);
	EXPECT_EQ(CompareNCaseTest("�", "�"), 0);
	EXPECT_EQ(CompareNCaseTest("\0", "\0"), 0);
}

//can be removed see CompareNCaseParamTest
TEST(Functions, testCompareNCaseWSTRING)
{
#define CompareNCaseTestW CompareNCase<std::wstring>

	EXPECT_EQ(CompareNCaseTestW(L"�", L"�"), 1); // non latin symbols
	EXPECT_EQ(CompareNCaseTestW(L"A", L"�"), -1); // latin and non-latin symbols
	EXPECT_EQ(CompareNCaseTestW(L"f", L""), 1);
	EXPECT_EQ(CompareNCaseTestW(L"", L"5"), -1);
	EXPECT_EQ(CompareNCaseTestW(L"\n", L"\r"), -1);
	EXPECT_EQ(CompareNCaseTestW(L"", L" "), -1);
	EXPECT_EQ(CompareNCaseTestW(L" ", L""), 1);
	EXPECT_EQ(CompareNCaseTestW(L"  ", L""), 1);
	EXPECT_EQ(CompareNCaseTestW(L"", L" "), -1);
	EXPECT_EQ(CompareNCaseTestW(L" ", L"  "), -1);
	EXPECT_EQ(CompareNCaseTestW(L"\n\n", L"\n"), 1);
	EXPECT_EQ(CompareNCaseTestW(L"\n\r", L"\n"), 1);
	EXPECT_EQ(CompareNCaseTestW(L"abc", L"ab"), 1);
	EXPECT_EQ(CompareNCaseTestW(L"ab", L"abc"), -1);
	EXPECT_EQ(CompareNCaseTestW(L" ab", L"ab"), -1);
	EXPECT_EQ(CompareNCaseTestW(L"ab\n", L"ab"), 1);

	EXPECT_EQ(CompareNCaseTestW(L"", L""), 0);
	EXPECT_EQ(CompareNCaseTestW(L" ", L" "), 0);
	EXPECT_EQ(CompareNCaseTestW(L"\r", L"\r"), 0);
	EXPECT_EQ(CompareNCaseTestW(L"\n", L"\n"), 0);
	EXPECT_EQ(CompareNCaseTestW(L"ABC", L"ABC"), 0);
	EXPECT_EQ(CompareNCaseTestW(L"����", L"����"), 0);
	EXPECT_EQ(CompareNCaseTestW(L"�", L"�"), 0);
	EXPECT_EQ(CompareNCaseTestW(L"\0", L"\0"), 0);
}
