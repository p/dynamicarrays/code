#include "gtest/gtest.h"
#include "DynamicArrays.h"

TEST(THArrayRaw, testRawArray0)
{
	EXPECT_THROW(THArrayRaw iar(0), THArrayException);

}

// Add, Get
TEST(THArrayRaw, testRawArray1)
{
	THArrayRaw iar(30);

	const int n = 30;
	char str[n];

	memset(str, '\0', n);
	EXPECT_THROW(iar.Get(0, str), THArrayException);
		
	memset(str, '\0', n);
	EXPECT_THROW(iar.Get(1, str), THArrayException);

	char str0[n] = "1234567890qwertyuiopasdfghjkl";
	char str1[n] = "11111111112222222222333333333";
	char str2[n] = "00000000009999999999888888888";

	iar.Add(str0);
	iar.Add(str1);
	iar.Add(str2);

	EXPECT_EQ(iar.Count(), 3ul);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str1, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	memset(str, '\0', n);
	EXPECT_THROW(iar.Get(3, str), THArrayException);

	iar.Add(nullptr);

	/*memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str2) == 0);
*/
}

// Count, DeleteValue
TEST(THArrayRaw, testRawArray3)
{
	const int n = 30;
	THArrayRaw iar(n);

	EXPECT_EQ(iar.Count(), 0ul);

	EXPECT_THROW(iar.Delete(0), THArrayException);

	char str0[] = "1234567890qwertyuiopasdfghjkl00000";
	char str1[n] = "11111111112222222222333333333";
	char str2[n] = "00000000009999999999888888888";

	iar.Add(str0);
	iar.Add(str1);
	iar.Add(str2);

	EXPECT_EQ(iar.Count(), 3ul);

	iar.Delete(0);
	EXPECT_EQ(iar.Count(), 2ul);

	iar.Delete(0);
	EXPECT_EQ(iar.Count(), 1ul);

	EXPECT_THROW(iar.Delete(1), THArrayException);

	EXPECT_EQ(iar.Count(), 1ul);

	iar.Delete(0);
	EXPECT_EQ(iar.Count(), 0ul);

	EXPECT_THROW(iar.Delete(0), THArrayException);
}

// Count, Delete
TEST(THArrayRaw, testRawArray3_1)
{
	const int n = 30;
	THArrayRaw iar(n);

	EXPECT_EQ(iar.Count(), 0ul);

	EXPECT_THROW(iar.Delete(0), THArrayException);	
	EXPECT_THROW(iar.Delete(5), THArrayException);

	char str0[] = "1234567890qwertyuiopasdfghjkl00000";
	char str1[n] = "11111111112222222222333333333";
	char str2[n] = "00000000009999999999888888888";

	iar.Add(str0);
	iar.Add(str1);
	iar.Add(str2);

	EXPECT_EQ(iar.Count(), 3ul);
	EXPECT_THROW(iar.Delete(3), THArrayException);

	iar.Delete(iar.Count() - 1);
	EXPECT_EQ(iar.Count(), 2ul);
	EXPECT_THROW(iar.Delete(2), THArrayException);

	iar.Delete(iar.Count() - 1);
	EXPECT_EQ(iar.Count(), 1ul);
	EXPECT_THROW(iar.Delete(1), THArrayException);

	EXPECT_EQ(iar.Count(), 1ul);

	iar.Delete(iar.Count() - 1);
	EXPECT_EQ(iar.Count(), 0ul);

	EXPECT_THROW(iar.Delete(0), THArrayException);
	
	EXPECT_THROW(iar.Delete(iar.Count() - 1), THArrayException);
}


// Capacity, Clear, ClearMem
TEST(THArrayRaw, testRawArray4)
{
	const int n = 30;
	THArrayRaw iar(n);

	char str[n];

	char str0[] = "1234567890qwertyuiopasdfghjkl00000";
	char str1[n] = "11111111112222222222333333333";
	char str2[n] = "00000000009999999999888888888";

	iar.Add(str0);
	iar.Add(str1);
	iar.Add(str2);

	EXPECT_EQ(iar.Count(), 3ul);
	EXPECT_EQ(iar.Capacity(), 4ul);

	iar.Clear();

	EXPECT_EQ(iar.Capacity(), 4ul);

	iar.Add(str0);
	iar.Add(str0);
	iar.Add(str0);
	iar.Add(str0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	EXPECT_EQ(iar.Count(), 4ul);
	EXPECT_EQ(iar.Capacity(), 4ul);

	iar.ClearMem();

	EXPECT_EQ(iar.Count(), 0ul);
	EXPECT_EQ(iar.Capacity(), 0ul);
}

// Insert
TEST(THArrayRaw, testRawArray7)
{
	const int n = 30;
	THArrayRaw iar(n);

	char str[n];

	char str0[] = "1234567890qwertyuiopasdfghjkl00000";
	char str1[n] = "11111111112222222222333333333";
	char str2[n] = "00000000009999999999888888888";

	iar.Add(str0);
	iar.Add(str1);
	iar.Add(str2);

	iar.Insert(0, str2);

	EXPECT_EQ(iar.Count(), 4ul);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str1, n), 0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	char str33[n] = "hgjtkmnhjgdtyugdftrefbnmlkoi7";
	iar.Insert(2, str33);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str33, n), 0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str1, n), 0);

	memset(str, '\0', n);
	iar.Get(4, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	EXPECT_EQ(iar.Count(), 5ul);

	EXPECT_THROW(iar.Insert(6, nullptr), THArrayException);

	EXPECT_EQ(iar.Count(), 5ul);

	char str44[n] = "44444444444444444444444444";
	iar.Insert(5, str44);

	EXPECT_EQ(iar.Count(), 6ul);

	memset(str, '\0', n);
	iar.Get(5, str);
	EXPECT_EQ(memcmp(str, str44, n), 0);

	iar.Clear();

	EXPECT_EQ(iar.Count(), 0ul);
}

// Zero, SetCapacity, Hold
TEST(THArrayRaw, testRawArray8)
{
	const int n = 30;
	THArrayRaw iar(n);

	char str[n];

	char str0[] = "1234567890qwertyuiopasdfghjkl00000";
	char str1[n] = "11111111112222222222333333333";
	char str2[n] = "00000000009999999999888888888";
	char str3[] = "33333333333333333333333333333333333333333333333";

	iar.Add(str0);
	iar.Add(str1);
	iar.Add(str2);
	iar.Add(str3);

	iar.Zero();

	char strzero[n];
	memset(strzero, '\0', n);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, strzero, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, strzero, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, strzero, n), 0);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, strzero, n), 0);

	iar.SetCapacity(8);

	EXPECT_EQ(iar.Capacity(), 8ul);

	iar.Hold();

	EXPECT_EQ(iar.Capacity(), iar.Count());
}

// AddMany, AddFillValues
TEST(THArrayRaw, testRawArray9)
{
	const int n = 5;
	THArrayRaw iar(n);

	char str[n];

	//char str0[] = "1234567890qwertyuiopasdfghjkl00000";
	char str1[] = "11111111112222222222333333333";
	//char str2[] = "00000000009999999999888888888";
	//char str3[] = "33333333333333333333333333333333333333333333333";

	iar.AddMany(str1, 6);

	memset(str, '\0', n);
	iar.Get(5, str);
	EXPECT_EQ(memcmp(str, str1 + 5 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(4, str);
	EXPECT_EQ(memcmp(str, str1 + 4 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str1 + 3 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str1 + 2 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str1 + 1 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str1 + 0 * n, n), 0);

	iar.AddFillValues(10);
	EXPECT_EQ(iar.Count(), 16ul);

	memset(str, '\0', n);
	iar.Get(5, str);
	EXPECT_EQ(memcmp(str, str1 + 5 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(4, str);
	EXPECT_EQ(memcmp(str, str1 + 4 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str1 + 3 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str1 + 2 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str1 + 1 * n, n), 0);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str1 + 0 * n, n), 0);

	char strzero[n];
	memset(strzero, '\0', n);

	memset(str, '\0', n);
	iar.Get(15, str);
	EXPECT_EQ(memcmp(str, strzero, n), 0);

	memset(str, '\0', n);
	iar.Get(10, str);
	EXPECT_EQ(memcmp(str, strzero, n), 0);

	memset(str, '\0', n);
	iar.Get(7, str);
	EXPECT_EQ(memcmp(str, strzero, n), 0);

	memset(str, '\0', n);
	iar.Get(6, str);
	EXPECT_EQ(memcmp(str, strzero, n), 0);

	memset(str, '\0', n);
	iar.Get(5, str);
	EXPECT_NE(memcmp(str, strzero, n), 0);

	EXPECT_THROW(iar.Get(16, str), THArrayException);
}

//  Swap
TEST(THArrayRaw, testRawArray13)
{
	const int n = 15;
	THArrayRaw iar(n);

	char str[n];

	char str0[] = "1234567890qwertyuiopasdfghjkl00000";
	char str1[] = "11111111112222222222333333333";
	char str2[] = "00000000009999999999888888888";
	char str3[] = "33333333333333333333333333333333333333333333333";

	iar.Add(str0);
	iar.Add(str1);
	iar.Add(str2);
	iar.Add(str3);

	iar.Swap(1, 1);
	iar.Swap(0, 0);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str1, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str3, n), 0);

	iar.Swap(0, 1);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str1, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str3, n), 0);

	iar.Swap(0, 3);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str3, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str1, n), 0);

	iar.Swap(1, 3);

	memset(str, '\0', n);
	iar.Get(0, str);
	EXPECT_EQ(memcmp(str, str3, n), 0);

	memset(str, '\0', n);
	iar.Get(1, str);
	EXPECT_EQ(memcmp(str, str1, n), 0);

	memset(str, '\0', n);
	iar.Get(2, str);
	EXPECT_EQ(memcmp(str, str2, n), 0);

	memset(str, '\0', n);
	iar.Get(3, str);
	EXPECT_EQ(memcmp(str, str0, n), 0);

	EXPECT_THROW(iar.Swap(3, 4), THArrayException);
	EXPECT_THROW(iar.Swap(4, 4), THArrayException);
	EXPECT_THROW(iar.Swap(6, 6), THArrayException);
}

