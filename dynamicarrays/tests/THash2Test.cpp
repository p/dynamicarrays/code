#include "gtest/gtest.h"
#include <vector>
#include "DynamicArrays.h"

typedef unsigned long KeyType;
typedef unsigned long ValueType1;
typedef unsigned long ValueType2;

typedef THash2<KeyType, ValueType1, ValueType2> TestHash;

TEST(THash2Long, EmptyHash1)
{
	TestHash hash;

	EXPECT_EQ(hash.Count(), 0u);
	EXPECT_EQ(hash.GetAIndexes().Count(), 0u);
	EXPECT_EQ(hash.GetAValues().Count(), 0u);
	EXPECT_NO_THROW(hash.Clear());
	//EXPECT_NO_THROW(hash.ClearMem());
}

TEST(THash2Long, EmptyHash2)
{
	TestHash hash;

	EXPECT_THROW(hash.GetKey(0u), THArrayException);
	EXPECT_THROW(hash.GetKey(1u), THArrayException);
	EXPECT_THROW(hash.GetValue(3u), THArrayException);
	EXPECT_THROW(hash.GetValue(1000u), THArrayException);
	EXPECT_NO_THROW(hash.Delete(3u));
	EXPECT_NO_THROW(hash.Delete(888u));
	EXPECT_EQ(hash.GetValuePointer(0u), nullptr);
	EXPECT_EQ(hash.GetValuePointer(44u), nullptr);
}

TEST(THash2Long, EmptyIndexOf)
{
	TestHash hash;

	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(1), false);
	EXPECT_EQ(hash.IfExists(1234), false);
}

TEST(THash2Long, SetValue)
{
	TestHash hash;
	hash.SetValue(1, 1, 1);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(1, 1), true);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(0, 1), false);
	EXPECT_EQ(hash.IfExists(1, 0), false);
	EXPECT_EQ(hash.IfExists(2), false);
	EXPECT_EQ(hash.IfExists(2, 0), false);
	EXPECT_EQ(hash.IfExists(2, 1), false);
	EXPECT_EQ(hash.IfExists(0, 2), false);
	EXPECT_EQ(hash.IfExists(1, 2), false);

	THash<ValueType1, ValueType2> v;
	v.SetValue(1, 1);
	EXPECT_EQ(hash.GetValue(1), v);

	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);

	hash.SetValue(1111, 1111, 1111);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(1, 1), true);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(0, 1), false);
	EXPECT_EQ(hash.IfExists(1, 0), false);
	EXPECT_EQ(hash.IfExists(2), false);
	EXPECT_EQ(hash.IfExists(2, 0), false);
	EXPECT_EQ(hash.IfExists(2, 1), false);
	EXPECT_EQ(hash.IfExists(0, 2), false);
	EXPECT_EQ(hash.IfExists(1, 2), false);

	EXPECT_EQ(hash.IfExists(111), false);
	EXPECT_EQ(hash.IfExists(11), false);
	EXPECT_EQ(hash.IfExists(111111), false);
	EXPECT_EQ(hash.IfExists(1111), true);
	EXPECT_EQ(hash.IfExists(111111, 1111), false);
	EXPECT_EQ(hash.IfExists(1111, 1), false);
	EXPECT_EQ(hash.IfExists(1111, 11), false);
	EXPECT_EQ(hash.IfExists(1111, 111), false);
	EXPECT_EQ(hash.IfExists(1111, 1111), true);

	EXPECT_EQ(hash.GetValue(1), v);
	EXPECT_EQ(hash.GetValue(1u, 1u), 1u);
	EXPECT_EQ(hash.GetValue(1111u, 1111u), 1111u);
	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);
	EXPECT_THROW(hash.GetValue(0, 1111), THArrayException);
	EXPECT_THROW(hash.GetValue(1111, 0), THArrayException);
	EXPECT_THROW(hash.GetValue(2, 1111), THArrayException);
	EXPECT_THROW(hash.GetValue(1111, 2), THArrayException);
	EXPECT_THROW(hash.GetValue(0, 1), THArrayException);
	EXPECT_THROW(hash.GetValue(1, 0), THArrayException);
	EXPECT_THROW(hash.GetValue(2, 1), THArrayException);
	EXPECT_THROW(hash.GetValue(1, 2), THArrayException);

	EXPECT_EQ(hash.Count(), 2u);

	THash<ValueType1, ValueType2> vv;
	vv.SetValue(55, 55);
	hash.SetValue(1u, vv);
	EXPECT_EQ(hash.Count(), 2u);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(1, 55), true);
	EXPECT_EQ(hash.IfExists(1, 1), false);
	EXPECT_EQ(hash.IfExists(55, 1), false);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(0, 1), false);
	EXPECT_EQ(hash.IfExists(1, 0), false);
	EXPECT_EQ(hash.IfExists(2), false);
	EXPECT_EQ(hash.IfExists(2, 0), false);
	EXPECT_EQ(hash.IfExists(2, 1), false);
	EXPECT_EQ(hash.IfExists(0, 2), false);
	EXPECT_EQ(hash.IfExists(1, 2), false);
	EXPECT_EQ(hash.IfExists(111111), false);
	EXPECT_EQ(hash.IfExists(1111), true);
	EXPECT_EQ(hash.IfExists(1111, 1111), true);
	EXPECT_EQ(hash.IfExists(1111, 55), false);
	EXPECT_EQ(hash.IfExists(55, 1111), false);

	EXPECT_EQ(hash.GetValue(1u), vv);
	EXPECT_EQ(hash.GetValue(1u, 55u), 55u);
	EXPECT_EQ(hash.GetValue(1111u, 1111u), 1111u);

	THash<ValueType1, ValueType2> vvv;
	vvv.SetValue(1111, 1111);
	EXPECT_EQ(hash.GetValue(1111), vvv);

	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);
	EXPECT_THROW(hash.GetValue(0, 1111), THArrayException);
	EXPECT_THROW(hash.GetValue(1111, 0), THArrayException);
	EXPECT_THROW(hash.GetValue(2, 1111), THArrayException);
	EXPECT_THROW(hash.GetValue(1111, 2), THArrayException);
	EXPECT_THROW(hash.GetValue(0, 1), THArrayException);
	EXPECT_THROW(hash.GetValue(1, 0), THArrayException);
	EXPECT_THROW(hash.GetValue(2, 1), THArrayException);
	EXPECT_THROW(hash.GetValue(1, 2), THArrayException);
}

TEST(THash2Long, CountTest)
{
	TestHash hash;

	hash.SetValue(44, 55, 66);
	EXPECT_EQ(hash.Count(), 1u);

	hash.SetValue(44, 55, 77);
	EXPECT_EQ(hash.Count(), 1u);

	hash.SetValue(44, 56, 88);
	EXPECT_EQ(hash.Count(), 2u);

	hash.SetValue(33);
	EXPECT_EQ(hash.Count(), 2u); // note that Count hasn't changed

	hash.SetValue(33, 55, 66);
	EXPECT_EQ(hash.Count(), 3u);

	hash.SetValue(33, 55, 77);
	EXPECT_EQ(hash.Count(), 3u);

	hash.SetValue(33, 56, 88);
	EXPECT_EQ(hash.Count(), 4u);

	THash<ValueType1, ValueType2> vvv;
	vvv.SetValue(1111, 1111);
	
	hash.SetValue(22, vvv);
	EXPECT_EQ(hash.Count(), 5u);
	hash.SetValue(33, vvv);
	EXPECT_EQ(hash.Count(), 4u); // weid result - we set value but count is desreased
	hash.SetValue(44, vvv);
	EXPECT_EQ(hash.Count(), 3u);// weid result - we set value but count is desreased

}
