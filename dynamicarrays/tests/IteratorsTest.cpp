#include "gtest/gtest.h"
#include "DynamicArrays.h"

typedef std::string TestingType;
typedef std::string TestingType1;
typedef std::string TestingType2;

//typedef long TestingType;
//typedef long TestingType1;
//typedef long TestingType2;

typedef THArraySorted<TestingType> THTestArray;
typedef THash<TestingType1, TestingType2> TestHash;

TEST(InteratorsTest, ArrayTest1)
{
	THTestArray arr;

	THTestArray::iterator b = arr.begin();
	THTestArray::iterator e = arr.end();
	EXPECT_EQ(b, e);  // for empty container two iteratord are equal each other

	auto bb = arr.begin();
	auto ee = arr.end();
	EXPECT_EQ(bb, ee);  // for empty container two iteratord are equal each other

	auto eee = arr.end();
	EXPECT_EQ(bb, eee);  // for empty container two iteratord are equal each other
	EXPECT_EQ(b, eee);  // for empty container two iteratord are equal each other

	TestingType v = TestingType{ 4 };
	arr.AddValue(v);
	bb = arr.begin();
	ee = arr.end();
	EXPECT_NE(bb, ee);
	++bb;
	EXPECT_EQ(bb, ee);
	--bb;
	EXPECT_NE(bb, ee);
	--ee;
	EXPECT_EQ(bb, ee);
	EXPECT_EQ(v, *ee);
	EXPECT_EQ(v, *bb);

	TestingType vv = TestingType{ 1 };
	arr.AddValue(vv);
	bb = arr.begin();
	ee = arr.end();
	EXPECT_NE(bb, ee);
	EXPECT_EQ(vv, *bb); // because array is sorted
	++bb;
	EXPECT_NE(bb, ee);
	EXPECT_EQ(v, *bb);
	++bb;
	EXPECT_EQ(bb, ee);

	--bb;
	--ee;
	EXPECT_EQ(bb, ee);
	EXPECT_EQ(v, *bb);
	EXPECT_EQ(v, *ee);
}

TEST(InteratorsTest, ArrayTest2)
{
	THTestArray arr;

	char counter = 0;
	for (TestingType a: arr)
	{
		EXPECT_EQ(true, false); // arr is empty, it should not go here
	}
	EXPECT_EQ(0, counter);

	counter = 0;
	for (TestingType& a: arr)
	{
		EXPECT_EQ(true, false); // arr is empty, it should not go here
	}
	EXPECT_EQ(0, counter);

	counter = 0;
	for (TestingType b : arr)
	{
		EXPECT_EQ(true, false); // arr is empty, it should not go here
	}
	EXPECT_EQ(0, counter);

	counter = 0;
	for (TestingType& b : arr)
	{
		EXPECT_EQ(true, false); // arr is empty, it should not go here
	}
	EXPECT_EQ(0, counter);

	counter = 0;
	for (auto iter = arr.begin(); iter != arr.end(); ++iter)
	{
		EXPECT_EQ(true, false); // arr is empty, it should not go here
	}
	EXPECT_EQ(0, counter);
}

TEST(InteratorsTest, ArrayTest3)
{
	THTestArray origarr;
	THTestArray arr;

	TestingType v = TestingType{ 3 };
	arr.AddValue(v);
	origarr.AddValue(v);

	int counter = 0;
	for(TestingType a: arr)
	{
		EXPECT_EQ(a, v);
		counter++;
	}
	EXPECT_EQ(1, counter);
	
	counter = 0;
	for (TestingType& a: arr)
	{
		EXPECT_EQ(a, v);
		counter++;
	}
	EXPECT_EQ(1, counter);

	counter = 0;
	for (TestingType b : arr)
	{
		EXPECT_EQ(b, v);
		counter++;
	}
	EXPECT_EQ(1, counter);

	counter = 0;
	for (TestingType& b : arr)
	{
		EXPECT_EQ(b, v);
		counter++;
	}
	EXPECT_EQ(1, counter);

	counter = 0;
	for (auto iter = arr.begin(); iter != arr.end(); ++iter)
	{
		EXPECT_EQ(*iter, v);
		counter++;
	}
	EXPECT_EQ(1, counter);
}

TEST(InteratorsTest, ArrayTest4)
{
	THTestArray arr;
	THTestArray origarr;

	for (char i = 0; i < 5; i++)
	{
		TestingType v = TestingType{ i };
		arr.AddValue(v);
		TestingType vv = TestingType{i};
		origarr.AddValue(vv);
	}
	
	int counter = 0;
	for (TestingType a: arr)
	{
		EXPECT_EQ(a, origarr[counter++]);
	}
	EXPECT_EQ(5, counter);

	counter = 0;
	for (TestingType& a: arr)
	{
		EXPECT_EQ(a, origarr[counter++]);
	}
	EXPECT_EQ(5, counter);

	counter = 0;
	for (TestingType b : arr)
	{
		EXPECT_EQ(b, origarr[counter++]);
	}
	EXPECT_EQ(5, counter);

	counter = 0;
	for (TestingType& b : arr)
	{
		EXPECT_EQ(b, origarr[counter++]);
	}
	EXPECT_EQ(5, counter);

	counter = 0;
	for (auto iter = arr.begin(); iter != arr.end(); ++iter)
	{
		EXPECT_EQ(*iter, origarr[counter++]);
	}
	EXPECT_EQ(5, counter);
}


TEST(InteratorsTest, ArrayTest5)
{
	THTestArray arr;
	THTestArray origarr;

	for (char i = 0; i < 5; i++)
	{
		TestingType v = TestingType{ 'a' + i };
		arr.AddValue(v);
		origarr.AddValue(v);
	}

	TestingType vv = TestingType{ 'd'};
	THTestArray::iterator iter = std::find(arr.begin(), arr.end(), vv);
	EXPECT_EQ(vv, *iter);

	vv = TestingType{ 10 };
	iter = std::find(arr.begin(), arr.end(), vv);
	EXPECT_EQ(arr.end(), iter);

	EXPECT_EQ(TestingType{ 'a'}, *(arr.begin()));
	std::reverse(arr.begin(), arr.end());
	EXPECT_EQ(TestingType{ 'a' + 4 }, *(arr.begin()));

	std::fill(arr.begin(), arr.end(), TestingType{ 3 });

}

bool constIteratorFind(const THTestArray& cont, TestingType& tofind)
{
	for (auto iter = cont.cbegin(); iter != cont.cend(); ++iter)
	{
		if (*iter == tofind) return true;
	}
	return false;
}

TEST(InteratorsTest, ArrayTest6)
{
	THTestArray arr;
	THTestArray origarr;

	for (char i = 0; i < 5; i++)
	{
		TestingType v = TestingType{ 'a' + i };
		arr.AddValue(v);
		origarr.AddValue(v);
	}

	TestingType vv = TestingType{ 'd' };

	EXPECT_TRUE(constIteratorFind(arr, vv));
	THTestArray::const_iterator citer = arr.cbegin();
	
	vv = TestingType{ 10 };
	EXPECT_FALSE(constIteratorFind(arr, vv));

	citer = arr.cbegin();
	EXPECT_EQ(TestingType{ 'a' }, *(citer));
}

TEST(InteratorsTest, HashTest1)
{
	TestHash hash;

	THash<TestingType1, TestingType2>::iterator b = hash.begin();
	THash<TestingType1, TestingType2>::iterator e = hash.end();
	EXPECT_EQ(b, e);

	TestingType1 k = TestingType1{ 1 };
	TestingType2 v = TestingType2{ 2 };
	hash.SetValue(k, v);

	auto bb = hash.begin();
	auto ee = hash.end();
	EXPECT_NE(bb, ee);
	
	EXPECT_EQ(k, (*bb).first);
	EXPECT_EQ(v, (*bb).second);

	++bb;
	EXPECT_EQ(bb, ee);

	++bb;
	EXPECT_EQ(bb, ee);

	++ee;
	EXPECT_EQ(bb, ee);

	--bb;
	--ee;
	EXPECT_EQ(bb, ee);
	EXPECT_EQ(k, (*bb).first);
	EXPECT_EQ(v, (*bb).second);
	EXPECT_EQ(k, (*ee).first);
	EXPECT_EQ(v, (*ee).second);

}

TEST(InteratorsTest, HashTest2)
{
	TestHash hash;
	TestHash orighash;

	for (char i = 0; i < 5; i++)
	{
		TestingType1 k = TestingType1{ 'a' + i };
		TestingType2 v = TestingType2{'a' + i + 10};
		hash.SetValue(k, v);
		orighash.SetValue(k, v);
	}

	char counter = 0;
	for (auto iter = hash.begin(); iter != hash.end(); ++iter)
	{
		EXPECT_EQ(TestingType1{ 'a' + counter }, (*iter).first);
		EXPECT_EQ(TestingType2{ 'a' + counter + 10 }, (*iter).second);
		counter++;
	}

	counter = 0;
	for (TestHash::iterator::value_type a: hash)
	{
		EXPECT_EQ(TestingType1{ 'a' + counter }, a.first);
		EXPECT_EQ(TestingType2{ 'a' + counter + 10 }, a.second);
		counter++;
	}

	counter = 0;
	for(auto a: hash)
	{
		EXPECT_EQ(TestingType1{ 'a' + counter }, a.first);
		EXPECT_EQ(TestingType2{ 'a' + counter + 10 }, a.second);
		counter++;
	}

}

TEST(InteratorsTest, HashTest3)
{
	TestHash hash;
	TestHash orighash;

	for (char i = 0; i < 5; i++)
	{
		TestingType1 k = TestingType1{ 'a' + i };
		TestingType2 v = TestingType2{ 'a' + i + 10 };
		hash.SetValue(k, v);
		orighash.SetValue(k, v);
	}
	auto tt1 = TestingType1{ 'a' + 3 };
	auto tt2 = TestingType2{ 'a' + 13 };
	auto vv = TestHash::iterator::value_type{ tt1, tt2 };
	TestHash::iterator iter = std::find(hash.begin(), hash.end(), vv);
	EXPECT_EQ(vv, *iter);

	tt1 = TestingType1{ 'a' + 3 };
	tt2 = TestingType2{ 'a' + 12 };
	vv = TestHash::iterator::value_type{ tt1, tt2 };
	iter = std::find(hash.begin(), hash.end(), vv);
	EXPECT_EQ(hash.end(), iter);
}
