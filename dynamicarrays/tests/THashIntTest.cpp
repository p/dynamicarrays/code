#include "gtest/gtest.h"
#include "DynamicArrays.h"

typedef int TestingType1;
typedef int TestingType2;
typedef THash<TestingType1, TestingType2> TestHash;

TEST(THashInt, EmptyHash1)
{
	TestHash hash;

	EXPECT_EQ(hash.Count(), 0u);
	EXPECT_EQ(hash.GetKeys().Count(), 0u);
	EXPECT_EQ(hash.GetValues().Count(), 0u);
	//EXPECT_NE(hash.GetKeys(), nullptr);
	//EXPECT_NE(hash.GetValues(), nullptr);
	EXPECT_NO_THROW(hash.Clear());
	EXPECT_NO_THROW(hash.ClearMem());
}

TEST(THashInt, EmptyHash2)
{
	TestHash hash;

	EXPECT_THROW(hash.GetKey(0), THArrayException);
	EXPECT_THROW(hash.GetKey(1), THArrayException);
	EXPECT_THROW(hash.GetValue(3), THArrayException);
	EXPECT_THROW(hash.GetValue(1000), THArrayException);
	EXPECT_NO_THROW(hash.Delete(3));
	EXPECT_NO_THROW(hash.Delete(888));
	EXPECT_EQ(hash.GetValuePointer(0), nullptr);
	EXPECT_EQ(hash.GetValuePointer(44), nullptr);
}

TEST(THashInt, EmptyIndexOf)
{
	TestHash hash;

	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(1), false);
	EXPECT_EQ(hash.IfExists(1234), false);
}

TEST(THashInt, SetValue)
{
	TestHash hash;
	hash.SetValue(1, 2);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(2), false);
	
	EXPECT_EQ(hash.GetValue(1), 2);
	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);

	hash.SetValue(1111111, 2);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(2), false);
	EXPECT_EQ(hash.IfExists(111111), false);
	EXPECT_EQ(hash.IfExists(1111111), true);
	EXPECT_EQ(hash.GetValue(1), 2);
	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);

	EXPECT_EQ(hash.Count(), 2u);
	hash.SetValue(1, 5);
	EXPECT_EQ(hash.Count(), 2u);

	EXPECT_EQ(hash.IfExists(1), true);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(2), false);
	EXPECT_EQ(hash.IfExists(111111), false);
	EXPECT_EQ(hash.IfExists(1111111), true);
	EXPECT_EQ(hash.GetValue(1), 5);
	EXPECT_EQ(hash.GetValue(1111111), 2);
	EXPECT_THROW(hash.GetValue(0), THArrayException);
	EXPECT_THROW(hash.GetValue(2), THArrayException);
}

TEST(THashInt, SetValueIntString)
{
	THash<int, std::string> hash;

	for (int i = 0; i < 100; i++)
	{
		hash.SetValue(2*i, std::to_string(2*i) + "_" + std::to_string(i+1));
	}
	
	for (int i = 0; i < 100; i++)
	{
		EXPECT_EQ(hash.GetValue(2*i), std::to_string(2 * i) + "_" + std::to_string(i + 1));
	}

	EXPECT_EQ(hash.IfExists(0), true);
	EXPECT_EQ(hash.IfExists(2), true);
	EXPECT_EQ(hash.IfExists(12), true);
	EXPECT_EQ(hash.IfExists(1), false);
	EXPECT_EQ(hash.IfExists(99), false);
	EXPECT_EQ(hash.IfExists(100), true);
	EXPECT_EQ(hash.IfExists(99*2), true);
	EXPECT_EQ(hash.IfExists(98*2), true);
	EXPECT_EQ(hash.IfExists(100*2), false);

	EXPECT_EQ(hash.IfExists(99*2), true);
	hash.Delete(99*2);
	EXPECT_EQ(hash.IfExists(99*2), false);

	EXPECT_EQ(hash.IfExists(34), true);
	hash.Delete(34);
	EXPECT_EQ(hash.IfExists(34), false);

	hash.Clear();
	EXPECT_EQ(hash.IfExists(1), false);
	EXPECT_EQ(hash.IfExists(0), false);
	EXPECT_EQ(hash.IfExists(98), false);
	EXPECT_EQ(hash.Count(), 0u);
}
