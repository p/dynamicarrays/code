#ifndef _ASSERT_H_
#define _ASSERT_H_

#include "stdio.h"
#include <exception>
#include <string>
#include <strstream>

#ifdef _DEBUG

template<class T>
inline std::string toString(const T& value)
{
	std::strstream o;
	o << value;
	return std::string(o.str(), o.pcount());
}

inline void _ASSERT(std::string cond, std::string filename, int line)
//inline void _ASSERT(char* cond, char* filename, int line)
{
	char str[1024];
	sprintf(str, "Assert triggered ('%s') in the file %s(%d)", cond.c_str(), filename.c_str(), line);
	throw std::exception(str);
};

inline void _ASSERT_EQUAL(std::string expected, std::string actual, char* filename, int line)
{
	char str[1024];
	sprintf(str, "Expected value '%s' but was '%s' in the file %s(%d)", 
		expected.c_str(), actual.c_str(), filename, line);
	throw std::exception(str);
};

#define ASSERT(condition) if(condition){}else{ _ASSERT(#condition, __FILE__, __LINE__); }
#define ASSERT_EQUAL(expected, actual) if((expected) != (actual)){_ASSERT_EQUAL(toString(expected), toString(actual), __FILE__, __LINE__); }
#define ASSERT_MESSAGE(message, condition)if(condition){}else{ _ASSERT(toString(message), __FILE__, __LINE__); }

#else

#define ASSERT
#define ASSERT_EQUAL
#define ASSERT_MESSAGE

#endif //_DEBUG

#endif //_ASSERT_H_
