#include "gtest/gtest.h"
#include "DynamicArrays.h"

typedef int TestingType;
typedef THArraySorted<TestingType> THTestArray;

TEST(THArraySortedInt, EmptyArray1)
{
	THTestArray arr;

	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
	EXPECT_NO_THROW(arr.Clear());
	EXPECT_NO_THROW(arr.ClearMem());
	EXPECT_NO_THROW(arr.Hold());
}

TEST(THArraySortedInt, EmptyArray2)
{
	THTestArray arr;

	EXPECT_THROW(arr.GetValue(0), THArrayException);
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(1000), THArrayException);
	EXPECT_THROW(arr.DeleteValue(0), THArrayException);
	EXPECT_THROW(arr.DeleteValue(1), THArrayException);
	EXPECT_THROW(arr.DeleteValue(888), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(0), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(555), THArrayException);
}

TEST(THArraySortedInt, EmptyIndexOf)
{
	THTestArray arr;

	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(1), -1);
	EXPECT_EQ(arr.IndexOf(1234), -1);

	EXPECT_EQ(arr.IndexOfFrom(0, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(1, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(2, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(666, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(0, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(2, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(666, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(0, 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(1, 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(2, 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(666, 55), -1);
}

TEST(THArraySortedInt, AddValue)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue(3), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), 3);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, 3);
	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(11), THArrayException);
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	EXPECT_EQ(arr.IndexOf(3), 0);
	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(1), -1);
	EXPECT_EQ(arr.IndexOf(2), -1);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOfFrom(3, 0), 0);
	EXPECT_THROW(arr.IndexOfFrom(3, 1), THArrayException); // exception because Start=1 is out of bounds.
	EXPECT_THROW(arr.IndexOfFrom(3, 2), THArrayException);
	EXPECT_EQ(arr.IndexOfFrom(0, 0), -1);
}

TEST(THArraySortedInt, AddValue2)
{
	THTestArray arr;
	TestingType v = 555'555'555;
	EXPECT_EQ(arr.Add(&v), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), v);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, v);

	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(v), THArrayException);

	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);
	EXPECT_EQ(arr.IndexOf(v), 0);
	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(1), -1);
	EXPECT_EQ(arr.IndexOf(2), -1);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOfFrom(v, 0), 0);
	EXPECT_THROW(arr.IndexOfFrom(v, 1), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v, 2), THArrayException);
	EXPECT_EQ(arr.IndexOfFrom(0, 0), -1);
}

TEST(THArraySortedInt, AddValue3)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue(3), 0u);
	EXPECT_EQ(arr.AddValue(99), 1u);
	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), 3);
	EXPECT_EQ(arr.GetValue(1), 99);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, 3);
	vv = arr.GetValuePointer(1);
	EXPECT_EQ(*vv, 99);
	EXPECT_THROW(arr.GetValuePointer(2), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(6), THArrayException);

	EXPECT_THROW(arr.GetValue(2), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);
	EXPECT_EQ(arr.IndexOf(3), 0);
	EXPECT_EQ(arr.IndexOf(99), 1);
	EXPECT_EQ(arr.IndexOf(98), -1);
	EXPECT_EQ(arr.IndexOf(100), -1);
	EXPECT_EQ(arr.IndexOf(2), -1);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOfFrom(3, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(3, 1), -1);
	EXPECT_THROW(arr.IndexOfFrom(3, 2), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(3, 3), THArrayException);
	EXPECT_EQ(arr.IndexOfFrom(99, 0), 1);
	EXPECT_EQ(arr.IndexOfFrom(99, 1), 1);
	EXPECT_THROW(arr.IndexOfFrom(99, 2), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(99, 99), THArrayException);
}

TEST(THArraySortedInt, AddValue4)
{
	THTestArray arr;
	TestingType v1 = 333;
	TestingType v2 = 555'555'555;
	EXPECT_EQ(arr.Add(&v2), 0u);
	EXPECT_EQ(arr.Add(&v1), 0u);

	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);

	EXPECT_EQ(arr.GetValue(1), v2);
	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_THROW(arr.GetValue(2), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.IndexOf(v2), 1);
	EXPECT_EQ(arr.IndexOf(-1), -1);
	EXPECT_EQ(arr.IndexOf(-333), -1);
	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(1), -1);
	EXPECT_EQ(arr.IndexOf(2), -1);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOfFrom(v1, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(v1, 1), -1);
	EXPECT_THROW(arr.IndexOfFrom(v1, 2), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v1, 3), THArrayException);
	EXPECT_EQ(arr.IndexOfFrom(v2, 0), 1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 1), 1);
	EXPECT_THROW(arr.IndexOfFrom(v2, 2), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(v2, 3), THArrayException);

}

TEST(THArraySortedInt, AddValue5)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue(8), 0u);
	EXPECT_EQ(arr.AddValue(88), 1u);
	EXPECT_EQ(arr[0], 8);
	EXPECT_EQ(arr[1], 88);
	EXPECT_THROW(arr[3], THArrayException);
	EXPECT_THROW(arr[88], THArrayException);
}

TEST(THArraySortedInt, AddValue6)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue(3), 0u);
	EXPECT_EQ(arr.Count(), 1u); 
	EXPECT_EQ(arr.Capacity(), 4u);
	EXPECT_EQ(arr.IndexOf(3), 0); 
	EXPECT_EQ(arr.GetValue(0), 3);

	EXPECT_EQ(arr.AddValue(99), 1u);
	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.GetValue(0), 3);
	EXPECT_EQ(arr.GetValue(1), 99);

	EXPECT_EQ(arr.AddValue(2), 0u);
	EXPECT_EQ(arr.Count(), 3u);
	EXPECT_EQ(arr.GetValue(0), 2);
	EXPECT_EQ(arr.GetValue(1), 3); 
	EXPECT_EQ(arr.GetValue(2), 99);

	EXPECT_EQ(arr.AddValue(3), 1u);
	EXPECT_EQ(arr.Count(), 4u);
	EXPECT_EQ(arr.GetValue(0), 2);
	EXPECT_EQ(arr.GetValue(1), 3);
	EXPECT_EQ(arr.GetValue(2), 3);
	EXPECT_EQ(arr.GetValue(3), 99);


	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, 2);
	vv = arr.GetValuePointer(1);
	EXPECT_EQ(*vv, 3);
	vv = arr.GetValuePointer(2);
	EXPECT_EQ(*vv, 3);
	vv = arr.GetValuePointer(3);
	EXPECT_EQ(*vv, 99);

	EXPECT_THROW(arr.GetValuePointer(4), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(99), THArrayException);

	EXPECT_THROW(arr.GetValue(4), THArrayException);
	EXPECT_THROW(arr.GetValue(5), THArrayException);
	
	EXPECT_EQ(arr.IndexOf(99), 3);
	EXPECT_EQ(arr.IndexOf(98), -1);
	EXPECT_EQ(arr.IndexOf(100), -1);
	EXPECT_EQ(arr.IndexOf(2), 0);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOf(3), 2);
	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(-1), -1);
	EXPECT_EQ(arr.IndexOf(-2), -1); 
	EXPECT_EQ(arr.IndexOf(-3), -1);
	EXPECT_EQ(arr.IndexOf(-99), -1);

	EXPECT_EQ(arr.IndexOfFrom(-1, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(-1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(-1, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(-1, 3), -1);
	EXPECT_THROW(arr.IndexOfFrom(-1, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(-1, 11), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(2, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(2, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(2, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(2, 3), -1);
	EXPECT_THROW(arr.IndexOfFrom(2, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(2, 5), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(3, 0), 2);
	EXPECT_EQ(arr.IndexOfFrom(3, 1), 2);
	EXPECT_EQ(arr.IndexOfFrom(3, 2), 2);
	EXPECT_EQ(arr.IndexOfFrom(3, 3), -1);
	EXPECT_THROW(arr.IndexOfFrom(3, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(3, 5), THArrayException);

	EXPECT_EQ(arr.IndexOfFrom(99, 0), 3);
	EXPECT_EQ(arr.IndexOfFrom(99, 1), 3);
	EXPECT_EQ(arr.IndexOfFrom(99, 2), 3);
	EXPECT_EQ(arr.IndexOfFrom(99, 3), 3);
	EXPECT_THROW(arr.IndexOfFrom(99, 4), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(99, 5), THArrayException);
	EXPECT_THROW(arr.IndexOfFrom(99, 99), THArrayException);
}


TEST(THArraySortedInt, ArayCopy)
{
	THTestArray arr1, arr2;

	arr1.AddValue(7);
	arr1.AddValue(77);

	arr2.AddValue(6);
	arr2.AddValue(66);

	arr1 = arr2;

	EXPECT_EQ(arr1.Count(), 2u);
	EXPECT_EQ(arr1.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr1.Capacity(), 2u);
	//EXPECT_NE(arr1.Memory(), nullptr);

	EXPECT_EQ(arr2.Count(), 2u);
	EXPECT_EQ(arr2.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr2.Capacity(), 4u);
	//EXPECT_NE(arr2.Memory(), nullptr);

	EXPECT_EQ(arr1[0], 6);
	EXPECT_EQ(arr1[1], 66);
	EXPECT_THROW(arr1[3], THArrayException);
	EXPECT_THROW(arr1[4], THArrayException);
	EXPECT_THROW(arr1[5], THArrayException);

	EXPECT_EQ(arr2[0], 6);
	EXPECT_EQ(arr2[1], 66);
	EXPECT_THROW(arr2[3], THArrayException);
	EXPECT_THROW(arr2[4], THArrayException);
	EXPECT_THROW(arr2[5], THArrayException);
}

TEST(THArraySortedInt, Capacity1)
{
	THTestArray arr;

	arr.SetCapacity(500);
	EXPECT_EQ(arr.Capacity(), 500u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(600);
	EXPECT_EQ(arr.Capacity(), 600u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(400);
	EXPECT_EQ(arr.Capacity(), 400u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(0);
	EXPECT_EQ(arr.Capacity(), 0u);
	EXPECT_EQ(arr.Count(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);

	//int v = 2'000'000'000;
	//arr.SetCapacity(v); // try to allocate 2G memory
	//EXPECT_EQ(arr.Capacity(), v);
	//EXPECT_EQ(arr.Count(), 0);

}

TEST(THArraySortedInt, Capacity2)
{
	THTestArray arr;

	for (uint i = 0; i < 500; i++)
	{
		arr.AddValue(i + 777);
	}

	EXPECT_GT(arr.Capacity(), 500u);

	arr.Hold();
	EXPECT_EQ(arr.Capacity(), 500u);

	arr.SetCapacity(600);
	EXPECT_EQ(arr.Capacity(), 600u);
	EXPECT_EQ(arr.Count(), 500u);
	arr.SetCapacity(400);
	EXPECT_EQ(arr.Capacity(), 400u);
	EXPECT_EQ(arr.Count(), 400u);
	arr.SetCapacity(0);
	EXPECT_EQ(arr.Capacity(), 0u);
	EXPECT_EQ(arr.Count(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
}

TEST(THArraySortedInt, Grow)
{
	THTestArray arr;

	EXPECT_EQ(arr.Capacity(), 0u);
	arr.AddValue(11);
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue(22);
	arr.AddValue(33);
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue(44);
	arr.AddValue(11);
	EXPECT_EQ(arr.Capacity(), 8u);
	arr.AddValue(55);
	arr.AddValue(66);
	arr.AddValue(77);
	EXPECT_EQ(arr.Capacity(), 8u);
	arr.AddValue(22);
	EXPECT_EQ(arr.Capacity(), 12u);
	EXPECT_EQ(arr.Count(), 9u);

	for (uint i = 0; i < 4; i++)
	{
		arr.AddValue(i + 11);
	}
	EXPECT_EQ(arr.Capacity(), 28u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 29u);
	EXPECT_EQ(arr.Capacity(), 44u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 45u);
	EXPECT_EQ(arr.Capacity(), 60u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 61u);
	EXPECT_EQ(arr.Capacity(), 76u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 77u);
	EXPECT_EQ(arr.Capacity(), 95u);

	for (uint i = 0; i < 19; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 96u);
	EXPECT_EQ(arr.Capacity(), 118u);

	arr.Clear();
	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.Capacity(), 118u);

	arr.GrowTo(100);
	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.Capacity(), 118u);
}

TEST(THArraySortedInt, Zero)
{
	THTestArray arr;

	for (int i = 2; i < 35; i++)
	{
		arr.AddValue(i);
	}

	EXPECT_EQ(arr.Count(), 33u);

	for (int i = 0; i < (int)arr.Count(); i++)
	{
		EXPECT_EQ(arr[i], i + 2);
	}

	arr.Zero();

	EXPECT_EQ(arr.Count(), 33u);

	for (uint i = 0; i < arr.Count(); i++)
	{
		EXPECT_EQ(arr[i], 0);
	}
}


TEST(THArraySortedInt, InsertValue1)
{
	THTestArray arr;

	for (int i = 0; i < 100; i++)
	{
		arr.AddValue(i + 1000);
	}

	EXPECT_EQ(arr.Count(), 100u);

	for (int i = 0; i < 100; i++)
	{
		EXPECT_EQ(arr[i], i + 1000);
	}
}

TEST(THArraySortedInt, InsertValue2)
{
	THTestArray arr;

	for (int i = 0; i < 100; i++)
	{
		arr.AddValue(1000 - i);
	}

	EXPECT_EQ(arr.Count(), 100u);

	for (int i = 0; i < 100; i++)
	{
		EXPECT_EQ(arr[i], 1000 - 99 + i);
	}
}


TEST(THArraySortedInt, DeleteValue1)
{
	THTestArray arr;

	for (int i = 0; i < 10; i++)
	{
		arr.AddValue(i + 1);
	}

	EXPECT_EQ(arr.Count(), 10u);
	arr.DeleteValue(0);
	EXPECT_EQ(arr.Count(), 9u);
	for (int i = 0; i < 9; i++)
	{
		EXPECT_EQ(arr[i], i + 2);
	}

	EXPECT_THROW(arr.DeleteValue(arr.Count()), THArrayException); // try to delete out of range element
	EXPECT_EQ(arr.Count(), 9u);

	arr.DeleteValue(arr.Count() - 1); //try to delete last element in the array
	EXPECT_EQ(arr.Count(), 8u);
	for (int i = 0; i < 8; i++)
	{
		EXPECT_EQ(arr[i], i + 2);
	}
}


