#include "gtest/gtest.h"
#include "DynamicArrays.h"

class StringToArrayParamTest : public testing::TestWithParam<std::tuple<std::string, char, int, std::string> > 
{
protected:
    THArrayString arr;
    void SetUp() override
    {
       // arr.Clear();
    }

    // You can implement all the usual fixture class members here.
    // To access the test parameter, call GetParam() from class
    // TestWithParam<T>.

};

TEST_P(StringToArrayParamTest, StringToArrayTest) // Inside a test, access the test parameter with the GetParam() method of the TestWithParam<T> class:
{
    std::string initial, result;
    char delim;
    uint cnt;
    std::tie(initial, delim, cnt, result) = GetParam();

    StringToArray(initial, arr, delim);

    std::string res = toString(arr);
    EXPECT_EQ(arr.Count(), cnt);
    EXPECT_EQ(res, result);
};

//TEST_P(StringToArrayParamTest, StringToArrayTest2) // Inside a test, access the test parameter with the GetParam() method of the TestWithParam<T> class:
//{
//    std::string initial, result;
//    char delim;
//    uint cnt;
//    std::tie(initial, delim, cnt, result) = GetParam();
//
//    StringToArray(initial, arr, delim);
//
//    std::string res = toString(arr);
//    EXPECT_EQ(arr.Count(), cnt);
//    EXPECT_EQ(res, result);
//};


INSTANTIATE_TEST_CASE_P(SimpleStrToArr1, StringToArrayParamTest, testing::Values(std::make_tuple("", '\n', 0, "")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr2, StringToArrayParamTest, testing::Values(std::make_tuple("\n", '\n', 0, "")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr3, StringToArrayParamTest, testing::Values(std::make_tuple(" ", '\n', 1, " ")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr4, StringToArrayParamTest, testing::Values(std::make_tuple(" \n", '\n', 1, " ")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr5, StringToArrayParamTest, testing::Values(std::make_tuple("\n ", '\n', 1, " ")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr6, StringToArrayParamTest, testing::Values(std::make_tuple(",,,,,", ',', 0, "")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr7, StringToArrayParamTest, testing::Values(std::make_tuple(" ,,,,, ", ',', 2, "  ")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr8, StringToArrayParamTest, testing::Values(std::make_tuple("1,2,3", ',', 3, "123")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr9, StringToArrayParamTest, testing::Values(std::make_tuple("1,,2,,3,", ',', 3, "123")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr10, StringToArrayParamTest, testing::Values(std::make_tuple(",1,2,,3", ',', 3, "123")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr11, StringToArrayParamTest, testing::Values(std::make_tuple(",,1,,2,3", ',', 3, "123")));

INSTANTIATE_TEST_CASE_P(SimpleStrToArr12, StringToArrayParamTest, testing::Values(std::make_tuple(",,,1,2,,,3,", ',', 3, "123")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr13, StringToArrayParamTest, testing::Values(std::make_tuple("1,2,,3,", ',', 3, "123")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr14, StringToArrayParamTest, testing::Values(std::make_tuple("1,,2,3,,,,", ',', 3, "123")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr15, StringToArrayParamTest, testing::Values(std::make_tuple("12,34,56", ',', 3, "123456")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr16, StringToArrayParamTest, testing::Values(std::make_tuple("1,,23,5,", ',', 3, "1235")));

INSTANTIATE_TEST_CASE_P(SimpleStrToArr17, StringToArrayParamTest, testing::Values(std::make_tuple(",123,4,55", ',', 3, "123455")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr18, StringToArrayParamTest, testing::Values(std::make_tuple("1,,23,5,", '2', 2, "1,,3,5,")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr19, StringToArrayParamTest, testing::Values(std::make_tuple("1,,23,5,", ' ', 1, "1,,23,5,")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr20, StringToArrayParamTest, testing::Values(std::make_tuple("1,,23,5", '5', 1, "1,,23,")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr21, StringToArrayParamTest, testing::Values(std::make_tuple("\n\n235", '\n', 1, "235")));
INSTANTIATE_TEST_CASE_P(SimpleStrToArr22, StringToArrayParamTest, testing::Values(std::make_tuple("1235\n\n", '\n', 1, "1235")));


class CompareNCaseParamTest : public testing::TestWithParam<std::tuple<std::string, std::string, bool, int> >
{
protected:
    //THArrayString arr;
    void SetUp() override
    {
        // arr.Clear();
    }

    // You can implement all the usual fixture class members here.
    // To access the test parameter, call GetParam() from class
    // TestWithParam<T>.

};

TEST_P(CompareNCaseParamTest, EqualNCaseTest) // Inside a test, access the test parameter with the GetParam() method of the TestWithParam<T> class:
{
    std::string str1, str2;
    bool res1;
    int res2;

    std::tie(str1, str2, res1, res2) = GetParam();

    EXPECT_EQ(EqualNCase(str1, str2), res1);
};

TEST_P(CompareNCaseParamTest, CompareNCaseTest) // Inside a test, access the test parameter with the GetParam() method of the TestWithParam<T> class:
{
    std::string str1, str2;
    bool res1;
    int res2;

    std::tie(str1, str2, res1, res2) = GetParam();

    EXPECT_EQ(CompareNCase(str1, str2), res2);
};


INSTANTIATE_TEST_CASE_P(CompareNCaseTestData1, CompareNCaseParamTest, testing::Values(std::make_tuple("", "", true, 0)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData2, CompareNCaseParamTest, testing::Values(std::make_tuple("f", "", false, 1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData3, CompareNCaseParamTest, testing::Values(std::make_tuple("", "5", false, -1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData4, CompareNCaseParamTest, testing::Values(std::make_tuple("\n", "\r", false, -1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData5, CompareNCaseParamTest, testing::Values(std::make_tuple("", " ", false, -1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData6, CompareNCaseParamTest, testing::Values(std::make_tuple(" ", "", false, 1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData7, CompareNCaseParamTest, testing::Values(std::make_tuple("  ", "", false, 1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData8, CompareNCaseParamTest, testing::Values(std::make_tuple("  ", "", false, 1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData9, CompareNCaseParamTest, testing::Values(std::make_tuple("\n\n", "\n", false, 1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData10, CompareNCaseParamTest, testing::Values(std::make_tuple("\n\r", "\n", false, 1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData11, CompareNCaseParamTest, testing::Values(std::make_tuple("\r\n", "\n", false, 1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData12, CompareNCaseParamTest, testing::Values(std::make_tuple("abc", "ab", false, 1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData13, CompareNCaseParamTest, testing::Values(std::make_tuple("ab", "abc", false, -1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData14, CompareNCaseParamTest, testing::Values(std::make_tuple(" ab", "ab", false, -1)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData15, CompareNCaseParamTest, testing::Values(std::make_tuple("ab\n", "ab", false, 1)));

INSTANTIATE_TEST_CASE_P(CompareNCaseTestData16, CompareNCaseParamTest, testing::Values(std::make_tuple(" ", " ", true, 0)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData17, CompareNCaseParamTest, testing::Values(std::make_tuple("\r", "\r", true, 0)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData18, CompareNCaseParamTest, testing::Values(std::make_tuple("\n", "\n", true, 0)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData19, CompareNCaseParamTest, testing::Values(std::make_tuple("AbC", "AbC", true, 0)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData20, CompareNCaseParamTest, testing::Values(std::make_tuple("����", "����", true, 0)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData21, CompareNCaseParamTest, testing::Values(std::make_tuple("�", "�", true, 0)));
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData22, CompareNCaseParamTest, testing::Values(std::make_tuple("\0", "\0", true, 0)));

INSTANTIATE_TEST_CASE_P(CompareNCaseTestData23, CompareNCaseParamTest, testing::Values(std::make_tuple("�", "A", false, 1))); // non-latin and latin symbols
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData24, CompareNCaseParamTest, testing::Values(std::make_tuple("�FG", "�FG", true, 0))); // mix of non-latin and latin symbols
INSTANTIATE_TEST_CASE_P(CompareNCaseTestData25, CompareNCaseParamTest, testing::Values(std::make_tuple("�FG", "�FR", false, -1))); // non-latin and latin symbols


