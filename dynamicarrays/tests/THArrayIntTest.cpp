#include "gtest/gtest.h"
#include "DynamicArrays.h"

typedef int TestingType;
typedef THArray<TestingType> THTestArray;

TEST(THArrayInt, EmptyArray1) 
{
	THTestArray arr;

	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
	EXPECT_NO_THROW(arr.Clear());
	EXPECT_NO_THROW(arr.ClearMem());
	EXPECT_NO_THROW(arr.Hold());
	EXPECT_NO_THROW(arr.Reverse());
	EXPECT_NO_THROW(arr.Reverse(5));
}

TEST(THArrayInt, EmptyArray2)
{
	THTestArray arr;

	EXPECT_THROW(arr.GetValue(0), THArrayException);
	EXPECT_THROW(arr.GetValue(1000), THArrayException);
	EXPECT_THROW(arr.DeleteValue(0), THArrayException);
	EXPECT_THROW(arr.DeleteValue(888), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(0), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(555), THArrayException);
	EXPECT_THROW(arr.Pop(), THArrayException);
}

TEST(THArrayInt, EmptyIndexOf)
{
	THTestArray arr;

	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(1), -1);
	EXPECT_EQ(arr.IndexOf(1234), -1);

	EXPECT_EQ(arr.IndexOfFrom(0, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(1, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(2, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(666, 0), -1);
	EXPECT_EQ(arr.IndexOfFrom(0, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(2, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(666, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(0, 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(1, 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(2, 55), -1);
	EXPECT_EQ(arr.IndexOfFrom(666, 55), -1);
}

TEST(THArrayInt, AddValue)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue(3), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), 3);

	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, 3);
	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(11), THArrayException);
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);
	
	EXPECT_EQ(arr.IndexOf(3),  0);
	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(1), -1);
	EXPECT_EQ(arr.IndexOf(2), -1);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOfFrom(3, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(3, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(3, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(0, 0), -1);
}

TEST(THArrayInt, AddValue2)
{
	THTestArray arr;
	TestingType v = 555'555'555;
	EXPECT_EQ(arr.Add(&v), 0u);
	EXPECT_EQ(arr.Count(), 1u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), v);
	
	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, v);

	EXPECT_THROW(arr.GetValuePointer(1), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(v), THArrayException);
	
	EXPECT_THROW(arr.GetValue(1), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);
	EXPECT_EQ(arr.IndexOf(v), 0);
	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(1), -1);
	EXPECT_EQ(arr.IndexOf(2), -1);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOfFrom(v, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(v, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(v, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(0, 0), -1);
}

TEST(THArrayInt, AddValue3)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue(3), 0u);
	EXPECT_EQ(arr.AddValue(99), 1u);
	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);
	EXPECT_EQ(arr.GetValue(0), 3);
	EXPECT_EQ(arr.GetValue(1), 99);
	
	TestingType* vv = arr.GetValuePointer(0);
	EXPECT_EQ(*vv, 3);
	vv = arr.GetValuePointer(1);
	EXPECT_EQ(*vv, 99);
	EXPECT_THROW(arr.GetValuePointer(2), THArrayException);
	EXPECT_THROW(arr.GetValuePointer(6), THArrayException);

	EXPECT_THROW(arr.GetValue(2), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);
	EXPECT_EQ(arr.IndexOf(3), 0);
	EXPECT_EQ(arr.IndexOf(99), 1);
	EXPECT_EQ(arr.IndexOf(98), -1);
	EXPECT_EQ(arr.IndexOf(100), -1);
	EXPECT_EQ(arr.IndexOf(2), -1);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOfFrom(3, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(3, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(3, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(3, 3), -1);
	EXPECT_EQ(arr.IndexOfFrom(99, 0), 1);
	EXPECT_EQ(arr.IndexOfFrom(99, 1), 1);
	EXPECT_EQ(arr.IndexOfFrom(99, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(99, 99), -1);
}

TEST(THArrayInt, AddValue4)
{
	THTestArray arr;
	TestingType v1 = 333;
	TestingType v2 = 555'555'555;
	EXPECT_EQ(arr.Add(&v1), 0u);
	EXPECT_EQ(arr.Add(&v2), 1u);

	EXPECT_EQ(arr.Count(), 2u);
	EXPECT_EQ(arr.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr.Capacity(), 4u);
	//EXPECT_NE(arr.Memory(), nullptr);

	EXPECT_EQ(arr.GetValue(0), v1);
	EXPECT_EQ(arr.GetValue(1), v2);
	EXPECT_THROW(arr.GetValue(2), THArrayException);
	EXPECT_THROW(arr.GetValue(10), THArrayException);

	EXPECT_EQ(arr.IndexOf(v1), 0);
	EXPECT_EQ(arr.IndexOf(v2), 1);
	EXPECT_EQ(arr.IndexOf(0), -1);
	EXPECT_EQ(arr.IndexOf(1), -1);
	EXPECT_EQ(arr.IndexOf(2), -1);
	EXPECT_EQ(arr.IndexOf(4), -1);
	EXPECT_EQ(arr.IndexOfFrom(v1, 0), 0);
	EXPECT_EQ(arr.IndexOfFrom(v1, 1), -1);
	EXPECT_EQ(arr.IndexOfFrom(v1, 2), -1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 0), 1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 1), 1);
	EXPECT_EQ(arr.IndexOfFrom(v2, 2), -1);
}

TEST(THArrayInt, AddValue5)
{
	THTestArray arr;

	EXPECT_EQ(arr.AddValue(8), 0u);
	EXPECT_EQ(arr.AddValue(88), 1u);
	EXPECT_EQ(arr[0], 8);
	EXPECT_EQ(arr[1], 88);
	EXPECT_THROW(arr[3], THArrayException);
	EXPECT_THROW(arr[88], THArrayException);
}

TEST(THArrayInt, ArayCopy)
{
	THTestArray arr1, arr2;

	arr1.AddValue(7);
	arr1.AddValue(77);

	arr2.AddValue(6);
	arr2.AddValue(66);

	arr1 = arr2;

	EXPECT_EQ(arr1.Count(), 2u);
	EXPECT_EQ(arr1.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr1.Capacity(), 2u);
	//EXPECT_NE(arr1.Memory(), nullptr);

	EXPECT_EQ(arr2.Count(), 2u);
	EXPECT_EQ(arr2.ItemSize(), sizeof(TestingType));
	EXPECT_EQ(arr2.Capacity(), 4u);
	//EXPECT_NE(arr2.Memory(), nullptr);

	EXPECT_EQ(arr1[0], 6);
	EXPECT_EQ(arr1[1], 66);
	EXPECT_THROW(arr1[3], THArrayException);
	EXPECT_THROW(arr1[4], THArrayException);
	EXPECT_THROW(arr1[5], THArrayException);

	EXPECT_EQ(arr2[0], 6);
	EXPECT_EQ(arr2[1], 66);
	EXPECT_THROW(arr2[3], THArrayException);
	EXPECT_THROW(arr2[4], THArrayException);
	EXPECT_THROW(arr2[5], THArrayException);
}

TEST(THArrayInt, Capacity1)
{
	THTestArray arr;

	arr.SetCapacity(500);
	EXPECT_EQ(arr.Capacity(), 500u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(600);
	EXPECT_EQ(arr.Capacity(), 600u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(400);
	EXPECT_EQ(arr.Capacity(), 400u);
	EXPECT_EQ(arr.Count(), 0u);
	arr.SetCapacity(0);
	EXPECT_EQ(arr.Capacity(), 0u);
	EXPECT_EQ(arr.Count(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
	
	//int v = 2'000'000'000;
	//arr.SetCapacity(v); // try to allocate 2G memory
	//EXPECT_EQ(arr.Capacity(), v);
	//EXPECT_EQ(arr.Count(), 0);

}

TEST(THArrayInt, Capacity2)
{
	THTestArray arr;

	for (uint i = 0; i < 500; i++)
	{
		arr.AddValue(i + 777);
	}

	EXPECT_GT(arr.Capacity(), 500u);
	
	arr.Hold();
	EXPECT_EQ(arr.Capacity(), 500u);
	
	arr.SetCapacity(600);
	EXPECT_EQ(arr.Capacity(), 600u);
	EXPECT_EQ(arr.Count(), 500u);
	arr.SetCapacity(400);
	EXPECT_EQ(arr.Capacity(), 400u);
	EXPECT_EQ(arr.Count(), 400u);
	arr.SetCapacity(0);
	EXPECT_EQ(arr.Capacity(), 0u);
	EXPECT_EQ(arr.Count(), 0u);
	//EXPECT_EQ(arr.Memory(), nullptr);
}

TEST(THArrayInt, Grow)
{
	THTestArray arr;

	EXPECT_EQ(arr.Capacity(), 0u);
	arr.AddValue(11);
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue(22);
	arr.AddValue(33);
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue(44);
	EXPECT_EQ(arr.Capacity(), 4u);
	arr.AddValue(11); 
	EXPECT_EQ(arr.Capacity(), 8u);
	arr.AddValue(55);
	arr.AddValue(66);
	arr.AddValue(77);
	EXPECT_EQ(arr.Capacity(), 8u);
	arr.AddValue(22);
	EXPECT_EQ(arr.Capacity(), 12u);
	EXPECT_EQ(arr.Count(), 9u);

	for (uint i = 0; i < 4; i++)
	{
		arr.AddValue(i+11);
	}
	EXPECT_EQ(arr.Capacity(), 28u);
	
	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 29u);
	EXPECT_EQ(arr.Capacity(), 44u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 45u);
	EXPECT_EQ(arr.Capacity(), 60u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 61u);
	EXPECT_EQ(arr.Capacity(), 76u);

	for (uint i = 0; i < 16; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 77u);
	EXPECT_EQ(arr.Capacity(), 95u);

	for (uint i = 0; i < 19; i++)
	{
		arr.AddValue(i + 11);
	}

	EXPECT_EQ(arr.Count(), 96u);
	EXPECT_EQ(arr.Capacity(), 118u);

	arr.Clear();
	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.Capacity(), 118u);
	
	arr.GrowTo(100);
	EXPECT_EQ(arr.Count(), 0u);
	EXPECT_EQ(arr.Capacity(), 118u);
}

TEST(THArrayInt, PushPop)
{
	THTestArray arr;

	arr.Push(500);
	EXPECT_EQ(arr.Capacity(), 4u);
	EXPECT_EQ(arr.Count(), 1u);

	arr.Push(600);
	EXPECT_EQ(arr.Capacity(), 4u);
	EXPECT_EQ(arr.Count(), 2u);

	arr.Push(0);
	EXPECT_EQ(arr.Capacity(), 4u);
	EXPECT_EQ(arr.Count(), 3u);

	EXPECT_EQ(arr.Pop(), 0);
	EXPECT_EQ(arr.Count(), 2u);

	EXPECT_EQ(arr.Pop(), 600);
	EXPECT_EQ(arr.Count(), 1u);

	EXPECT_EQ(arr.Pop(), 500);
	EXPECT_EQ(arr.Count(), 0u);

	EXPECT_THROW(arr.Pop(), THArrayException);

	arr.InsertValue(0, 555);
	EXPECT_EQ(arr.Pop(), 555);
	EXPECT_EQ(arr.Count(), 0u);
}

TEST(THArrayInt, PopFront)
{
	THTestArray arr;

	arr.Push(500);
	arr.Push(600);
	arr.Push(700);
	arr.InsertValue(0, 800);
	arr.InsertValue(1, 900);
	arr.InsertValue(arr.Count(), 200); //800 900 500 600 700 200
	
	EXPECT_EQ(arr.Capacity(), 8u);
	EXPECT_EQ(arr.Count(), 6u);

	EXPECT_EQ(arr[0], 800);
	EXPECT_EQ(arr[1], 900);
	EXPECT_EQ(arr[2], 500);
	EXPECT_EQ(arr[3], 600);
	EXPECT_EQ(arr[4], 700);
	EXPECT_EQ(arr[5], 200);
	
	EXPECT_EQ(arr.PopFront(), 800);
	EXPECT_EQ(arr.Count(), 5u);

	EXPECT_EQ(arr.PopFront(), 900);
	EXPECT_EQ(arr.Count(), 4u);

	EXPECT_EQ(arr.PopFront(), 500);
	EXPECT_EQ(arr.Count(), 3u);

	EXPECT_EQ(arr.PopFront(), 600);
	EXPECT_EQ(arr.Count(), 2u);

	EXPECT_EQ(arr.PopFront(), 700);
	EXPECT_EQ(arr.Count(), 1u);

	EXPECT_EQ(arr.PopFront(), 200);
	EXPECT_EQ(arr.Count(), 0u);

	EXPECT_THROW(arr.PopFront(), THArrayException);

	arr.InsertValue(0, 555);
	EXPECT_EQ(arr.PopFront(), 555);
	EXPECT_EQ(arr.Count(), 0u);
}

TEST(THArrayInt, Zero)
{
	THTestArray arr;

	for (int i = 2; i < 35; i++)
	{
		arr.InsertValue(arr.Count(), i);
	}

	EXPECT_EQ(arr.Count(), 33u);

	for (int i = 0; i < (int)arr.Count(); i++)
	{
		EXPECT_EQ(arr[i], i+2);
	}

	arr.Zero();

	EXPECT_EQ(arr.Count(), 33u);

	for (uint i = 0; i < arr.Count(); i++)
	{
		EXPECT_EQ(arr[i], 0);
	}
}


TEST(THArrayInt, InsertValue1)
{
	THTestArray arr;

	for (int i = 0; i < 100; i++)
	{
		arr.InsertValue(0, i+1000);
	}

	EXPECT_EQ(arr.Count(), 100u);

	for (int i = 0; i < 100; i++)
	{
		EXPECT_EQ(arr[i], 1099 - i);
	}
}

TEST(THArrayInt, DeleteValue1)
{
	THTestArray arr;

	for (int i = 0; i < 10; i++)
	{
		arr.AddValue(i + 1);
	}

	EXPECT_EQ(arr.Count(), 10u);
	arr.DeleteValue(0);
	EXPECT_EQ(arr.Count(), 9u);
	for (int i = 0; i < 9; i++)
	{
		EXPECT_EQ(arr[i], i + 2);
	}

	EXPECT_THROW(arr.DeleteValue(arr.Count()), THArrayException); // try to delete out of range element
	EXPECT_EQ(arr.Count(), 9u);

	arr.DeleteValue(arr.Count() - 1); //try to delete last element in the array
	EXPECT_EQ(arr.Count(), 8u);
	for (int i = 0; i < 8; i++)
	{
		EXPECT_EQ(arr[i], i + 2);
	}
}

TEST(THArrayInt, Reverse1)
{
	THTestArray arr;
	
	EXPECT_NO_THROW(arr.Reverse());
	EXPECT_EQ(arr.Count(), 0u);
	arr.Reverse(0);
	arr.Reverse(1);
	arr.Reverse(10);

	for (int i = 0; i < 10; i++)
	{
		arr.AddValue(i + 1);
	}
	EXPECT_NO_THROW(arr.Reverse());

	EXPECT_EQ(arr.Count(), 10u);
	for (int i = 0; i < 10; i++)
	{
		EXPECT_EQ(arr[i], 10 - i);
	}

	arr.Reverse(5); // note: element with index 5 is included into reverse.

	EXPECT_EQ(arr.Count(), 10u);
	for (int i = 0; i <= 5; i++)
	{
		EXPECT_EQ(arr[i], i + 5);
	}

	for (int i = 6; i < 10; i++)
	{
		EXPECT_EQ(arr[i], 10 - i);
	}
}

TEST(THArrayInt, ItemSize)
{
	THTestArray aint;
	THArray<unsigned int> auint;
	THArray<long int> alint;
	THArray<long> along;
	THArray<unsigned long> aulong;
	THArray<long long> all;
	THArray<long long int> alli;
	THArray<unsigned long long> aull;
	THArray<float> afloat;
	THArray<long float> alfloat;
	THArray<double> adouble;
	THArray<long double> aldouble;
	THArray<char> achar;
	THArray<unsigned char> auchar;
	THArray<short> ashort;
	THArray<unsigned short> aushort;
	THArray<int64_t> aint64;
	THArray<size_t> asizet;
	THArray<char*> acharref;
	THArray<std::string> astring;
	//THArray<THArray<std::string>> aastring;

	astring.AddFillValues(10);    // test how FillValues() works with complex template parameter
	EXPECT_EQ(astring.Count(), 10u);
	EXPECT_EQ(astring.GetValue(5).length(), 0u);

	//aastring.AddFillValues(1000);
	//EXPECT_EQ(aastring.Count(), 1000);
	//EXPECT_EQ(aastring.GetValue(5).Count(), 0);

	EXPECT_EQ(aint.ItemSize(), 4u);
	EXPECT_EQ(auint.ItemSize(), 4u);
	EXPECT_EQ(alint.ItemSize(), 4u);
	EXPECT_EQ(along.ItemSize(), 4u);
	EXPECT_EQ(aulong.ItemSize(), 4u);
	EXPECT_EQ(all.ItemSize(), 8u);
	EXPECT_EQ(alli.ItemSize(), 8u);
	EXPECT_EQ(aull.ItemSize(), 8u);
	EXPECT_EQ(afloat.ItemSize(), 4u);
	EXPECT_EQ(alfloat.ItemSize(), 8u);
	EXPECT_EQ(adouble.ItemSize(), 8u);
	EXPECT_EQ(aldouble.ItemSize(), 8u);
	EXPECT_EQ(achar.ItemSize(), 1u);
	EXPECT_EQ(auchar.ItemSize(), 1u);
	EXPECT_EQ(ashort.ItemSize(), 2u);
	EXPECT_EQ(aushort.ItemSize(), 2u);
	EXPECT_EQ(aint64.ItemSize(), 8u);
	EXPECT_EQ(asizet.ItemSize(), 8u);
	EXPECT_EQ(acharref.ItemSize(), 8u);
#ifdef _DEBUG
	EXPECT_EQ(astring.ItemSize(), 40u);
#else
	EXPECT_EQ(astring.ItemSize(), 32u);
#endif
}

TEST(THArrayInt, testArrayException1)
{
	THArray<THArray<std::string> > arr;

	EXPECT_THROW(arr[0], THArrayException);
}

TEST(THArrayInt, testArrayException2)
{
	THArray<THArray<std::string> > arr1;
	THArray<std::string> arr2;
	arr1.AddValue(arr2);

	EXPECT_THROW(arr1.GetValue(0)[0], THArrayException);
}

TEST(THArrayInt, testArrayException3)
{
	THArray<THArray<std::string> > arr1;
	THArray<std::string> arr2;
	arr1.AddValue(arr2);

	EXPECT_THROW(arr1.GetValue(0).GetValue(0), THArrayException);
}

TEST(THArrayInt, testArrayException4)
{
	THArray<int> arr1;
	arr1.AddValue(1);

	EXPECT_THROW(arr1.SetValue(1, 289), THArrayException);
}

TEST(THArrayInt, testArrayException5)
{
	THArray<int> arr1;
	arr1.AddValue(654);

	EXPECT_THROW(arr1.InsertValue(2, 456), THArrayException);
}

TEST(THArrayInt, testArrayException6)
{
	THArray<int> arr1;

	arr1.InsertValue(0, 4567);

	EXPECT_THROW(arr1.InsertValue(2, 7777), THArrayException);
}

TEST(THArrayInt, testArrayException7)
{
	THArray<int> arr1;

	arr1.AddValue(456);

	EXPECT_THROW(arr1.DeleteValue(1), THArrayException);
}

TEST(THArrayInt, testArrayException8)
{
	THArray<int> arr1;

	EXPECT_THROW(arr1.DeleteValue(0), THArrayException);
}

// AddValue, GetValue
TEST(THArrayInt, testIntArray1)
{
	THArray<int> iar;

	iar.AddValue(1);
	iar.AddValue(1000000);
	iar.AddValue(-9999999);

	EXPECT_EQ(iar.GetValue(0), 1);
	EXPECT_EQ(iar.GetValue(1), 1000000);
	EXPECT_EQ(iar.GetValue(2), -9999999);
	EXPECT_EQ(iar.Count(), 3ul);

}

// IndexOf
TEST(THArrayInt, testIntArray2)
{
	THArray<int> iar;

	iar.AddValue(0);
	iar.AddValue(567800000);

	EXPECT_EQ(iar.IndexOf(0), 0);
	EXPECT_EQ(iar.IndexOf(567800000), 1);
	EXPECT_EQ(iar.IndexOf(1), -1);
	EXPECT_EQ(iar.IndexOf(-1), -1);

	iar.Clear();

	EXPECT_EQ(iar.IndexOf(0), -1);
	EXPECT_EQ(iar.IndexOf(567800000), -1);
	EXPECT_EQ(iar.IndexOf(65), -1);

	EXPECT_EQ(iar.Count(), 0ul);
}

// Count, DeleteValue
TEST(THArrayInt, testIntArray3)
{
	THArray<int> iar;

	EXPECT_EQ(iar.Count(), 0ul);

	iar.AddValue(98);
	iar.AddValue(45);
	iar.AddValue(66);
	EXPECT_EQ(iar.Count(), 3ul);

	iar.DeleteValue(0);
	EXPECT_EQ(iar.Count(), 2ul);

	iar.DeleteValue(0);
	EXPECT_EQ(iar.Count(), 1ul);

	EXPECT_THROW(iar.DeleteValue(1), THArrayException);

	EXPECT_EQ(iar.Count(), 1ul);

	iar.DeleteValue(0);
	EXPECT_EQ(iar.Count(), 0ul);

	EXPECT_THROW(iar.DeleteValue(0), THArrayException);
}

// Capacity, Clear, ClearMem
TEST(THArrayInt, testIntArray4)
{
	THArray<int> iar;

	iar.AddValue(7);
	iar.AddValue(-9);
	iar.AddValue(987654321);

	EXPECT_EQ(iar[0], 7);
	EXPECT_EQ(iar[1], -9);
	EXPECT_EQ(iar[2], 987654321);
	EXPECT_EQ(iar.Count(), 3ul);

	EXPECT_EQ(iar.Capacity(), 4ul);

	iar.Clear();

	EXPECT_EQ(iar.Capacity(), 4ul);

	iar.AddValue(2);
	iar.AddValue(4);
	iar.AddValue(101);
	iar.AddValue(101);

	EXPECT_EQ(iar[2], 101);
	EXPECT_EQ(iar[3], 101);
	EXPECT_EQ(iar.Count(), 4ul);
	EXPECT_EQ(iar.Capacity(), 4ul);

	iar.ClearMem();

	EXPECT_EQ(iar.Count(), 0ul);
	EXPECT_EQ(iar.Capacity(), 0ul);
}

// Reverse
TEST(THArrayInt, testIntArray5)
{
	THArray<int> iar;

	iar.AddValue(7);
	iar.AddValue(9);
	iar.AddValue(-1000);
	iar.AddValue(0);

	EXPECT_EQ(iar[0], 7);
	EXPECT_EQ(iar[1], 9);


	iar.Reverse();

	EXPECT_EQ(iar[0], 0);
	EXPECT_EQ(iar[1], -1000);
	EXPECT_EQ(iar[2], 9);
	EXPECT_EQ(iar[3], 7);
	EXPECT_EQ(iar.Count(), 4ul);

	iar.Clear();
	EXPECT_EQ(iar.Count(), 0ul);

	iar.AddValue(7);
	iar.AddValue(9);
	iar.AddValue(-1000);
	iar.AddValue(0);
	iar.AddValue(78);

	EXPECT_EQ(iar.Count(), 5ul);

	iar.Reverse();

	EXPECT_EQ(iar[0], 78);
	EXPECT_EQ(iar[1], 0);
	EXPECT_EQ(iar[2], -1000);
	EXPECT_EQ(iar[3], 9);
	EXPECT_EQ(iar[4], 7);

}

// Reverse(Index)
TEST(THArrayInt, testIntArray6)
{
	THArray<int> iar;

	iar.AddValue(4);
	iar.AddValue(8);
	iar.AddValue(654);
	iar.AddValue(-5);
	iar.AddValue(0);

	iar.Reverse(0);

	EXPECT_EQ(iar[0], 4);
	EXPECT_EQ(iar[1], 8);
	EXPECT_EQ(iar[2], 654);
	EXPECT_EQ(iar[3], -5);
	EXPECT_EQ(iar[4], 0);


	iar.Reverse(2);

	EXPECT_EQ(iar.Count(), 5ul);
	EXPECT_EQ(iar[0], 654);
	EXPECT_EQ(iar[1], 8);
	EXPECT_EQ(iar[2], 4);
	EXPECT_EQ(iar[3], -5);
	EXPECT_EQ(iar[4], 0);

	iar.Reverse(1);

	EXPECT_EQ(iar[0], 8);
	EXPECT_EQ(iar[1], 654);
	EXPECT_EQ(iar[2], 4);
	EXPECT_EQ(iar[3], -5);
	EXPECT_EQ(iar[4], 0);

	iar.Reverse(3);

	EXPECT_EQ(iar[0], -5);
	EXPECT_EQ(iar[1], 4);
	EXPECT_EQ(iar[2], 654);
	EXPECT_EQ(iar[3], 8);
	EXPECT_EQ(iar[4], 0);

}

// InsertValue
TEST(THArrayInt, testIntArray7)
{
	THArray<int> iar;

	iar.AddValue(7);
	iar.AddValue(9);

	iar.InsertValue(0, 11);

	EXPECT_EQ(iar.Count(), 3ul);
	EXPECT_EQ(iar[0], 11);
	EXPECT_EQ(iar[1], 7);
	EXPECT_EQ(iar[2], 9);

	iar.InsertValue(2, 33);

	EXPECT_EQ(iar.Count(), 4ul);
	EXPECT_EQ(iar[0], 11);
	EXPECT_EQ(iar[1], 7);
	EXPECT_EQ(iar[2], 33);
	EXPECT_EQ(iar[3], 9);

	EXPECT_THROW(iar.InsertValue(5, 55), THArrayException);

	EXPECT_EQ(iar.Count(), 4ul);

	iar.InsertValue(4, 44);

	EXPECT_EQ(iar.Count(), 5ul);
	EXPECT_EQ(iar[4], 44);

	iar.Clear();

	int a = 5;
	iar.Insert(0, &a);
	a = 6;
	iar.Insert(1, &a);

	EXPECT_EQ(iar.Count(), 2ul);
	EXPECT_EQ(iar[0], 5);
	EXPECT_EQ(iar[1], 6);
}

// Zero, SetCapacity, Hold
TEST(THArrayInt, testIntArray8)
{
	THArray<int> iar;

	iar.AddValue(1);
	iar.AddValue(10);
	iar.AddValue(100);
	iar.AddValue(-12345);

	iar.Zero();

	EXPECT_EQ(iar[0], 0);
	EXPECT_EQ(iar[1], 0);
	EXPECT_EQ(iar[2], 0);
	EXPECT_EQ(iar[3], 0);

	iar.SetCapacity(8);

	EXPECT_EQ(iar.Capacity(), 8ul);

	iar.Hold();

	EXPECT_EQ(iar.Capacity(), iar.Count());
}

// AddValue, AddFillValues
TEST(THArrayInt, testIntArray9)
{
	THArray<int> iar;

	int* buf = new int[20];

	buf[0] = 99;
	buf[1] = 88;
	buf[2] = -77;
	buf[3] = 66;
	buf[4] = -555;

	iar.AddValue(1);
	iar.AddValue(10);
	iar.AddValue(-12345);

	for (int i = 0; i < 20; i++)
		iar.Add(&buf[i]);

	EXPECT_EQ(iar.Count(), 23ul);
	EXPECT_EQ(iar[0], 1);
	EXPECT_EQ(iar[1], 10);
	EXPECT_EQ(iar[2], -12345);
	EXPECT_EQ(iar[3], 99);
	EXPECT_EQ(iar[4], 88);
	EXPECT_EQ(iar[5], -77);
	EXPECT_EQ(iar[6], 66);
	EXPECT_EQ(iar[7], -555);

	iar.AddFillValues(10);

	EXPECT_EQ(iar.Count(), 33ul);
	//ASSERT(iar[22] == 0); value with index 22 is not defined (see initialization of buf[] array above)
	EXPECT_EQ(iar[23], 0);
	EXPECT_EQ(iar[24], 0);
	EXPECT_EQ(iar[25], 0);
	EXPECT_EQ(iar[30], 0);
	EXPECT_EQ(iar[31], 0);
	EXPECT_EQ(iar[32], 0);

	EXPECT_THROW(iar[33], THArrayException);
}


// IndexOf<Compare>(i)
TEST(THArrayInt, testIntArray10)
{
	THArray<int> iar;

	iar.AddValue(1);
	iar.AddValue(7);
	iar.AddValue(-8);
	iar.AddValue(-0);
	iar.AddValue(2);

	EXPECT_EQ(iar.IndexOf(-8), 2);

	using cmpint = Compare<int>;
	
	EXPECT_EQ(iar.IndexOf<cmpint>(1), 0);
	EXPECT_EQ(iar.IndexOf<cmpint>(0), 3);
	EXPECT_EQ(iar.IndexOf<cmpint>(2), 4);
	EXPECT_EQ(iar.IndexOf<cmpint>(-2), -1);
	EXPECT_EQ(iar.IndexOf<cmpint>(-8), 2);
}

// IndexOfFrom, IndexOfFrom(i, v, Compare)
TEST(THArrayInt, testIntArray11)
{
	THArray<int> iar;

	iar.AddValue(1);
	iar.AddValue(-8);
	iar.AddValue(7);
	iar.AddValue(-0);

	EXPECT_EQ(iar.IndexOfFrom(-8, 0), 1);
	EXPECT_EQ(iar.IndexOfFrom(-8, 1), 1);
	EXPECT_EQ(iar.IndexOfFrom(-8, 2), -1);
	EXPECT_EQ(iar.IndexOfFrom(-8, 3), -1);
	EXPECT_EQ(iar.IndexOfFrom(-8, 6), -1);


	iar.ClearMem();

	iar.AddValue(1);
	iar.AddValue(-8);
	iar.AddValue(7);
	iar.AddValue(-0);
	iar.AddValue(-8);
	iar.AddValue(2);
	iar.AddValue(9876);

	EXPECT_EQ(iar.IndexOfFrom(-8, 0), 1);
	EXPECT_EQ(iar.IndexOfFrom(-8, 1), 1);
	EXPECT_EQ(iar.IndexOfFrom(-8, 2), 4);
	EXPECT_EQ(iar.IndexOfFrom(-8, 3), 4);
	EXPECT_EQ(iar.IndexOfFrom(-8, 4), 4);
	EXPECT_EQ(iar.IndexOfFrom(-8, 5), -1);
	EXPECT_EQ(iar.IndexOfFrom(-8, 50), -1);

	iar.ClearMem();

	using cmpint = Compare<int>;

	iar.AddValue(1);
	iar.AddValue(-8);
	iar.AddValue(7);
	iar.AddValue(-0);

	EXPECT_EQ(iar.IndexOfFrom<Compare<int>>(-8, 0), 1);
	EXPECT_EQ(iar.IndexOfFrom<Compare<int>>(-8, 0), 1);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 1), 1);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 2), -1);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 3), -1);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 6), -1);

	iar.ClearMem();

	iar.AddValue(1);
	iar.AddValue(-8);
	iar.AddValue(7);
	iar.AddValue(-0);
	iar.AddValue(-8);
	iar.AddValue(2);
	iar.AddValue(9876);

	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 0), 1);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 1), 1);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 2), 4);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 3), 4);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 4), 4);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 5), -1);
	EXPECT_EQ(iar.IndexOfFrom<cmpint>(-8, 50), -1);

}

// Push, Pop
TEST(THArrayInt, testIntArray12)
{
	THArray<int> iar;

	iar.Push(34567);
	iar.Push(7);
	iar.Push(-8);
	iar.Push(-0);
	iar.Push(2);

	EXPECT_EQ(iar.IndexOf(34567), 0);
	EXPECT_EQ(iar.IndexOf(-8), 2);
	EXPECT_EQ(iar.IndexOf(1), -1);
	EXPECT_EQ(iar.IndexOf(0), 3);
	EXPECT_EQ(iar.IndexOf(2), 4);
	EXPECT_EQ(iar.IndexOf(-2), -1);

	EXPECT_EQ(iar.Pop(), 2);

	EXPECT_EQ(iar.IndexOf(34567), 0);
	EXPECT_EQ(iar.IndexOf(-8), 2);
	EXPECT_EQ(iar.IndexOf(1), -1);
	EXPECT_EQ(iar.IndexOf(0), 3);
	EXPECT_EQ(iar.IndexOf(2), -1);
	EXPECT_EQ(iar.IndexOf(-2), -1);

	EXPECT_EQ(iar.Pop(), 0);

	EXPECT_EQ(iar.IndexOf(34567), 0);
	EXPECT_EQ(iar.IndexOf(-8), 2);
	EXPECT_EQ(iar.IndexOf(1), -1);
	EXPECT_EQ(iar.IndexOf(0), -1);
	EXPECT_EQ(iar.IndexOf(2), -1);
	EXPECT_EQ(iar.IndexOf(-2), -1);

	EXPECT_EQ(iar.Pop(), -8);

	EXPECT_EQ(iar.IndexOf(34567), 0);
	EXPECT_EQ(iar.IndexOf(-8), -1);
	EXPECT_EQ(iar.IndexOf(1), -1);
	EXPECT_EQ(iar.IndexOf(0), -1);
	EXPECT_EQ(iar.IndexOf(2), -1);
	EXPECT_EQ(iar.IndexOf(-2), -1);

	EXPECT_EQ(iar.Pop(), 7);

	EXPECT_EQ(iar.Count(), 1ul);

	EXPECT_EQ(iar.IndexOf(34567), 0);
}

// Push, Swap
TEST(THArrayInt, testIntArray13)
{
	THArray<int> iar;

	iar.Push(765);
	iar.Push(754);
	iar.Push(12);
	iar.Push(8);
	iar.Push(05);

	iar.Swap(1, 1);
	iar.Swap(0, 0);
}

// Memory
TEST(THArrayInt, testIntArray14)
{
	THArray<int> iar;

	//int* mem = iar.Memory();
	//EXPECT_EQ(mem, nullptr);

	iar.AddValue(321);
	//mem = iar.Memory();
	//EXPECT_NE(mem, nullptr);
}

// GetValuePointer
TEST(THArrayInt, testIntArray15)
{
	THArray<int> iar;

	iar.AddValue(321);
	iar.AddValue(987);

	EXPECT_EQ(iar.Count(), 2ul);

	int* pValue = iar.GetValuePointer(0);
	*pValue = 333;
	EXPECT_EQ(iar[0], 333);

	pValue = iar.GetValuePointer(1);
	*pValue = 111;
	EXPECT_EQ(iar[1], 111);

	EXPECT_THROW(iar.GetValuePointer(2), THArrayException);
	EXPECT_THROW(iar.GetValuePointer(5), THArrayException);
}


// test for operator=
TEST(THArrayInt, testIntArray16)
{
	THArray<int> iar;

	iar.AddValue(321);
	iar.AddValue(987);
	iar.AddValue(777);
	iar.AddValue(000);

	EXPECT_EQ(iar.Count(), 4ul);

	THArray<int> iarcopy;

	iarcopy.AddValue(1);
	iarcopy.AddValue(88888);

	iarcopy = iar;

	EXPECT_EQ(iarcopy.Count(), 4ul);
	EXPECT_EQ(iarcopy[0], 321);
	EXPECT_EQ(iarcopy[1], 987);
	EXPECT_EQ(iarcopy[2], 777);
	EXPECT_EQ(iarcopy[3], 0);
}

