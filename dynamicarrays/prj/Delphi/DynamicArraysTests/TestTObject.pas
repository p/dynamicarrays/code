unit TestTObject;

interface

type
TTestObject = class
private
  FValue: Integer;
public
  constructor Create(Value: Integer);
  function Equals(Obj: TObject): Boolean; override;
  function GetHashCode: Integer;override;

end;

implementation


{ TTestObject }

constructor TTestObject.Create(Value: Integer);
begin
  inherited Create;
  FValue := Value;
end;

function TTestObject.Equals(Obj: TObject): Boolean;
begin
  if Obj is TTestObject
    then Result := FValue = TTestObject(Obj).FValue
    else Result := inherited;
end;

function TTestObject.GetHashCode: Integer;
begin
  Result := FValue;
end;

end.
