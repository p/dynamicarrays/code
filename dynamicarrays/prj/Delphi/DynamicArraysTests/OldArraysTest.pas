unit OldArraysTest;

interface

uses
  DUnitX.TestFramework, DynamicArrays;

type
  [TestFixture]
  THArrayTest = class
  public
    // Sample Methods
    [Test]
    procedure TestEmpty1;
    [Test]
    procedure TestEmpty2;
    [Test]
    procedure TestIndexOf1;
    [Test]
    [TestCase('TestIndexofA','4,1,2')]
    [TestCase('TestIndexofB','0,-1,3')]
    [TestCase('TestIndexofC','-1,0,3')]
    procedure TestIndexOf2(const AValue1, AValue2, AValue3 : Integer);
    [Test]
    procedure TestAdd1;
    [Test]
    procedure TestCapacity1;
    // Test with TestCase Attribute to supply parameters.
    [Test]
    [TestCase('TestA','1,2')]
    [TestCase('TestB','3,4')]
    procedure Test2(const AValue1 : Integer;const AValue2 : Integer);
  end;

implementation
uses SysUtils;

procedure THArrayTest.TestEmpty1;
var arr:THArray;
begin
  arr := THArray.Create();

  Assert.AreEqual(0, arr.Count);
  Assert.AreEqual(0, arr.Capacity);
  Assert.AreEqual(1, arr.ItemSize);
  Assert.AreEqual(nil, arr.Memory);

  arr.Clear();
  Assert.AreEqual(0, arr.Count);
  Assert.AreEqual(0, arr.Capacity);
  Assert.AreEqual(1, arr.ItemSize);
  Assert.AreEqual(nil, arr.Memory);

  arr.ClearMem();
  Assert.AreEqual(0, arr.Count);
  Assert.AreEqual(0, arr.Capacity);
  Assert.AreEqual(1, arr.ItemSize);
  Assert.AreEqual(nil, arr.Memory);

  arr.Hold;
  Assert.AreEqual(0, arr.Count);
  Assert.AreEqual(0, arr.Capacity);
  Assert.AreEqual(1, arr.ItemSize);
  Assert.AreEqual(nil, arr.Memory);

  arr.Zero;
  Assert.AreEqual(0, arr.Count);
  Assert.AreEqual(0, arr.Capacity);
  Assert.AreEqual(1, arr.ItemSize);
  Assert.AreEqual(nil, arr.Memory);

  arr.Sort(nil);  // no exception should be raised in case of nil parameters
  arr.QuickSort(nil, nil);

end;

procedure THArrayTest.TestEmpty2;
var arr:THArray;
    p:pointer;
begin
  arr := THArray.Create();
  p := nil;

  Assert.WillRaise(procedure begin arr.Delete(0) end, ERangeError, 'd0');
  Assert.WillRaise(procedure begin arr.Delete(1) end, ERangeError, 'd1');
  Assert.WillRaise(procedure begin arr.Delete(2) end, ERangeError, 'd2');
  Assert.WillRaise(procedure begin arr.Delete(10000) end, ERangeError, 'd10000');

  Assert.WillRaise(procedure begin arr.GetAddr(0) end, ERangeError, 'ga0');
  Assert.WillRaise(procedure begin arr.GetAddr(1) end, ERangeError, 'ga1');
  Assert.WillRaise(procedure begin arr.GetAddr(2) end, ERangeError, 'ga2');
  Assert.WillRaise(procedure begin arr.GetAddr(10000) end, ERangeError, 'ga10000');

  Assert.WillRaise(procedure begin arr.Get(0, p) end, ERangeError, 'g0');
  Assert.WillRaise(procedure begin arr.Get(1, p) end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin arr.Get(2, p) end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin arr.Get(10000, p) end, ERangeError, 'g10000');

  Assert.WillRaise(procedure begin arr.Update(0, p) end, ERangeError, 'u0');
  Assert.WillRaise(procedure begin arr.Update(1, p) end, ERangeError, 'u1');
  Assert.WillRaise(procedure begin arr.Update(2, p) end, ERangeError, 'u2');
  Assert.WillRaise(procedure begin arr.Update(10000, p) end, ERangeError, 'u10000');

end;

procedure THArrayTest.TestIndexOf1;
var arr:THArray;
    v: integer;
begin
  arr := THArray.Create();

  Assert.AreEqual(-1, arr.IndexOf(nil));
  Assert.AreEqual(-1, arr.IndexOf(pointer(1)));
  Assert.AreEqual(-1, arr.IndexOf(@v));

  Assert.AreEqual(-1, arr.IndexOfFrom(nil, 0));
  Assert.AreEqual(-1, arr.IndexOfFrom(nil, 1));
  Assert.AreEqual(-1, arr.IndexOfFrom(nil, 2));
  Assert.AreEqual(-1, arr.IndexOfFrom(nil, 1000));
  Assert.AreEqual(-1, arr.IndexOfFrom(pointer(1), 0));
  Assert.AreEqual(-1, arr.IndexOfFrom(pointer(1), 1));
  Assert.AreEqual(-1, arr.IndexOfFrom(pointer(1), 2));
  Assert.AreEqual(-1, arr.IndexOfFrom(pointer(1), 1000));

  Assert.AreEqual(-1, arr.IndexOfFrom(@v, 0));
  Assert.AreEqual(-1, arr.IndexOfFrom(@v, 1));
  Assert.AreEqual(-1, arr.IndexOfFrom(@v, 2));
  Assert.AreEqual(-1, arr.IndexOfFrom(@v, 1000));

end;

procedure THArrayTest.TestIndexOf2(const AValue1, AValue2, AValue3 : Integer);
var arr:THArray;
    v,vv,vvv: integer;
begin
  arr := THArray.Create();
  arr.ItemSize := sizeof(Integer);

  v   := AValue1;
  vv  := AValue2;
  vvv := Avalue3;

  arr.Add(@v);
  arr.Get(0, @vv);

  Assert.AreEqual(AValue1, vv);
  Assert.AreEqual(0, arr.IndexOf(@v));
  Assert.AreEqual(0, arr.IndexOf(@vv));
  Assert.AreEqual(-1, arr.IndexOf(@arr));

  Assert.WillRaise(procedure begin arr.IndexOfFrom(nil, 0); end, EAccessViolation, 'indexoffrom nil 0');
  Assert.AreEqual(-1, arr.IndexOfFrom(nil, 1));
  Assert.AreEqual(-1, arr.IndexOfFrom(nil, 2));
  Assert.AreEqual(-1, arr.IndexOfFrom(nil, 555));

  Assert.WillRaise(procedure begin arr.IndexOfFrom(pointer(1), 0); end, EAccessViolation, 'indexoffrom pointer1 0');
  Assert.AreEqual(-1, arr.IndexOfFrom(pointer(1), 1));
  Assert.AreEqual(-1, arr.IndexOfFrom(pointer(1), 2));
  Assert.AreEqual(-1, arr.IndexOfFrom(pointer(1), 666));

  Assert.AreEqual(0, arr.IndexOfFrom(@v, 0));
  Assert.AreEqual(-1, arr.IndexOfFrom(@v, 1));
  Assert.AreEqual(-1, arr.IndexOfFrom(@v, 2));
  Assert.AreEqual(-1, arr.IndexOfFrom(@v, 333));

  Assert.AreEqual(0, arr.IndexOfFrom(@vv, 0));
  Assert.AreEqual(-1, arr.IndexOfFrom(@vv, 1));
  Assert.AreEqual(-1, arr.IndexOfFrom(@vv, 2));
  Assert.AreEqual(-1, arr.IndexOfFrom(@vv, 333));

  Assert.AreEqual(-1, arr.IndexOfFrom(@vvv, 0));
  Assert.AreEqual(-1, arr.IndexOfFrom(@vvv, 1));
  Assert.AreEqual(-1, arr.IndexOfFrom(@vvv, 2));
  Assert.AreEqual(-1, arr.IndexOfFrom(@vvv, 333));
end;

procedure THArrayTest.TestCapacity1;
var arr:THArray;
begin
  arr := THArray.Create();

  Assert.AreEqual(0, arr.Capacity);

  arr.SetCapacity(1);
  Assert.AreEqual(1, arr.Capacity);
  Assert.AreEqual(0, arr.Count);

  arr.SetCapacity(0);
  Assert.AreEqual(0, arr.Capacity);

  arr.SetCapacity(2);
  Assert.AreEqual(2, arr.Capacity);
  Assert.AreEqual(0, arr.Count);

  arr.SetCapacity(3);
  Assert.AreEqual(3, arr.Capacity);
  Assert.AreEqual(0, arr.Count);

  arr.SetCapacity(100_000_000);
  Assert.AreEqual(100_000_000, arr.Capacity);
  Assert.AreEqual(0, arr.Count);

  arr.SetCapacity(0);
  Assert.AreEqual(0, arr.Capacity);
  Assert.AreEqual(0, arr.Count);

  arr.SetCapacity(100);
  Assert.AreEqual(100, arr.Capacity);
  Assert.AreEqual(0, arr.Count);

  arr.Zero;
  Assert.AreEqual(100, arr.Capacity);

  arr.Clear();
  Assert.AreEqual(100, arr.Capacity);

  arr.Hold;
  Assert.AreEqual(0, arr.Capacity);
  Assert.AreEqual(0, arr.Count);

  arr.SetCapacity(200);
  arr.ClearMem();
  Assert.AreEqual(0, arr.Capacity);
end;

procedure THArrayTest.TestAdd1;
var arr:THArray;
    i:integer;
    p:pointer;
begin
  arr := THArray.Create();

  Assert.AreEqual(0, arr.Count);
  Assert.AreEqual(0, arr.Capacity);

  arr.Add(nil);
  Assert.AreEqual(1, arr.Count);
  Assert.AreEqual(4, arr.Capacity);
  i := 7;
  p := @i;
  arr.Get(0, p);
  Assert.AreEqual(0, i);
end;

procedure THArrayTest.Test2(const AValue1 : Integer;const AValue2 : Integer);
var arr:THArrayInteger;
begin
  arr := THArrayInteger.Create();

  Assert.AreEqual(0, arr.Count);
  arr.ClearMem();

  arr.AddValue(5);
  Assert.AreEqual(1, arr.Count);
  Assert.AreEqual(5, arr[0]);
  Assert.WillRaise(procedure begin arr[1] end, ERangeError, 'fffff');
end;

initialization
 // TDUnitX.RegisterTestFixture(THArrayTest);

end.
