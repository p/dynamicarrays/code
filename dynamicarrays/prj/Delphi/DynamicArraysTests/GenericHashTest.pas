unit GenericHashTest;

interface

uses
  DUnitX.TestFramework, TestBase, Hash;

type
  [TestFixture]
  TGenericHashTest<K, V:Constructor> = class(TTestBase)
  private
    hash1: THash<K,V>;
  public
    [TearDownFixture]
    procedure TearDownFixture; // this method is called when all tests of this fixture are executed. so we have to free objects from FObjects
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    [TestCase('TestA','1,2')]
    [TestCase('TestB','34, 56')]
    [TestCase('TestC','-1, 0')]
    [TestCase('TestD','0, -3333333')]
    procedure TestEmpty1(AValue1, AValue2: Integer);
    [Test]
    [TestCase('TestA','1,2, 12')]
    [TestCase('TestB','3,4, 34')]
    [TestCase('TestC','1,4, 14')]
    procedure TestSetValue1(const Index1, Index2, Value : Integer);
    [Test]
    [TestCase('TestA','0,2, 54')]
    [TestCase('TestB','3,-4, 666')]
    [TestCase('TestC','100,-4, 444')]
    procedure TestSetValue2(const Index1, Index2, Value : Integer);
    [Test]
    procedure TestDelete1();
    [Test]
    procedure TestDelete2();
    procedure TestIfExists1();

  end;

implementation

uses SysUtils, DynamicArray;

procedure TGenericHashTest<K,V>.Setup;
begin
   hash1 := THash<K,V>.Create;
end;

procedure TGenericHashTest<K,V>.TearDown;
begin
  FreeAndNil(hash1);
end;

procedure TGenericHashTest<K,V>.TearDownFixture;
begin
   FreeTObjects;
end;

procedure TGenericHashTest<K,V>.TestEmpty1(AValue1, AValue2: Integer);
begin
  Assert.AreEqual(0, hash1.Count);

  hash1.Clear;
  Assert.AreEqual(0, hash1.Count);

  hash1.ClearMem;
  Assert.AreEqual(0, hash1.Count);

  Assert.WillRaise(procedure begin hash1.GetValue(CreateValue<K>(AValue1)); end, ERangeError, '11111');
  Assert.WillRaise(procedure begin hash1[CreateValue<K>(AValue1)]; end, ERangeError, '2222222');

  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<K>(AValue1)); end, nil, '3333333');

  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<K>(0)); end, nil, 'delete 0');
  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<K>(1)); end, nil, 'delete 1');
end;

procedure TGenericHashTest<K, V>.TestIfExists1;
begin

end;

procedure TGenericHashTest<K, V>.TestDelete1;
var
  key11, key12 : K;
  val1, val2: V;
begin
  key11 := CreateValue<K>(5);
  key12 := CreateValue<K>(8);
  val1  := CreateValue<V>(222);
  val2  := CreateValue<V>(999);

  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key11, val1);
  Assert.AreEqual(1, hash1.Count);
  Assert.AreEqual<V>(val1, hash1.GetValue(key11));
  hash1.SetValue(key12, val1);
  Assert.AreEqual(2, hash1.Count);
  Assert.AreEqual<V>(val1, hash1.GetValue(key12));

  hash1.SetValue(key12, val2);
  Assert.AreEqual<V>(val2, hash1.GetValue(key12));
  Assert.AreEqual(2, hash1.Count);

  hash1.Delete(key11);
  Assert.AreEqual(1, hash1.Count);

  hash1.Delete(key12);
  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key12, val2);
  Assert.AreEqual(1, hash1.Count);
  Assert.AreEqual<V>(val2, hash1.GetValue(key12));

	hash1.Delete(key11);
  Assert.AreEqual(1, hash1.Count);
  Assert.AreEqual<V>(val2, hash1.GetValue(key12));
  hash1.Delete(key12);
  Assert.AreEqual(0, hash1.Count);
end;

procedure TGenericHashTest<K, V>.TestDelete2;
var
  key11, key12 : K;
  val1, val2: V;
  I: Integer;
  keys1, keys2: THArrayG<K>;
  vals1, vals2: THArrayG<V>;
begin
  //key11 := CreateValue<K>(5);
  //key12 := CreateValue<K>(8);

  keys1 := THArrayG<K>.Create;
  keys2 := THArrayG<K>.Create;
  vals1 := THArrayG<V>.Create;
  vals2 := THArrayG<V>.Create;


  for I := 0 to 10 do begin
    key11 := CreateValue<K>(i + 111);
    val1 := CreateValue<V>(i + 222);
    keys1.AddValue(key11);
    vals1.AddValue(val1);
    hash1[key11] := val1;
  end;

  Assert.AreEqual(11, hash1.Count);

  for I := 0 to 10 do begin
    key12 := CreateValue<K>(i + 333);
    val2 := CreateValue<V>(i + 999);
    keys2.AddValue(key12);
    vals2.AddValue(val2);
    hash1[key12] := val2;
  end;

  Assert.AreEqual(22, hash1.Count);

  for I := 0 to 10 do begin
    key11 := keys1[i];
    val1 := vals1[i];
    Assert.AreEqual<V>(val1, hash1[key11]);
  end;

  for I := 0 to 10 do begin
    key12 := keys2[i];
    val2 := vals2[i];
    Assert.AreEqual<V>(val2, hash1[key12]);
  end;

  hash1.Delete(keys1[0]);
  Assert.AreEqual(21, hash1.Count);

  for I := 1 to 10 do begin
    key11 := keys1[i];
    val1 := vals1[i];
    Assert.AreEqual<V>(val1, hash1[key11]);
  end;

  hash1.Delete(keys1[10]);
  Assert.AreEqual(20, hash1.Count);
  for I := 1 to 9 do begin
    key12 := keys1[i];
    val1 := vals1[i];
    Assert.AreEqual<V>(val1, hash1[key12]);
  end;
{
  hash1.Delete(arr2[5]);
  Assert.AreEqual(19, hash1.Count);
  for I := 0 to 4 do begin
    key22 := arr2[i];
    Assert.AreEqual<V>(CreateValue<V>(i + 999), hash1[key12, key22]);
  end;
  for I := 6 to 10 do begin
    key22 := arr2[i];
    Assert.AreEqual<V>(CreateValue<V>(i + 999), hash1[key12, key22]);
  end;
 }
  keys1.Free;
  keys2.Free;
  vals1.Free;
  vals2.Free;
end;

procedure TGenericHashTest<K,V>.TestSetValue1(const Index1, Index2, Value : Integer);
var
  key1: K;
  key2:K;
  val: V;
begin
  key1 := CreateValue<K>(Index1);
  key2 := CreateValue<K>(Index2);
  val  := CreateValue<V>(Value);

  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key1, val);
  Assert.AreEqual(1, hash1.Count);

  Assert.AreEqual<V>(val, hash1.GetValue(key1));
  Assert.AreEqual(1, hash1.Count);

  hash1.Delete(key1);
  Assert.AreEqual(0, hash1.Count);
  Assert.WillRaise(procedure begin hash1.GetValue(key1); end, ERangeError, 'key1');

  hash1.SetValue(key2, val);
  Assert.AreEqual(1, hash1.Count);
  hash1.Clear;
  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key2, val);
  Assert.AreEqual(1, hash1.Count);
  hash1.ClearMem;
  Assert.AreEqual(0, hash1.Count);

end;

procedure TGenericHashTest<K,V>.TestSetValue2(const Index1, Index2, Value : Integer);
var
  key1: K;
  //key2: K;
  val1, val2: V;
begin
  key1 := CreateValue<K>(Index1);
  //key2 := CreateValue<K>(Index2);
  val1  := CreateValue<V>(Value);
  val2  := CreateValue<V>(Value + 1);

  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key1, val1);
  Assert.AreEqual(1, hash1.Count);

  hash1.SetValue(key1, val2);
  Assert.AreEqual(1, hash1.Count);

  Assert.AreEqual<V>(val2, hash1.GetValue(key1));
  Assert.AreEqual(1, hash1.Count);

  hash1.Delete(key1);
  Assert.AreEqual(0, hash1.Count);
  Assert.WillRaise(procedure begin hash1.GetValue(key1); end, ERangeError, 'key2');

  hash1.SetValue(key1, val1);
  Assert.AreEqual(1, hash1.Count);
  hash1.Delete(key1);
  Assert.AreEqual(0, hash1.Count);

end;

initialization
  TDUnitX.RegisterTestFixture(TGenericHashTest<Integer, Integer>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Integer, Cardinal>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Integer, Byte>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Byte, Byte>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Char, Word>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<WideChar, SmallInt>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<string, string>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<string, Integer>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<string, AnsiString>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<AnsiString, string>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<ShortString, AnsiString>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<string, Byte>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Int64, Pointer>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<UInt64, PInteger>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<PChar, Integer>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Cardinal, PChar>);

  TDUnitX.RegisterTestFixture(TGenericHashTest<Single, Word>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Real, Real>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Integer, Currency>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Extended, Cardinal>);

  TDUnitX.RegisterTestFixture(TGenericHashTest<TObject, THarrayG<Integer>>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<THarrayG<Integer>, string>);
  TDUnitX.RegisterTestFixture(TGenericHashTest<Cardinal, THarrayG<Integer>>);

 // TDUnitX.RegisterTestFixture(TGenericHashTest<array of Pointer, array of TObject>);
 // TDUnitX.RegisterTestFixture(TGenericHashTest<array of Extended, THarrayG<Integer>>);

end.

