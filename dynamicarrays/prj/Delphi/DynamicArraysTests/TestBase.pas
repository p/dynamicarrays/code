unit TestBase;

interface

uses DynamicArray;

type
  TTestBase = class
  protected
  // array contans all created TObject (and descendants) instances by CreateValue<T> method.
  // used to Free all these classes in one place to avoid memoty leaks during tests.
    FObjects: THarrayG<TObject>;
    FComparableTypes: set of TTypeKind; // list of types that are compared by value in Delphi. for example class types (TObject and all descendants) are NOT compared by value

    function CreateValue<T:constructor>(const AValue: Integer): T;
    function CreateInteger<T>(const AValue: T): Integer;
    procedure FreeTObjects;
  public
    constructor Create;
    destructor Destroy; override;
end;

implementation

uses System.TypInfo, SysUtils, TestTObject;

procedure TTestBase.FreeTObjects;
var
  I: Integer;
begin
   for I := 1 to FObjects.Count do FObjects[i - 1].Free;
   FObjects.ClearMem;
end;

function TTestBase.CreateValue<T>(const AValue: Integer): T;
var
  TypeDataPtr: PTypeData;
  TypeInfoPtr: PTypeInfo;
  TypeKind: TTypeKind;
  I8: Int8 absolute Result;
  U8: UInt8 absolute Result;
  I16: Int16 absolute Result;
  U16: UInt16 absolute Result;
  I32: Int32 absolute Result;
  U32: UInt32 absolute Result;
  I64: Int64 absolute Result;
  UStr: UnicodeString absolute Result;
  AnsiStr: AnsiString absolute Result;
  ShortStr: ShortString absolute Result;
  IntArr: THArrayG<Integer> absolute Result;
  IntObj: TObject absolute Result;
  Pnter: Pointer absolute Result;
  Ch: Char absolute Result;
  WCh: WideChar absolute Result;
  flSingle: Single absolute Result;
  flDouble: Double absolute Result;
  flExtended: Extended absolute Result;
  flComp: Comp absolute Result;
  flCurrency: Currency absolute Result;
  Bool: Boolean absolute Result;
begin
  Result := T.Create;

  TypeInfoPtr := TypeInfo(T);
  TypeDataPtr := GetTypeData(TypeInfoPtr);
  TypeKind := GetTypeKind(T);

  case TypeKind of
    tkInteger:
      begin
        case TypeDataPtr.OrdType of
          otSByte: I8  := Int8(AValue);
          otUByte: U8  := UInt8(AValue);
          otSWord: I16 := Int16(AValue);
          otUWord: U16 := UInt16(AValue);
          otSLong: I32 := Int32(AValue);
          otULong: U32 := UInt32(AValue);
        else
          System.Assert(False, 'CreateValue<T>: Invalid integer type');
        end;
      end;
     tkInt64: I64 := Int64(AValue);

     tkUString: UStr    := IntToStr(AValue);
     tkLString: AnsiStr := AnsiString(IntToStr(AValue));
     tkString: ShortStr := ShortString(IntToStr(AValue));
     tkPointer: Pnter   := Pointer(AValue);
     tkChar:    Ch      := Char(AValue);
     tkWChar:   WCh     := WideChar(AValue);

     tkFloat:
     case TypeDataPtr.FloatType of
         ftSingle: flSingle    := AValue;
         ftDouble: flDouble    := AValue;
         ftExtended:flExtended := AValue;
         ftComp: flComp        := AValue;
         ftCurr: flCurrency    := AValue;
     else
         System.Assert(False, 'CreateValue<T>: Invalid float type');
     end;

     tkEnumeration:
     begin
       if TypeInfoPtr.Name = 'Boolean'
         then Bool := AValue <> 0
         else U8 := UInt8(Avalue); // real enumeration type here. enumerations cannot hold more than 256 values.
     end;

     tkClass:
       begin
        if TypeInfoPtr = TypeInfo(THArrayG<Integer>) then begin
          //THArrayG<Integer> object instance has already created above, see Result := T.Create line;
          IntArr.AddValue(AValue);
          FObjects.AddValue(IntArr);
        end else if TypeInfoPtr = TypeInfo(TObject) then begin
          //TObject instance has already created above, see Result := T.Create line;
          //IntObj.Free;
          //IntObj := TTestObject.Create(AValue);
          FObjects.AddValue(IntObj);
        end else System.Assert(False, 'CreateValue<T>: Class is not supported');
       end;

     //tkDynArray:
  else
    System.Assert(False, 'CreateValue<T>: Type kind is not supported');
  end;
end;

destructor TTestBase.Destroy;
begin
  FreeTObjects;
  FreeAndNil(FObjects);
  inherited;
end;

constructor TTestBase.Create;
begin
  inherited Create;
  FObjects := THArrayG<TObject>.Create;
  FComparableTypes := [tkInteger, tkInt64, tkFloat, tkChar, tkWChar, tkString, tkUString, tkLString, tkWString, tkPointer, tkEnumeration, tkRecord];
end;

function TTestBase.CreateInteger<T>(const AValue: T): Integer;
var
  TypeDataPtr: PTypeData;
  TypeInfoPtr: PTypeInfo;
  TypeKind: TTypeKind;
  Value: T;
  I8: Int8 absolute Value;
  U8: UInt8 absolute Value;
  I16: Int16 absolute Value;
  U16: UInt16 absolute Value;
  I32: Int32 absolute Value;
  U32: UInt32 absolute Value;
  I64: Int64 absolute Value;
  UStr: UnicodeString absolute Value;
  AnsiStr: AnsiString absolute Value;
  ShortStr: ShortString absolute Value;
  IntArr: THArrayG<Integer> absolute Value;
  IntObj: TObject absolute Value;
  Pnter: Pointer absolute Value;
  Ch: Char absolute Value;
  WCh: WideChar absolute Value;
  flSingle: Single absolute Value;
  flDouble: Double absolute Value;
  flExtended: Extended absolute Value;
  flComp: Comp absolute Value;
  flCurrency: Currency absolute Value;
  Bool: Boolean absolute Value;
begin
  TypeInfoPtr := TypeInfo(T);
  TypeDataPtr := GetTypeData(TypeInfoPtr);
  TypeKind := GetTypeKind(T);
  Value := AValue;

  Result := -1; // default result

  case TypeKind of
    tkInteger:
      begin
        case TypeDataPtr.OrdType of
          otSByte: Result  := I8;
          otUByte: Result := U8;
          otSWord: Result := I16;
          otUWord: Result := U16;
          otSLong: Result := I32;
          otULong: Result := U32;
        else
          System.Assert(False, 'CreateInteger<T>: Invalid integer type');
        end;
      end;
     tkInt64: I64 := I64;

     tkUString: Result := StrToInt(UStr);
     tkLString: Result := StrToInt(string(AnsiStr));
     tkString:  Result := StrToInt(string(ShortStr));
     tkPointer: Result := Integer(Pnter);
     tkChar:    Result := Integer(Ch);
     tkWChar:   Result := Integer(WCh);

     tkFloat:
     case TypeDataPtr.FloatType of
         ftSingle:  Result :=  Round(flSingle);
         ftDouble:  Result :=  Round(flDouble);
         ftExtended:Result :=  Round(flExtended);
         ftComp:    Result :=  Round(flComp);
         ftCurr:    Result :=  Round(flCurrency);
     else
         System.Assert(False, 'CreateInteger<T>: Invalid float type');
     end;

     tkEnumeration:
     begin
       if TypeInfoPtr.Name = 'Boolean'
         then Result :=  Integer(Bool)
         else Result := U8; // real enumeration type here. enumerations cannot hold more than 256 values.
     end;

     tkClass:
       begin
        if TypeInfoPtr = TypeInfo(THArrayG<Integer>) then begin
          Result := IntArr.GetValue(0);
        end else if TypeInfoPtr = TypeInfo(TObject) then begin
          Result := Integer(IntObj);
        end else System.Assert(False, 'CreateInteger<T>: Class is not supported');
       end;
  else
    System.Assert(False, 'CreateInteger<T>: Type kind is not supported');
  end;
end;

end.
