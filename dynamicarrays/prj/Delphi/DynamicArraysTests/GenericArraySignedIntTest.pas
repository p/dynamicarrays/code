unit GenericArraySignedIntTest;

interface

uses
  DUnitX.TestFramework, TestBase, DynamicArray;

type
  [TestFixture]
  TGenericArraySignedIntTest<T:constructor> = class(TTestBase)
  private type
    TTestingArrayType = THArrayG<T>;
	private
    function Compare1(arr: THArrayG<T>; i, j: Cardinal): Integer;
    function Compare2(Item1, Item2: T): Integer;
    function IsTBoolean: Boolean;
    procedure VerifySorting(arr: THarrayG<T>);
    procedure CopyArray(src, dest: TTestingArrayType);

  protected
   // FComparableTypes: set of TTypeKind; // list of types that are compared by value in Delphi. for example class types (TObject and all descendants) are NOT compared by value
    array1: TTestingArrayType;
    array2: TTestingArrayType;
  public
    [TearDownFixture]
    procedure TearDownFixture; // this method is called when all tests of this fixture are executed. so we have to free objects from FObjects
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure TestEmpty1;
    [Test]
    procedure TestEmpty2;
    [Test]
    procedure TestCountOne;
    [Test]
    procedure TestIndexOF1;
    [Test]
    [TestCase('TestIndexofA','4, 1, 2, 0')] // last figure is used for assers when TT=Boolean
    [TestCase('TestIndexofB','1,-1, 3, 0')]
    [TestCase('TestIndexofC','0, 3,-1, 1')]
    [TestCase('TestIndexofD','-13, 13, -1, 0')]
    procedure TestIndexOf2(const AValue1, AValue2, AValue3, AValue4: Integer);
    [Test]
    procedure TestCapacity1;
    [Test]
    procedure TestCapacity2;
    [Test]
    [TestCase('TestA','1,2')]
    [TestCase('TestB','3,4')]
    [TestCase('TestD','0,-1')]
    procedure TestAddTwoDiff(const AValue1, AValue2 : Integer);
    [Test]
    [TestCase('TestA','11,   11')]
    [TestCase('TestB','  1111111,1111111')]
    [TestCase('TestC','  0, 0')]
    //[TestCase('TestD','00000000, 0000000')]
    procedure TestAddTwoEqual(const AValue1, AValue2 : Integer);
    [Test]
    procedure TestGrow;
    [Test]
    procedure TestInsert1;
    [Test]
    procedure TestInsert2;
    [Test]
    procedure TestDeleteValue1;
    [Test]
    procedure TestDeleteValue2;
    [Test]
    procedure TestAddClearAdd1;
    [Test]
    procedure TestAddClearAdd2;
    [Test]
    procedure TestSort1;

    [TestCase('TestA', '1;2;3;4;5, 1;2;3;4;5')]
    [TestCase('TestB', '5;4;3;2;1, 1;2;3;4;5')]
    [TestCase('TestC', '44;53;1;12;100;999;9, 1;9;12;44;53;100;999')]
    [TestCase('TestD', '44;44;44;44;44;44;9, ;9;44;44;44;44;44;44')]
    [TestCase('TestE', '9;1;9;1;9;1;9;1, 1;1;1;1;;9;9;9;9;;')]
    [TestCase('TestF', '15, 15')]
    [TestCase('TestG', '0, 0')]
    [TestCase('TestH', '0;0, 0;0')]
    [TestCase('TestK', '16;15, 15;16')]
    procedure TestSort2(AValue1, AValue2: string);
    [Test]
    procedure TestQuickSort1;

    // <unsorted items> , <sorted items to verify>
    [TestCase('TestA', '1;2;3;4;5, 1;2;3;4;5')]
    [TestCase('TestB', '5;4;3;2;1, 1;2;3;4;5')]
    [TestCase('TestC', '44;53;1;12;100;999;9, 1;9;12;44;53;100;999')]
    [TestCase('TestD', '44;44;44;44;44;44;9, ;9;44;44;44;44;44;44')]
    [TestCase('TestE', '9;1;9;1;9;1;9;1, 1;1;1;1;;9;9;9;9;;')]
    [TestCase('TestF', '15, 15')]
    [TestCase('TestG', '0, 0')]
    [TestCase('TestH', '0;0, 0;0')]
    [TestCase('TestK', '16;15, 15;16')]
    procedure TestQuickSort2(AValue1, AValue2: string);
    [Test]
    procedure TestHGetToken1;
    [Test]
    procedure TestAddMany;
    [Test]
    procedure TestInsertMany;
    {[Test]
    procedure TestSelectionSortPerformance;
    [Test]
    procedure TestQuickSortPerformance;
    [Test]
    procedure TestInsertSortPerformance; }
    [Test(false)]
    procedure TestShakerSortPerformance;
    [Test]
    //[MaxTime(5000)]
    procedure TestBubbleSortPerformance;
    [Test]
   // [Ignore('Ignore this test to save testing time')]
    procedure TestAllSortAlgsPerformance;  // the same data array is sorted by 5 different sorting algs, time is measured

    // <unsorted items> , <value to find> , <value index to verify>
    [TestCase('TestA1', '1;2;3;4;5, 1 , 0')]
    [TestCase('TestA2', '1;2;3;4;5, 2 , 1')]
    [TestCase('TestA3', '1;2;3;4;5, 0 , -1')]
    [TestCase('TestA4', '5;4;3;2;1, 3 , 2')]
    [TestCase('TestA5', '5;4;3;2;1, 6 , -6')]
    [TestCase('TestA6', '5;4;3;2;1, 5 , 4')]
    [TestCase('TestA7', '5;4;3;2;1, 4 , 3')]
    [TestCase('TestA8', '5;4;3;2;6;1, 6 , 5')]
    [TestCase('TestB1', '44;53;1;12;100;119;9, 100, 5 ')]
    [TestCase('TestB2', '44;53;1;12;100;119;9, 119, 6 ')]
    [TestCase('TestB3', '44;53;1;12;100;119;9, 121, -8 ')]
    [TestCase('TestB4', '44;44;44;44;44;44;9, 9 , 0 ')]
    [TestCase('TestB5', '44;44;44;44;44;44;9, 44 , 1 ')]
    [TestCase('TestB6', '44;44;44;44;44;44;9, 0 , -1 ')]
    [TestCase('TestC1', '9;1;9;1;9;1;9;1, 1, 0')]
    [TestCase('TestC2', '9;1;9;1;9;1;9;1, 9, 4')]
    [TestCase('TestC3', '9;1;9;1;9;1;9;1, 10, -9')]
    [TestCase('TestC4', '15, 15, 0')]
    [TestCase('TestD1', '0, 0, 0')]
    [TestCase('TestD2', '0, 1, -2')]
    [TestCase('TestD3', '0;0, 0, 0')]
    [TestCase('TestD4', '0;0, -1, -1')]
    [TestCase('TestD5', '16;15, 16, 1')]
    [TestCase('TestE1','1;3;4;5, 1, 0')]
    [TestCase('TestE2','1;3;4;5, 4, 2')]
    [TestCase('TestE3','1;3;4;5, 2, -2')]
    [TestCase('TestE4','1;3;4;5, 6, -5')]
    [TestCase('TestF1','22;1;0;54;4, 0, 0')]
    [TestCase('TestF2','22;1;0;54;4, 1, 1')]
    [TestCase('TestF3','22;1;0;54;4, 4, 2')]
    [TestCase('TestF4','22;1;0;54;4, 10, -4')]
    [TestCase('TestF5','22;1;0;54;4, 11, -4')]
    [TestCase('TestF6','22;1;0;54;4, 21, -4')]
    [TestCase('TestF7','22;1;0;54;4, 23, -5')]
    [TestCase('TestF8','22;1;0;54;4, 53, -5')]
    [TestCase('TestF9','22;1;0;54;4, 54, 4')]
    [TestCase('TestF10','22;1;0;54;4, 55, -6')]
    [TestCase('TestF11','22;1;0;54;4, 95, -6')]
    [TestCase('TestG1','0;1;0;0;0, 0, 0')]
    [TestCase('TestG2','0;1;0;0;0, 1, 4 ')]
    [TestCase('TestG3','0;1;0;0;0, -1, -1')]
    [TestCase('TestG4','0;1;0;0;0, 2, -6')]
    [TestCase('TestG5','0;1;0;0;0, 3, -6')]
    [TestCase('TestG6','0;-1;0;0;0, -1, 0 ')]
    [TestCase('TestG7','0;-1;0;0;0, 0, 1 ')]
    [TestCase('TestG8','0;-1;0;0;0, 1, -6')]
    [TestCase('TestG9','0;-1;0;0;0, 2, -6')]
    [TestCase('TestG10','0;1;0;-1;0, -1, 0')]
    [TestCase('TestG11','0;1;0;-1;0, 0, 1 ')]
    [TestCase('TestG12','0;1;0;-1;0, 1, 4 ')]
    [TestCase('TestG13','0;1;0;-1;0, 2, -6 ')]
    [TestCase('TestG14','0;1;0;-1;0, -2, -1')]
    [TestCase('TestK1','-4;-115;-1;-91;0, -116, -1')]
    [TestCase('TestK2','-4;-115;-1;-91;0, -115, 0')]
    [TestCase('TestK3','-4;-115;-1;-91;0, -114, -2')]
    [TestCase('TestK4','-4;-115;-1;-91;0, -55, -3')]
    [TestCase('TestK5','-4;-115;-1;-91;0, -91, 1')]
    [TestCase('TestK6','-4;-115;-1;-91;0, 0, 4')]
    [TestCase('TestK7','-4;-115;-1;-91;0, -1, 3')]
    [TestCase('TestK8','-4;-115;-1;-91;0, -2, -4')]
    [TestCase('TestK9','-4;-115;-1;-91;0, 1, -6')]
    [TestCase('TestK10','-4;-115;-1;-91;0, 2, -6')]
    //[Ignore('temporary IMGORE')]
    [Category('QuickFindTests')]
    procedure TestQuickFind(AValue1, AValue2, AValue3: string);

end;

function MillisecToStr(ms: Cardinal): string;

const
  ARRAYSIZE_FOR_SORTING = 50_000;

implementation

uses System.TypInfo, System.Generics.Defaults, SysUtils, Winapi.Windows;

function MillisecToStr(ms: Cardinal): string;
var
	milliseconds: Cardinal;
	seconds: Cardinal;
	minutes: Cardinal;
	hours: Cardinal;
begin
	milliseconds := ms mod 1000;
	seconds := (ms div 1000) mod 60;
	minutes := (ms div 60000) mod 60;
	hours := (ms div 3600000) mod 24;

	//char buf[100];
	if hours > 0 then
		Result := Format('%u h %u min %u sec %u ms', [hours, minutes, seconds, milliseconds])
	else if minutes > 0 then
		Result := Format('%u min %u sec %u ms', [minutes, seconds, milliseconds])
	else
		Result := Format('%u sec %u ms', [seconds, milliseconds]);
end;



procedure TGenericArraySignedIntTest<T>.TearDownFixture;
begin
   FreeTObjects;
end;

procedure TGenericArraySignedIntTest<T>.TestEmpty1;
begin
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(T)), array1.ItemSize);

  array1.Clear();
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(T)), array1.ItemSize);

  array1.ClearMem();
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(T)), array1.ItemSize);

  array1.Hold;
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(T)), array1.ItemSize);


  {array1.Zero;
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(T)), array1.ItemSize);
  }

end;

procedure TGenericArraySignedIntTest<T>.TestEmpty2;
var
  val: T;
begin
  val := CreateValue<T>(33);

  Assert.WillRaise(procedure begin array1.DeleteValue(0) end, ERangeError, 'd0');
  Assert.WillRaise(procedure begin array1.DeleteValue(1) end, ERangeError, 'd1');
  Assert.WillRaise(procedure begin array1.DeleteValue(2) end, ERangeError, 'd2');
  Assert.WillRaise(procedure begin array1.DeleteValue(10000) end, ERangeError, 'd10000');

  Assert.WillRaise(procedure begin array1.GetValue(0) end, ERangeError, 'g0');
  Assert.WillRaise(procedure begin array1.GetValue(1) end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin array1.GetValue(2) end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin array1.GetValue(10000) end, ERangeError, 'g10000');

  Assert.WillRaise(procedure begin array1[0] end, ERangeError, 'g0');
  Assert.WillRaise(procedure begin array1[1] end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin array1[2] end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin array1[10000] end, ERangeError, 'g10000');

  Assert.WillRaise(procedure begin array1.SetValue(0, val) end, ERangeError, 'u0');
  Assert.WillRaise(procedure begin array1.SetValue(1, val) end, ERangeError, 'u1');
  Assert.WillRaise(procedure begin array1.SetValue(2, val) end, ERangeError, 'u2');
  Assert.WillRaise(procedure begin array1.SetValue(10000, val) end, ERangeError, 'u10000');
end;

procedure TGenericArraySignedIntTest<T>.TestCountOne;
var
  val, val2: T;
begin
  val := CreateValue<T>(666);
  val2 := CreateValue<T>(9999);

  array1.AddValue(val);

  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

 // Assert.WillRaise(procedure begin arr.DeleteValue(0) end, ERangeError, 'd0');
  Assert.WillRaise(procedure begin array1.DeleteValue(1) end, ERangeError, 'd1');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.WillRaise(procedure begin array1.DeleteValue(2) end, ERangeError, 'd2');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.WillRaise(procedure begin array1.DeleteValue(10000) end, ERangeError, 'd10000');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  Assert.AreEqual<T>(val, array1.GetValue(0));
  Assert.WillRaise(procedure begin array1.GetValue(1) end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin array1.GetValue(2) end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin array1.GetValue(10000) end, ERangeError, 'g10000');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  Assert.AreEqual<T>(val, array1[0]);
  Assert.WillRaise(procedure begin array1[1] end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin array1[2] end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin array1[10000] end, ERangeError, 'g10000');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  Assert.WillNotRaiseAny(procedure begin array1.SetValue(0, val2) end);
  Assert.WillRaise(procedure begin array1.SetValue(1, val) end, ERangeError, 'u1');
  Assert.WillRaise(procedure begin array1.SetValue(2, val) end, ERangeError, 'u2');
  Assert.WillRaise(procedure begin array1.SetValue(10000, val) end, ERangeError, 'u10000');
end;

procedure TGenericArraySignedIntTest<T>.TestDeleteValue1;
var
  I: Integer;
  v: T;
begin
  for I := 0 to 9 do begin
    v := CreateValue<T>(i + 1);
    array1.AddValue(v);
    array2.AddValue(v);
  end;

  Assert.AreEqual(10, array1.Count);
  Assert.AreEqual(10, array2.Count);
  Assert.AreEqual(12, array1.Capacity);
  Assert.AreEqual(12, array2.Capacity);
  array1.DeleteValue(0);
  array2.DeleteValue(0);
  Assert.AreEqual(9, array1.Count);
  Assert.AreEqual(9, array2.Count);
  Assert.AreEqual(12, array1.Capacity);
  Assert.AreEqual(12, array2.Capacity);

  if GetTypeKind(T) in FComparableTypes then
    for I := 0 to 8 do
      Assert.AreEqual<T>(CreateValue<T>(i + 2), array1[i]);

  Assert.WillRaise(procedure begin array1.DeleteValue(array1.Count); end, ERangeError, 'DeleteCalue at Count'); // try to delete out of range element
  Assert.AreEqual(9, array1.Count);

  array1.DeleteValue(array1.Count - 1); //try to delete last element in the array
  array2.DeleteValue(array2.Count - 1); //try to delete last element in the array
  Assert.AreEqual(8, array1.Count);
  Assert.AreEqual(8, array2.Count);
  Assert.AreEqual(12, array1.Capacity);
  Assert.AreEqual(12, array2.Capacity);

  for I := 0 to 7 do
  	if GetTypeKind(T) in FComparableTypes then
			Assert.AreEqual<T>(array1[i], CreateValue<T>(i + 2))
	  else
  		Assert.AreEqual<T>(array1[i], array2[i]);
end;

procedure TGenericArraySignedIntTest<T>.TestDeleteValue2;
var
  I: Integer;
begin
	for I := 1 to 5 do
		array1.AddValue(CreateValue<T>(i + 1));

	Assert.AreEqual(5, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
	array1.DeleteValue(0);
	Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(0);
	Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(0);
	Assert.AreEqual(2, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(0);
	Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(0);
	Assert.AreEqual(0, array1.Count);
	Assert.AreEqual(8, array1.Capacity);
  Assert.WillRaise(procedure begin array1.DeleteValue(0); end, ERangeError, 'DeleteCalue at 0'); // try to delete out of range element
  Assert.WillRaise(procedure begin array1.DeleteValue(0); end, ERangeError, 'DeleteCalue at 0'); // try to delete out of range element

	for I := 1 to 5 do
		array1.AddValue(CreateValue<T>(i + 1));

	Assert.AreEqual(5, array1.Count);
	Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(1);
	Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(1);
	Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(1);
	Assert.AreEqual(2, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(1);
	Assert.AreEqual(1, array1.Count);
 	Assert.AreEqual(8, array1.Capacity);
  Assert.WillRaise(procedure begin array1.DeleteValue(1); end, ERangeError, 'DeleteCalue at 0'); // try to delete out of range element
 	Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(8, array1.Capacity);

  array1.Clear;

  for I := 1 to 5 do
		array1.AddValue(CreateValue<T>(i + 1));

	Assert.AreEqual(5, array1.Count);
	Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(2, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(1, array1.Count);
 	Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(8, array1.Capacity);

  Assert.WillRaise(procedure begin array1.DeleteValue(array1.Count); end, ERangeError, 'DeleteCalue at 0'); // try to delete out of range element
	Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
end;

procedure TGenericArraySignedIntTest<T>.TestIndexOf1;
var v, vv, vvv: T;
begin
  v :=  CreateValue<T>(777);
  vv := CreateValue<T>(4);
  vvv := CreateValue<T>(0);

  Assert.AreEqual(-1, array1.IndexOf(nil));
  Assert.AreEqual(-1, array1.IndexOf(v));

  Assert.AreEqual(-1, array1.IndexOfFrom(nil, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(nil, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(nil, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(nil, 1000));

  Assert.AreEqual(-1, array1.IndexOfFrom(v, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 1000));

  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 1000));

  Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1000));

end;

procedure TGenericArraySignedIntTest<T>.TestIndexOf2(const AValue1, AValue2, AValue3, AValue4: Integer);
var
  v, vv, vvv, vcmp: T;
  Ind: Cardinal;
  bvvv : Boolean absolute vvv;
begin
  Assert.AreNotEqual(AValue1, AValue2, 'AValue1 and AValue2 must not be equal in TestIndexOf2()!');

  v   := CreateValue<T>(AValue1);
  vv  := CreateValue<T>(AValue2);
  vvv := CreateValue<T>(Avalue3);

  Ind := array1.AddValue(v);
  Assert.AreEqual(0, Ind);
  vcmp := array1.GetValue(Ind);
  Assert.AreEqual<T>(v, vcmp);
  Assert.AreEqual(array1.IndexOf(vcmp), array1.IndexOf(v));

  Ind := array1.AddValue(vv);
  Assert.AreEqual(1, Ind);
  vcmp := array1.GetValue(Ind);
  Assert.AreEqual<T>(vv, vcmp);
  Assert.AreEqual(array1.IndexOf(vcmp), array1.IndexOf(vv));

  if IsTBoolean() then begin
    Assert.AreEqual(AValue4, array1.IndexOfFrom(true, 0));
//    Assert.AreEqual(AValue4, array1.IndexOfFrom(true, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(true, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(true, 333));

    Assert.AreEqual(AValue4, array1.IndexOfFrom(true, 0));
  //  Assert.AreEqual(AValue4, array1.IndexOfFrom(vv, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(true, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(true, 333));

    Assert.AreEqual(true, bvvv);
    Assert.AreEqual(AValue4, array1.IndexOf(vvv));
    Assert.AreEqual(AValue4, array1.IndexOfFrom(vvv, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 333));

  end else begin

  // classes (e.g. TObject) cannot be compared by Assert.AreEqual<T> because it calls v.Equals(vv)
  // while v and vv are fake instances of TObject and calling any method raises an exception
 // if GetTypeKind(T) = tkClass
 // 	then Assert.AreEqual(CreateInteger<T>(v), CreateInteger<T>(vv))
 // 	else

    Assert.AreEqual(0, array1.IndexOfFrom(v, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(v, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(v, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(v, 333));

    Assert.AreEqual(1, array1.IndexOfFrom(vv, 0));
    Assert.AreEqual(1, array1.IndexOfFrom(vv, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(vv, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(vv, 333));

    Assert.AreEqual(-1, array1.IndexOf(vvv));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 333));
  end;

   if GetTypeKind(T) = tkClass then begin
    // for non-Class types nil is converted into 0 (zero) and produces incorrect assert results when AValue1 or Avalue2 is also = 0.
    Assert.AreEqual(-1, array1.IndexOfFrom(nil, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(nil, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(nil, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(nil, 555));
  end;

end;

procedure TGenericArraySignedIntTest<T>.TestInsert1;
var
  I: Integer;
  val: T;
begin
	for I := 0 to 99 do begin
    val := CreateValue<T>(i + 1000);
		array1.InsertValue(0, val);
    array2.InsertValue(0, val);
    if array1.Count < 4 then Assert.AreEqual(4, array1.Capacity);
    if (array1.Count >= 4) AND (array1.Count < 8) then Assert.AreEqual(8, array1.Capacity);
    if (array1.Count >= 8) AND (array1.Count < 12) then Assert.AreEqual(12, array1.Capacity);
    if (array1.Count >= 12) AND (array1.Count < 28) then Assert.AreEqual(28, array1.Capacity);
    if (array1.Count >= 28) AND (array1.Count < 44) then Assert.AreEqual(44, array1.Capacity);
    if (array1.Count >= 44) AND (array1.Count < 60) then Assert.AreEqual(60, array1.Capacity);
    if (array1.Count >= 60) AND (array1.Count < 76) then Assert.AreEqual(76, array1.Capacity);
    if (array1.Count >= 76) AND (array1.Count < 95) then Assert.AreEqual(95, array1.Capacity);
    if (array1.Count >= 95) then Assert.AreEqual(118, array1.Capacity);
  end;

  Assert.AreEqual(118, array1.Capacity);
  Assert.AreEqual(118, array2.Capacity);
	Assert.AreEqual(100, array1.Count);
  Assert.AreEqual(100, array2.Count);

 	if GetTypeKind(T) in FComparableTypes then
  	for I := 0 to 99 do
      Assert.AreEqual<T>(CreateValue<T>(1100 - I - 1), array1[i])
 	else
  	for I := 0 to 99 do
			Assert.AreEqual<T>(array1[i], array2[i])

end;

procedure TGenericArraySignedIntTest<T>.TestInsert2;
var
  //I: Integer;
  v1, v2,v3: T;
begin
  v1 := CreateValue<T>(111);
  v2 := CreateValue<T>(222);
  v3 := CreateValue<T>(333);
  array1.AddValue(v1);
  Assert.AreEqual(4, array1.Capacity);
  array1.AddValue(v3);
  Assert.AreEqual(4, array1.Capacity);
  array1.InsertValue(1, v2);
  Assert.AreEqual(4, array1.Capacity);

 	if GetTypeKind(T) in FComparableTypes then
	begin
		Assert.AreEqual<T>(CreateValue<T>(111), array1[0]);
		Assert.AreEqual<T>(CreateValue<T>(222), array1[1]);
		Assert.AreEqual<T>(CreateValue<T>(333), array1[2]);
	end	else 	begin
		Assert.AreEqual<T>(v1, array1[0]);
		Assert.AreEqual<T>(v2, array1[1]);
		Assert.AreEqual<T>(v3, array1[2]);
	end;
end;

procedure TGenericArraySignedIntTest<T>.TestQuickFind(AValue1, AValue2, AValue3: string);
var
  arr: THArrayG<string>;
  I: Integer;
  FindVal: T;
  //cmp: IComparer<T>;
begin
  arr := THArrayG<string>.Create;
  try
    //cmp := TComparer<T>.Default;

    HGetTokens(AValue1, ';', False, arr);
    for I := 1 to arr.Count do
      array1.AddValue(CreateValue<T>(StrToInt(Trim(arr[i - 1]))));

    array1.QuickSort(nil);
    VerifySorting(array1);

    FindVal := CreateValue<T>(StrToInt(Trim(AValue2)));
    Assert.AreEqual(StrToInt(Trim(AValue3)), array1.QuickFind(Compare2, FindVal));

  finally
    arr.Free;
  end;

end;

procedure TGenericArraySignedIntTest<T>.TestQuickSort1;
begin
  if GetTypeKind(T) in FComparableTypes then
  begin

    Assert.AreEqual(0, array1.Count);
    array1.QuickSort(nil);
    Assert.AreEqual(0, array1.Count);

    array1.QuickSort(Compare1);
    Assert.AreEqual(0, array1.Count);

    array1.AddValue(CreateValue<T>(4));
    array1.AddValue(CreateValue<T>(3));
    array1.AddValue(CreateValue<T>(7));
    array1.AddValue(CreateValue<T>(5));
    array1.QuickSort(nil);

    Assert.AreEqual<T>(CreateValue<T>(3), array1[0]);
    Assert.AreEqual<T>(CreateValue<T>(4), array1[1]);
    Assert.AreEqual<T>(CreateValue<T>(5), array1[2]);
    Assert.AreEqual<T>(CreateValue<T>(7), array1[3]);

    array1.Clear;
    array1.AddValue(CreateValue<T>(4));
    array1.AddValue(CreateValue<T>(3));
    array1.AddValue(CreateValue<T>(7));
    array1.AddValue(CreateValue<T>(5));
    array1.QuickSort(Compare1);

    Assert.AreEqual<T>(CreateValue<T>(3), array1[0]);
    Assert.AreEqual<T>(CreateValue<T>(4), array1[1]);
    Assert.AreEqual<T>(CreateValue<T>(5), array1[2]);
    Assert.AreEqual<T>(CreateValue<T>(7), array1[3]);
  end;
end;

procedure TGenericArraySignedIntTest<T>.TestQuickSort2(AValue1, AValue2: string);
var
  arr: THArrayG<string>;
  I: Integer;
  cmp: IComparer<T>;
begin
  arr := THArrayG<string>.Create;
  try
    cmp := TComparer<T>.Default;

    HGetTokens(AValue1, ';', False, arr);
    for I := 1 to arr.Count do
      array1.AddValue(CreateValue<T>(StrToInt(Trim(arr[i-1]))));

    array1.QuickSort(nil);
    for I := 1 to array1.Count - 1 do
      Assert.isTrue(cmp.Compare(array1[i-1], array1[i]) <= 0);

    Assert.Pass('TestQuickSort2: success.');
  finally
    arr.Free;
  end;
end;

procedure TGenericArraySignedIntTest<T>.TestSort1;
begin
  if GetTypeKind(T) in FComparableTypes then
  begin

    Assert.AreEqual(0, array1.Count);
    array1.SelectionSort(nil);
    Assert.AreEqual(0, array1.Count);

    array1.SelectionSort(Compare1);
    Assert.AreEqual(0, array1.Count);

    var v4 := CreateValue<T>(4);
    var v3 := CreateValue<T>(3);
    var v7 := CreateValue<T>(7);
    var v5 := CreateValue<T>(5);
    array1.AddValue(v4);
    array1.AddValue(v3);
    array1.AddValue(v7);
    array1.AddValue(v5);
    array1.SelectionSort(nil);

    Assert.AreEqual<T>(v3, array1[0]);
    Assert.AreEqual<T>(v4, array1[1]);
    Assert.AreEqual<T>(v5, array1[2]);
    Assert.AreEqual<T>(v7, array1[3]);

    array1.Clear;
    array1.AddValue(v4);
    array1.AddValue(v3);
    array1.AddValue(v7);
    array1.AddValue(v5);
    array1.SelectionSort(Compare1);

    Assert.AreEqual<T>(v3, array1[0]);
    Assert.AreEqual<T>(v4, array1[1]);
    Assert.AreEqual<T>(v5, array1[2]);
    Assert.AreEqual<T>(v7, array1[3]);
  end;
end;

procedure TGenericArraySignedIntTest<T>.TestSort2(AValue1, AValue2: string);
var
  arr: THArrayG<string>;
  I: Integer;
  cmp: IComparer<T>;
begin
  arr := THArrayG<string>.Create;
  try
    cmp := TComparer<T>.Default;

    HGetTokens(AValue1, ';', False, arr);
    for I := 1 to arr.Count do
      array1.AddValue(CreateValue<T>(StrToInt(Trim(arr[i-1]))));

    array1.SelectionSort(nil);
    for I := 1 to array1.Count - 1 do
      Assert.isTrue(cmp.Compare(array1[i-1], array1[i]) <= 0);

    // if Avalue1 and Avalue2 contain only one value then no Assert.isTrue called in for loop above.
    // and DUnitX generates a warning "No assertion were made duringhe test"
    // to eliminate such warning Assert.Pass is added below
    Assert.Pass('TestSort2: success.');
  finally
    arr.Free;
  end;
end;

procedure TGenericArraySignedIntTest<T>.VerifySorting(arr: THarrayG<T>);
var
  i: Cardinal;
  cmp: IComparer<T>;
begin
  cmp := TComparer<T>.Default;

  for i := 1 to arr.Count - 1 do
    Assert.IsTrue(cmp.Compare(arr[i - 1], arr[i]) <= 0, '*** Array is NOT sorted ***');

  //Assert.Pass('VerifySorting: Array is properly sorted.');
end;

procedure TGenericArraySignedIntTest<T>.TestBubbleSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(T) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<T>(r));
    end;

    var start := GetTickCount;
    array1.BubbleSort(nil);
    TDUnitX.CurrentRunner.Log(Format('BubbleSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;
{
procedure TGenericArraySignedIntTest<T>.TestSelectionSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(T) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<T>(r));
    end;

    var start := GetTickCount;
    array1.SelectionSort(nil);
    TDUnitX.CurrentRunner.Log(Format('SelectionSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;
  }
procedure TGenericArraySignedIntTest<T>.TestShakerSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(T) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<T>(r));
    end;

    var start := GetTickCount;
    array1.ShakerSort(nil);
    TDUnitX.CurrentRunner.Log(Format('ShakerSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;

{
procedure TGenericArraySignedIntTest<T>.TestQuickSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(T) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<T>(r));
    end;

    var start := GetTickCount;
    array1.QuickSort(nil);
    TDUnitX.CurrentRunner.Log(Format('QuickSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;

procedure TGenericArraySignedIntTest<T>.TestInsertSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(T) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<T>(r));
    end;

    var start := GetTickCount;
    array1.InsertSort(nil);
    TDUnitX.CurrentRunner.Log(Format('InsertSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;
}
procedure TGenericArraySignedIntTest<T>.CopyArray(src, dest: TTestingArrayType);
var
  i: Cardinal;
begin
  dest.Clear;
  dest.SetCapacity(src.Capacity);

  for i := 1 to src.Count do dest.AddValue(src[i - 1]);
end;

procedure TGenericArraySignedIntTest<T>.TestAllSortAlgsPerformance;
var
  i: Integer;
begin

  if GetTypeKind(T) in FComparableTypes then begin

    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    // same initial array for all algs
    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(32_767);
      array1.AddValue(CreateValue<T>(r));
    end;

    // we will be sorting array2
    CopyArray(array1, array2);
    var start := GetTickCount;
    array2.BubbleSort(nil);
    TDUnitX.CurrentRunner.Log(Format('BubbleSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

    CopyArray(array1, array2);
    start := GetTickCount;
    array2.SelectionSort(nil);
    TDUnitX.CurrentRunner.Log(Format('SelectingSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

    CopyArray(array1, array2);
    start := GetTickCount;
    array2.ShakerSort(nil);
    TDUnitX.CurrentRunner.Log(Format('ShakerSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

    CopyArray(array1, array2);
    start := GetTickCount;
    array2.InsertSort(nil);
    TDUnitX.CurrentRunner.Log(Format('InsertSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

    CopyArray(array1, array2);
    start := GetTickCount;
    array2.QuickSort(nil);
    TDUnitX.CurrentRunner.Log(Format('QuiuckSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

  end;
end;


procedure TGenericArraySignedIntTest<T>.TestCapacity1;
begin
  Assert.AreEqual(0, array1.Capacity);

  array1.SetCapacity(1);
  Assert.AreEqual(1, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(0);
  Assert.AreEqual(0, array1.Capacity);

  array1.SetCapacity(2);
  Assert.AreEqual(2, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(3);
  Assert.AreEqual(3, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(100_000_000);
  Assert.AreEqual(100_000_000, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(0);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(100);
  Assert.AreEqual(100, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  //array1.Zero;
  //Assert.AreEqual(100, array1.Capacity);

  array1.Clear();
  Assert.AreEqual(100, array1.Capacity);

  array1.Hold;
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(200);
  array1.ClearMem();
  Assert.AreEqual(0, array1.Capacity);

end;

procedure TGenericArraySignedIntTest<T>.TestCapacity2;
var
  i: Integer;
begin
	for i := 0 to 499 do
		array1.AddValue(CreateValue<T>(i));

	Assert.AreEqual(556, array1.Capacity);

	array1.Hold();
	Assert.AreEqual(500, array1.Capacity);

	array1.SetCapacity(600);
	Assert.AreEqual(600, array1.Capacity);
	Assert.AreEqual(500, array1.Count);
	array1.SetCapacity(400);
	Assert.AreEqual(400, array1.Capacity);
	Assert.AreEqual(400, array1.Count);
	array1.SetCapacity(0);
	Assert.AreEqual(0, array1.Capacity);
	Assert.AreEqual(0, array1.Count);

end;

function TGenericArraySignedIntTest<T>.Compare1(arr: THArrayG<T>; i, j: Cardinal): integer;
const
  cmp: IComparer<T> = nil; // analogue of static variable which saves its value between function calls
begin
  if NOT Assigned(cmp) then
    cmp := TComparer<T>.Default; // should be called only once

  Result := cmp.Compare(arr[i], arr[j]);
end;

function TGenericArraySignedIntTest<T>.Compare2(Item1, Item2: T): Integer;
const
  cmp: IComparer<T> = nil; // analogue of static variable which saves its value between function calls
begin
  if NOT Assigned(cmp) then
    cmp := TComparer<T>.Default; // should be called only once

  Result := cmp.Compare(Item1, Item2);
end;

function TGenericArraySignedIntTest<T>.IsTBoolean: Boolean;
var
    TypeInfoPtr: PTypeInfo;
begin
  TypeInfoPtr := TypeInfo(T);
  Result := (GetTypeKind(T) = tkEnumeration) AND (TypeInfoPtr.Name = 'Boolean');
end;

procedure TGenericArraySignedIntTest<T>.Setup;
begin
 // FComparableTypes := [tkInteger, tkInt64, tkFloat, tkChar, tkWChar, tkString, tkUString, tkLString, tkWString, tkPointer, tkEnumeration, tkRecord];

	array1 := THArrayG<T>.Create;
  array2 := THArrayG<T>.Create;
end;

procedure TGenericArraySignedIntTest<T>.TearDown;
begin
  FreeAndNil(array1);
  FreeAndNil(array2);
end;

procedure TGenericArraySignedIntTest<T>.TestAddClearAdd1;
var
  I: Integer;
  v: T;
begin
	for I := 5 to 9 do begin
    v := CreateValue<T>(i);
		array1.InsertValue(array1.Count, v);
    array2.InsertValue(array2.Count, v);
  end;

  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);

	Assert.AreEqual(5, array1.Count);
  Assert.AreEqual(5, array2.Count);
  array1.ClearMem;
  array2.ClearMem;
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array2.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(0, array2.Capacity);

	for I := 15 to 19 do begin
    v := CreateValue<T>(i);
    array1.InsertValue(array1.Count, v);
		array2.InsertValue(array2.Count, v);
  end;

  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);

	for I := 0 to 4 do
		if GetTypeKind(T) in FComparableTypes then
        Assert.AreEqual<T>(CreateValue<T>(i + 15), array2.GetValue(i))
  	else
    	  Assert.AreEqual<T>(array2[i], array1.GetValue(i));
end;

procedure TGenericArraySignedIntTest<T>.TestAddClearAdd2;
var
  I: Integer;
  v: T;
begin

	for I := -9 to -5 do begin
    v := CreateValue<T>(i);
		array1.InsertValue(array1.Count, v);
    array2.InsertValue(array2.Count, v);
  end;

  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);
	Assert.AreEqual(5, array1.Count);
  Assert.AreEqual(5, array2.Count);
  array1.Clear;
  array2.Clear;
  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);
  Assert.AreEqual(0, array1.Count);
 	Assert.AreEqual(0, array2.Count);

	for I := -19 to -15 do begin
    v := CreateValue<T>(i);
    array1.InsertValue(array1.Count, v);
		array2.InsertValue(array2.Count, v);
  end;
  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);

  for I := 0 to 4 do
  	if GetTypeKind(T) in FComparableTypes then
  	 //	Assert.isTrue(CreateValue(I - 19) = arr2.GetValue(i))
    //  Assert.isTrue(comparer.Compare(CreateValue(I - 19), arr2[i]) = 0)
      Assert.AreEqual<T>(CreateValue<T>(I - 19), array2[i])
  else
      Assert.AreEqual<T>(array2[i], array1.GetValue(i));
end;

procedure TGenericArraySignedIntTest<T>.TestAddMany;
var
  val: T;
begin
  val := CreateValue<T>(999);
  array1.AddMany(val, 0);

  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);

  array1.AddMany(val, 1);

  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.AreEqual<T>(val, array1[0]);
  array1.Clear;

  array1.AddMany(val, 4);

  Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.AreEqual<T>(val, array1[0]);
  Assert.AreEqual<T>(val, array1[1]);
  Assert.AreEqual<T>(val, array1[2]);
  Assert.AreEqual<T>(val, array1[3]);
  array1.Clear;

  var val1 := CreateValue<T>(44);
  var val2 := CreateValue<T>(105);
  var val3 := CreateValue<T>(909);
  array1.AddValue(val1);
  array1.AddValue(val2);
  array1.AddValue(val3);
  Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  array1.AddMany(val, 5);

  Assert.AreEqual(8, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual<T>(val1, array1[0]);
  Assert.AreEqual<T>(val2, array1[1]);
  Assert.AreEqual<T>(val3, array1[2]);
  Assert.AreEqual<T>(val, array1[3]);
  Assert.AreEqual<T>(val, array1[4]);
  Assert.AreEqual<T>(val, array1[5]);
  Assert.AreEqual<T>(val, array1[6]);
  Assert.AreEqual<T>(val, array1[7]);
  array1.Clear;

end;

procedure TGenericArraySignedIntTest<T>.TestInsertMany;
var
  val: T;
begin
  val := CreateValue<T>(999);
  array1.InsertMany(0, val, 0);
  array1.InsertMany(1, val, 0);

  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);

  array1.InsertMany(0, val, 1);

  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.AreEqual<T>(val, array1[0]);
  array1.Clear;

  array1.InsertMany(0, val, 4);

  Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.AreEqual<T>(val, array1[0]);
  Assert.AreEqual<T>(val, array1[1]);
  Assert.AreEqual<T>(val, array1[2]);
  Assert.AreEqual<T>(val, array1[3]);
  array1.Clear;

  var val1 := CreateValue<T>(44);
  var val2 := CreateValue<T>(105);
  var val3 := CreateValue<T>(909);
  array1.AddValue(val1);
  array1.AddValue(val2);
  array1.AddValue(val3);
  Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  array1.InsertMany(1, val, 5);

  Assert.AreEqual(8, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual<T>(val1, array1[0]);
  Assert.AreEqual<T>(val, array1[1]);
  Assert.AreEqual<T>(val, array1[2]);
  Assert.AreEqual<T>(val, array1[3]);
  Assert.AreEqual<T>(val, array1[4]);
  Assert.AreEqual<T>(val, array1[5]);
  Assert.AreEqual<T>(val2, array1[6]);
  Assert.AreEqual<T>(val3, array1[7]);

  array1.InsertMany(0, val, 5);
  array1.InsertMany(1, val, 5);
  array1.InsertMany(array1.Count, val, 5);
  Assert.WillRaise(procedure begin array1.InsertMany(array1.Count + 1, val, 5) end, ERangeError, 'insertMany1');

end;

procedure TGenericArraySignedIntTest<T>.TestAddTwoDiff(const AValue1, AValue2 : Integer);
var
  v, vv, vvv: T;
  cmp: IComparer<T>;
begin
	Assert.AreNotEqual(AValue1, AValue2, 'AValue1 and AValue2 must be not equal in TestAddTwoDiff()!');

  v   := CreateValue<T>(AValue1);
  vv  := CreateValue<T>(AValue2);

  array1.AddValue(v);
  array1.AddValue(vv);

  Assert.AreEqual<T>(v, array1.GetValue(0));
  Assert.AreEqual<T>(vv, array1.GetValue(1));
  Assert.AreEqual<T>(v, array1.GetValue(0));

  Assert.WillRaise(procedure begin array1.GetValue(2) end, ERangeError, 'get2');
  Assert.WillRaise(procedure begin array1.GetValue(3) end, ERangeError, 'get3');
  Assert.WillRaise(procedure begin array1.GetValue(11) end, ERangeError, 'get11');

  Assert.AreEqual(0, array1.IndexOf(v));
  Assert.AreEqual(1, array1.IndexOf(vv));

  Assert.AreEqual(0, array1.IndexOfFrom(v, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 3));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 555));

  Assert.AreEqual(1, array1.IndexOfFrom(vv, 0));
  Assert.AreEqual(1, array1.IndexOfFrom(vv, 1));

  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 3));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 555));

  cmp := TComparer<T>.Default;
  if (AValue1 <> 0) AND (AValue2 <> 0) then
	begin
		vvv := CreateValue<T>(AValue1 + AValue2);
		if (cmp.Compare(vvv, v) <> 0) AND (cmp.Compare(vvv, vv) <> 0) then
			Assert.AreEqual(-1, array1.IndexOf(vvv));

		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 555));

		vvv := CreateValue<T>(AValue1 - AValue2);
		if (cmp.Compare(vvv, v) <> 0) AND (cmp.Compare(vvv, vv) <> 0) then
			Assert.AreEqual(-1, array1.IndexOf(vvv));

		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 555));
	end;
end;

procedure TGenericArraySignedIntTest<T>.TestAddTwoEqual(const AValue1, AValue2: Integer);
var
    v,vv,vvv: T;
    s: string absolute v;
    ss: string absolute vv;
   // i: Integer;
    cmp: IComparer<T>;
begin
	Assert.AreEqual(AValue1, AValue2, 'AValue1 and AValue2 must be equal in TestAddTwoEqual()!');

  v   := CreateValue<T>(AValue1);
  vv  := CreateValue<T>(AValue2);

  array1.AddValue(v);
  array1.AddValue(vv);

  if GetTypeKind(T) in FComparableTypes then begin
    Assert.AreEqual<T>(v, vv);
    Assert.AreEqual<T>(v, array1.GetValue(1));
    Assert.AreEqual<T>(vv, array1.GetValue(0));
    Assert.AreEqual(0, array1.IndexOf(v));
    Assert.AreEqual(0, array1.IndexOf(vv));
  end else begin
    Assert.AreEqual(0, array1.IndexOf(v));
    Assert.AreEqual(1, array1.IndexOf(vv));
  end;

  Assert.AreEqual<T>(v, array1.GetValue(0));
  Assert.AreEqual<T>(vv, array1.GetValue(1));

  Assert.WillRaise(procedure begin array1.GetValue(2) end, ERangeError, 'get2');
  Assert.WillRaise(procedure begin array1.GetValue(3) end, ERangeError, 'get3');
  Assert.WillRaise(procedure begin array1.GetValue(66) end, ERangeError, 'get66');


 	if GetTypeKind(T) in FComparableTypes then begin
    Assert.AreEqual(0, array1.IndexOfFrom(v, 0));
    Assert.AreEqual(1, array1.IndexOfFrom(v, 1));
    Assert.AreEqual(0, array1.IndexOfFrom(vv, 0));
    Assert.AreEqual(1, array1.IndexOfFrom(vv, 1));
  end else begin
    Assert.AreEqual(0, array1.IndexOfFrom(v, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(v, 1));
	  Assert.AreEqual(1, array1.IndexOfFrom(vv, 0));
    Assert.AreEqual(1, array1.IndexOfFrom(vv, 1));
  end;

  Assert.AreEqual(-1, array1.IndexOfFrom(v, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 3));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 555));

  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 3));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 555));

  cmp := TComparer<T>.Default;

	if AValue1 <> 0 then begin
		vvv := CreateValue<T>(AValue1 + AValue2);
		if (cmp.Compare(vvv, v) <> 0) AND (cmp.Compare(vvv, vv) <> 0) then begin
			Assert.AreEqual(-1, array1.IndexOf(vvv));
  		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
  		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
    end;

		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 555));

		vvv := CreateValue<T>(AValue1 - AValue2);
		if (cmp.Compare(vvv, v) <> 0) AND (cmp.Compare(vvv, vv) <> 0) then begin
			Assert.AreEqual(-1, array1.IndexOf(vvv));
  		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
	  	Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
    end;

		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 555));
	end;
end;

procedure TGenericArraySignedIntTest<T>.TestGrow;
var
  i: Integer;
begin
  Assert.AreEqual(0, array1.Capacity);
	array1.AddValue(CreateValue<T>(11));
	Assert.AreEqual(4, array1.Capacity);
	array1.AddValue(CreateValue<T>(22));
	array1.AddValue(CreateValue<T>(33));
	Assert.AreEqual(4, array1.Capacity);
	array1.AddValue(CreateValue<T>(44));
	Assert.AreEqual(8, array1.Capacity);
	array1.AddValue(CreateValue<T>(11));
	array1.AddValue(CreateValue<T>(55));
	array1.AddValue(CreateValue<T>(66));
	array1.AddValue(CreateValue<T>(77));
	Assert.AreEqual(12, array1.Capacity);
	array1.AddValue(CreateValue<T>(22));
	Assert.AreEqual(12, array1.Capacity);
	Assert.AreEqual(9, array1.Count);

	for i := 0 to 2 do
		array1.AddValue(CreateValue<T>(i+11));

    Assert.AreEqual(28, array1.Capacity);

 	for i := 0 to 15 do
		array1.AddValue(CreateValue<T>(i+11));

	Assert.AreEqual(28, array1.Count);
	Assert.AreEqual(44, array1.Capacity);

  for i := 0 to 15 do
		array1.AddValue(CreateValue<T>(i+11));

	Assert.AreEqual(44, array1.Count);
	Assert.AreEqual(60, array1.Capacity);

  for i := 0 to 15 do
		array1.AddValue(CreateValue<T>(i+11));

	Assert.AreEqual(60, array1.Count);
	Assert.AreEqual(76, array1.Capacity);

  for i := 0 to 15 do
		array1.AddValue(CreateValue<T>(i+11));

	Assert.AreEqual(76, array1.Count);
 	Assert.AreEqual(95, array1.Capacity);

  for i := 0 to 18 do
		array1.AddValue(CreateValue<T>(i+11));

	Assert.AreEqual(95, array1.Count);
	Assert.AreEqual(118, array1.Capacity);

	array1.Clear();
	Assert.AreEqual(0, array1.Count);
	Assert.AreEqual(118, array1.Capacity);

	array1.GrowTo(100);
	Assert.AreEqual(0, array1.Count);
	Assert.AreEqual(118, array1.Capacity);
end;


procedure TGenericArraySignedIntTest<T>.TestHGetToken1;
var
   flag: Boolean;
   arr: THArrayG<string>;
begin
  arr := THArrayG<string>.Create;

  for flag := False to true do
  begin
    arr.Clear;
    HGetTokens('', '', flag, arr);
    Assert.AreEqual(0, arr.Count);

    arr.Clear;
    HGetTokens('a', '', flag, arr);
    Assert.AreEqual(1, arr.Count);
    Assert.AreEqual('a', arr[0]);

    arr.Clear;
    HGetTokens('a', ',', flag, arr);
    Assert.AreEqual(1, arr.Count);
    Assert.AreEqual('a', arr[0]);

    arr.Clear;
    HGetTokens('abc', '', flag, arr);
    Assert.AreEqual(1, arr.Count);
    Assert.AreEqual('abc', arr[0]);

    arr.Clear;
    HGetTokens('', '.', flag, arr);
    Assert.AreEqual(0, arr.Count);

    arr.Clear;
    HGetTokens('', '..', flag, arr);
    Assert.AreEqual(0, arr.Count);

    arr.Clear;
    HGetTokens('', 'a', flag, arr);
    Assert.AreEqual(0, arr.Count);

    arr.Clear;
    HGetTokens('a c', ' ', flag, arr);
    Assert.AreEqual(2, arr.Count);
    Assert.AreEqual('a', arr[0]);
    Assert.AreEqual('c', arr[1]);

    arr.Clear;
    HGetTokens('a c', '  ', flag, arr);
    Assert.AreEqual(2, arr.Count);
    Assert.AreEqual('a', arr[0]);
    Assert.AreEqual('c', arr[1]);

    arr.Clear;
    HGetTokens(';a c;', ';', flag, arr);
    Assert.AreEqual(1, arr.Count);
    Assert.AreEqual('a c', arr[0]);

    arr.Clear;
    HGetTokens(';a;c;', ';;', flag, arr);
    Assert.AreEqual(2, arr.Count);
    Assert.AreEqual('a', arr[0]);
    Assert.AreEqual('c', arr[1]);

    arr.Clear;
    HGetTokens('auto;moto velo foto;', ';', flag, arr);
    Assert.AreEqual(2, arr.Count);
    Assert.AreEqual('auto', arr[0]);
    Assert.AreEqual('moto velo foto', arr[1]);

    arr.Clear;
    HGetTokens('auto;moto velo foto;.', ';', flag, arr);
    Assert.AreEqual(3, arr.Count);
    Assert.AreEqual('auto', arr[0]);
    Assert.AreEqual('moto velo foto', arr[1]);
    Assert.AreEqual('.', arr[2]);
  end;

  arr.Clear;
  HGetTokens(';;a;;c;;', ';', False, arr);
  Assert.AreEqual(2, arr.Count);
  Assert.AreEqual('a', arr[0]);
  Assert.AreEqual('c', arr[1]);

  arr.Clear;
  HGetTokens(';;a;;c;;', ';', True, arr);
  Assert.AreEqual(4, arr.Count);
  Assert.AreEqual('a', arr[0]);
  Assert.AreEqual('', arr[1]);
  Assert.AreEqual('c', arr[2]);
  Assert.AreEqual('', arr[3]);

  arr.Free;
end;

initialization

   Randomize; // need for testing sorting algs performance



   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<Integer>);
   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<SmallInt>);
   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<ShortInt>);
   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<FixedInt>);
   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<Int64>);

   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<Single>);
   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<Double>);
   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<Real>);
   TDUnitX.RegisterTestFixture(TGenericArraySignedIntTest<Currency>);

 end.
