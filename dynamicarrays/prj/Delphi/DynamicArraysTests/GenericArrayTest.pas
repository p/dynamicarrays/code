unit GenericArrayTest;

interface

uses
  DUnitX.TestFramework, TestBase, DynamicArray;

type
  [TestFixture]
  TGenericArrayTest<TT:constructor> = class(TTestBase)
  private type
    TTestingArrayType = THArrayG<TT>;
	private
    function Compare1(arr: THArrayG<TT>; i, j: Cardinal): Integer;
    function Compare2(Item1, Item2: TT): Integer;
    function IsTTBoolean: Boolean;
    procedure VerifySorting(arr: THarrayG<TT>);
    procedure CopyArray(arr1, arr2: TTestingArrayType);

  protected
    array1: TTestingArrayType;
    array2: TTestingArrayType;
  public
    [TearDownFixture]
    procedure TearDownFixture; // this method is called when all tests of this fixture are executed. so we have to free objects from FObjects
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure TestEmpty1;
    [Test]
    procedure TestEmpty2;
    [Test]
    procedure TestCountOne;
    [Test]
    procedure TestIndexOF1;
    [Test]
    [TestCase('TestIndexofA','4, 1, 2, 0')] // last figure is used for assers when TT=Boolean
    [TestCase('TestIndexofB','1,-1, 3, 0')]
    [TestCase('TestIndexofC','0, 3,-1, 1')]
    [TestCase('TestIndexofD','-13, 13, -1, 0')]
    procedure TestIndexOf2(const AValue1, AValue2, AValue3, AValue4: Integer);
    [Test]
    procedure TestCapacity1;
    [Test]
    procedure TestCapacity2;
    [Test]
    [TestCase('TestA','1,2')]
    [TestCase('TestB','3,4')]
    [TestCase('TestD','0,-1')]
    procedure TestAddTwoDiff(const AValue1, AValue2 : Integer);
    [Test]
    [TestCase('TestA','11,   11')]
    [TestCase('TestB','  1111111,1111111')]
    [TestCase('TestC','  0, 0')]
    //[TestCase('TestD','00000000, 0000000')]
    procedure TestAddTwoEqual(const AValue1, AValue2 : Integer);
    [Test]
    procedure TestGrow;
    [Test]
    procedure TestInsert1;
    [Test]
    procedure TestInsert2;
    [Test]
    procedure TestDeleteValue1;
    [Test]
    procedure TestDeleteValue2;
    [Test]
    procedure TestAddClearAdd1;
    [Test]
    procedure TestAddClearAdd2;
    [Test]
    procedure TestSort1;

    [TestCase('TestA', '1;2;3;4;5, 1;2;3;4;5')]
    [TestCase('TestB', '5;4;3;2;1, 1;2;3;4;5')]
    [TestCase('TestC', '44;53;1;12;100;999;9, 1;9;12;44;53;100;999')]
    [TestCase('TestD', '44;44;44;44;44;44;9, ;9;44;44;44;44;44;44')]
    [TestCase('TestE', '9;1;9;1;9;1;9;1, 1;1;1;1;;9;9;9;9;;')]
    [TestCase('TestF', '15, 15')]
    [TestCase('TestG', '0, 0')]
    [TestCase('TestH', '0;0, 0;0')]
    [TestCase('TestK', '16;15, 15;16')]
    procedure TestSort2(AValue1, AValue2: string);
    [Test]
    procedure TestQuickSort1;

    // <unsorted items> , <sorted items to verify>
    [TestCase('TestA', '1;2;3;4;5, 1;2;3;4;5')]
    [TestCase('TestB', '5;4;3;2;1, 1;2;3;4;5')]
    [TestCase('TestC', '44;53;1;12;100;999;9, 1;9;12;44;53;100;999')]
    [TestCase('TestD', '44;44;44;44;44;44;9, ;9;44;44;44;44;44;44')]
    [TestCase('TestE', '9;1;9;1;9;1;9;1, 1;1;1;1;;9;9;9;9;;')]
    [TestCase('TestF', '15, 15')]
    [TestCase('TestG', '0, 0')]
    [TestCase('TestH', '0;0, 0;0')]
    [TestCase('TestK', '16;15, 15;16')]
    procedure TestQuickSort2(AValue1, AValue2: string);
    [Test]
    procedure TestHGetToken1;
    [Test]
    procedure TestAddMany;
    [Test]
    procedure TestInsertMany;
    {[Test]
    procedure TestSelectionSortPerformance;
    [Test]
    procedure TestQuickSortPerformance;
    [Test]
    procedure TestInsertSortPerformance; }
    [Test(false)]
    procedure TestShakerSortPerformance;
    [Test]
    //[MaxTime(5000)]
    procedure TestBubbleSortPerformance;
    [Test]
    //[Ignore('Ignore this test to save testing time')]
    procedure TestAllSortAlgsPerformance;  // the same data array is sorted by 5 different sorting algs, time is measured

    // <unsorted items> , <value to find> , <value index to verify>
    [TestCase('TestA1', '1;2;3;4;5, 1 , 0')]
    [TestCase('TestA2', '1;2;3;4;5, 2 , 1')]
    [TestCase('TestA3', '1;2;3;4;5, 0 , -1')]
    [TestCase('TestA4', '5;4;3;2;1, 3 , 2')]
    [TestCase('TestA5', '5;4;3;2;1, 6 , -6')]
    [TestCase('TestA6', '5;4;3;2;1, 5 , 4')]
    [TestCase('TestA7', '5;4;3;2;1, 4 , 3')]
    [TestCase('TestA8', '5;4;3;2;6;1, 6 , 5')]
    [TestCase('TestC1', '44;53;1;12;100;109;9, 100, 1 ')]
    [TestCase('TestC2', '44;53;1;12;100;109;9, 109, 2 ')]
    [TestCase('TestC3', '44;53;1;12;100;109;9, 1000, -3 ')]
    [TestCase('TestD1', '44;44;44;44;44;44;9, 9 , 6 ')]
    [TestCase('TestD2', '44;44;44;44;44;44;9, 44 , 0 ')]
    [TestCase('TestD3', '44;44;44;44;44;44;9, 0 , -1 ')]
    [TestCase('TestE1', '9;1;9;1;9;1;9;1, 1, 0')]
    [TestCase('TestE2', '9;1;9;1;9;1;9;1, 9, 4')]
    [TestCase('TestE3', '9;1;9;1;9;1;9;1, 10, -5')]
    [TestCase('TestH', '15, 15, 0')]
    [TestCase('TestK1', '0, 0, 0')]
    [TestCase('TestK2', '0, 1, -2')]
    [TestCase('TestL1', '0;0, 0, 0')]
    [TestCase('TestL2', '0;0, -1, -1')]
    [TestCase('TestM', '16;15, 16, 1')]
    //[Ignore('temporary IMGORE')]
    [Category('QuickFindTests')]
    procedure TestQuickFind(AValue1, AValue2, AValue3: string);

end;

function MillisecToStr(ms: Cardinal): string;

const
  ARRAYSIZE_FOR_SORTING = 50_000;

implementation

uses System.TypInfo, System.Generics.Defaults, SysUtils, Winapi.Windows;

function MillisecToStr(ms: Cardinal): string;
var
	milliseconds: Cardinal;
	seconds: Cardinal;
	minutes: Cardinal;
	hours: Cardinal;
begin
	milliseconds := ms mod 1000;
	seconds := (ms div 1000) mod 60;
	minutes := (ms div 60000) mod 60;
	hours := (ms div 3600000) mod 24;

	//char buf[100];
	if hours > 0 then
		Result := Format('%u h %u min %u sec %u ms', [hours, minutes, seconds, milliseconds])
	else if minutes > 0 then
		Result := Format('%u min %u sec %u ms', [minutes, seconds, milliseconds])
	else
		Result := Format('%u sec %u ms', [seconds, milliseconds]);
end;



procedure TGenericArrayTest<TT>.TearDownFixture;
begin
   FreeTObjects;
end;

procedure TGenericArrayTest<TT>.TestEmpty1;
begin
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(TT)), array1.ItemSize);

  array1.Clear();
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(TT)), array1.ItemSize);

  array1.ClearMem();
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(TT)), array1.ItemSize);

  array1.Hold;
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(TT)), array1.ItemSize);


  {array1.Zero;
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(Cardinal(sizeof(TT)), array1.ItemSize);
  }

end;

procedure TGenericArrayTest<TT>.TestEmpty2;
var
  val: TT;
begin
  val := CreateValue<TT>(33);

  Assert.WillRaise(procedure begin array1.DeleteValue(0) end, ERangeError, 'd0');
  Assert.WillRaise(procedure begin array1.DeleteValue(1) end, ERangeError, 'd1');
  Assert.WillRaise(procedure begin array1.DeleteValue(2) end, ERangeError, 'd2');
  Assert.WillRaise(procedure begin array1.DeleteValue(10000) end, ERangeError, 'd10000');

  Assert.WillRaise(procedure begin array1.GetValue(0) end, ERangeError, 'g0');
  Assert.WillRaise(procedure begin array1.GetValue(1) end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin array1.GetValue(2) end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin array1.GetValue(10000) end, ERangeError, 'g10000');

  Assert.WillRaise(procedure begin array1[0] end, ERangeError, 'g0');
  Assert.WillRaise(procedure begin array1[1] end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin array1[2] end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin array1[10000] end, ERangeError, 'g10000');

  Assert.WillRaise(procedure begin array1.SetValue(0, val) end, ERangeError, 'u0');
  Assert.WillRaise(procedure begin array1.SetValue(1, val) end, ERangeError, 'u1');
  Assert.WillRaise(procedure begin array1.SetValue(2, val) end, ERangeError, 'u2');
  Assert.WillRaise(procedure begin array1.SetValue(10000, val) end, ERangeError, 'u10000');
end;

procedure TGenericArrayTest<TT>.TestCountOne;
var
  val, val2: TT;
begin
  val := CreateValue<TT>(666);
  val2 := CreateValue<TT>(9999);

  array1.AddValue(val);

  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

 // Assert.WillRaise(procedure begin arr.DeleteValue(0) end, ERangeError, 'd0');
  Assert.WillRaise(procedure begin array1.DeleteValue(1) end, ERangeError, 'd1');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.WillRaise(procedure begin array1.DeleteValue(2) end, ERangeError, 'd2');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.WillRaise(procedure begin array1.DeleteValue(10000) end, ERangeError, 'd10000');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  Assert.AreEqual<TT>(val, array1.GetValue(0));
  Assert.WillRaise(procedure begin array1.GetValue(1) end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin array1.GetValue(2) end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin array1.GetValue(10000) end, ERangeError, 'g10000');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  Assert.AreEqual<TT>(val, array1[0]);
  Assert.WillRaise(procedure begin array1[1] end, ERangeError, 'g1');
  Assert.WillRaise(procedure begin array1[2] end, ERangeError, 'g2');
  Assert.WillRaise(procedure begin array1[10000] end, ERangeError, 'g10000');
  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  Assert.WillNotRaiseAny(procedure begin array1.SetValue(0, val2) end);
  Assert.WillRaise(procedure begin array1.SetValue(1, val) end, ERangeError, 'u1');
  Assert.WillRaise(procedure begin array1.SetValue(2, val) end, ERangeError, 'u2');
  Assert.WillRaise(procedure begin array1.SetValue(10000, val) end, ERangeError, 'u10000');
end;

procedure TGenericArrayTest<TT>.TestDeleteValue1;
var
  I: Integer;
  v: TT;
begin
	//array1 := THArrayG<TT>.Create;
  //array2 := THArrayG<TT>.Create;

  for I := 0 to 9 do begin
    v := CreateValue<TT>(i + 1);
    array1.AddValue(v);
    array2.AddValue(v);
  end;

  Assert.AreEqual(10, array1.Count);
  Assert.AreEqual(10, array2.Count);
  Assert.AreEqual(12, array1.Capacity);
  Assert.AreEqual(12, array2.Capacity);
  array1.DeleteValue(0);
  array2.DeleteValue(0);
  Assert.AreEqual(9, array1.Count);
  Assert.AreEqual(9, array2.Count);
  Assert.AreEqual(12, array1.Capacity);
  Assert.AreEqual(12, array2.Capacity);

  if GetTypeKind(TT) in FComparableTypes then
    for I := 0 to 8 do
      Assert.AreEqual<TT>(CreateValue<TT>(i + 2), array1[i]);

  Assert.WillRaise(procedure begin array1.DeleteValue(array1.Count); end, ERangeError, 'DeleteCalue at Count'); // try to delete out of range element
  Assert.AreEqual(9, array1.Count);

  array1.DeleteValue(array1.Count - 1); //try to delete last element in the array
  array2.DeleteValue(array2.Count - 1); //try to delete last element in the array
  Assert.AreEqual(8, array1.Count);
  Assert.AreEqual(8, array2.Count);
  Assert.AreEqual(12, array1.Capacity);
  Assert.AreEqual(12, array2.Capacity);

  for I := 0 to 7 do
  	if GetTypeKind(TT) in FComparableTypes then
			Assert.AreEqual<TT>(array1[i], CreateValue<TT>(i + 2))
	  else
  		Assert.AreEqual<TT>(array1[i], array2[i]);
end;

procedure TGenericArrayTest<TT>.TestDeleteValue2;
var
  I: Integer;
begin
	for I := 1 to 5 do
		array1.AddValue(CreateValue<TT>(i + 1));

	Assert.AreEqual(5, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
	array1.DeleteValue(0);
	Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(0);
	Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(0);
	Assert.AreEqual(2, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(0);
	Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(0);
	Assert.AreEqual(0, array1.Count);
	Assert.AreEqual(8, array1.Capacity);
  Assert.WillRaise(procedure begin array1.DeleteValue(0); end, ERangeError, 'DeleteCalue at 0'); // try to delete out of range element
  Assert.WillRaise(procedure begin array1.DeleteValue(0); end, ERangeError, 'DeleteCalue at 0'); // try to delete out of range element

	for I := 1 to 5 do
		array1.AddValue(CreateValue<TT>(i + 1));

	Assert.AreEqual(5, array1.Count);
	Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(1);
	Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(1);
	Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(1);
	Assert.AreEqual(2, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(1);
	Assert.AreEqual(1, array1.Count);
 	Assert.AreEqual(8, array1.Capacity);
  Assert.WillRaise(procedure begin array1.DeleteValue(1); end, ERangeError, 'DeleteCalue at 0'); // try to delete out of range element
 	Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(8, array1.Capacity);

  array1.Clear;

  for I := 1 to 5 do
		array1.AddValue(CreateValue<TT>(i + 1));

	Assert.AreEqual(5, array1.Count);
	Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(2, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(1, array1.Count);
 	Assert.AreEqual(8, array1.Capacity);
  array1.DeleteValue(array1.Count - 1);
	Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(8, array1.Capacity);

  Assert.WillRaise(procedure begin array1.DeleteValue(array1.Count); end, ERangeError, 'DeleteCalue at 0'); // try to delete out of range element
	Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
end;

procedure TGenericArrayTest<TT>.TestIndexOf1;
var v, vv, vvv: TT;
begin
  v :=  CreateValue<TT>(777);
  vv := CreateValue<TT>(4);
  vvv := CreateValue<TT>(0);

  Assert.AreEqual(-1, array1.IndexOf(nil));
  Assert.AreEqual(-1, array1.IndexOf(v));

  Assert.AreEqual(-1, array1.IndexOfFrom(nil, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(nil, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(nil, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(nil, 1000));

  Assert.AreEqual(-1, array1.IndexOfFrom(v, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 1000));

  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 1000));

  Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1000));

end;

procedure TGenericArrayTest<TT>.TestIndexOf2(const AValue1, AValue2, AValue3, AValue4: Integer);
var
  v, vv, vvv, vcmp: TT;
  Ind: Cardinal;
  bvvv : Boolean absolute vvv;
begin
  Assert.AreNotEqual(AValue1, AValue2, 'AValue1 and AValue2 must not be equal in TestIndexOf2()!');

  v   := CreateValue<TT>(AValue1);
  vv  := CreateValue<TT>(AValue2);
  vvv := CreateValue<TT>(Avalue3);

  Ind := array1.AddValue(v);
  Assert.AreEqual(0, Ind);
  vcmp := array1.GetValue(Ind);
  Assert.AreEqual<TT>(v, vcmp);
  Assert.AreEqual(array1.IndexOf(vcmp), array1.IndexOf(v));

  Ind := array1.AddValue(vv);
  Assert.AreEqual(1, Ind);
  vcmp := array1.GetValue(Ind);
  Assert.AreEqual<TT>(vv, vcmp);
  Assert.AreEqual(array1.IndexOf(vcmp), array1.IndexOf(vv));

  if IsTTBoolean() then begin
    Assert.AreEqual(AValue4, array1.IndexOfFrom(true, 0));
//    Assert.AreEqual(AValue4, array1.IndexOfFrom(true, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(true, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(true, 333));

    Assert.AreEqual(AValue4, array1.IndexOfFrom(true, 0));
  //  Assert.AreEqual(AValue4, array1.IndexOfFrom(vv, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(true, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(true, 333));

    Assert.AreEqual(true, bvvv);
    Assert.AreEqual(AValue4, array1.IndexOf(vvv));
    Assert.AreEqual(AValue4, array1.IndexOfFrom(vvv, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 333));

  end else begin

  // classes (e.g. TObject) cannot be compared by Assert.AreEqual<TT> because it calls v.Equals(vv)
  // while v and vv are fake instances of TObject and calling any method raises an exception
 // if GetTypeKind(TT) = tkClass
 // 	then Assert.AreEqual(CreateInteger<TT>(v), CreateInteger<TT>(vv))
 // 	else

    Assert.AreEqual(0, array1.IndexOfFrom(v, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(v, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(v, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(v, 333));

    Assert.AreEqual(1, array1.IndexOfFrom(vv, 0));
    Assert.AreEqual(1, array1.IndexOfFrom(vv, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(vv, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(vv, 333));

    Assert.AreEqual(-1, array1.IndexOf(vvv));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 333));
  end;

   if GetTypeKind(TT) = tkClass then begin
    // for non-Class types nil is converted into 0 (zero) and produces incorrect assert results when AValue1 or Avalue2 is also = 0.
    Assert.AreEqual(-1, array1.IndexOfFrom(nil, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(nil, 1));
    Assert.AreEqual(-1, array1.IndexOfFrom(nil, 2));
    Assert.AreEqual(-1, array1.IndexOfFrom(nil, 555));
  end;

end;

procedure TGenericArrayTest<TT>.TestInsert1;
var
  I: Integer;
  val: TT;
begin
	for I := 0 to 99 do begin
    val := CreateValue<TT>(i + 1000);
		array1.InsertValue(0, val);
    array2.InsertValue(0, val);
    if array1.Count < 4 then Assert.AreEqual(4, array1.Capacity);
    if (array1.Count >= 4) AND (array1.Count < 8) then Assert.AreEqual(8, array1.Capacity);
    if (array1.Count >= 8) AND (array1.Count < 12) then Assert.AreEqual(12, array1.Capacity);
    if (array1.Count >= 12) AND (array1.Count < 28) then Assert.AreEqual(28, array1.Capacity);
    if (array1.Count >= 28) AND (array1.Count < 44) then Assert.AreEqual(44, array1.Capacity);
    if (array1.Count >= 44) AND (array1.Count < 60) then Assert.AreEqual(60, array1.Capacity);
    if (array1.Count >= 60) AND (array1.Count < 76) then Assert.AreEqual(76, array1.Capacity);
    if (array1.Count >= 76) AND (array1.Count < 95) then Assert.AreEqual(95, array1.Capacity);
    if (array1.Count >= 95) then Assert.AreEqual(118, array1.Capacity);
  end;

  Assert.AreEqual(118, array1.Capacity);
  Assert.AreEqual(118, array2.Capacity);
	Assert.AreEqual(100, array1.Count);
  Assert.AreEqual(100, array2.Count);

 	if GetTypeKind(TT) in FComparableTypes then
  	for I := 0 to 99 do
      Assert.AreEqual<TT>(CreateValue<TT>(1100 - I - 1), array1[i])
 	else
  	for I := 0 to 99 do
			Assert.AreEqual<TT>(array1[i], array2[i])

end;

procedure TGenericArrayTest<TT>.TestInsert2;
var
  //I: Integer;
  v1, v2,v3: TT;
begin
  v1 := CreateValue<TT>(111);
  v2 := CreateValue<TT>(222);
  v3 := CreateValue<TT>(333);
  array1.AddValue(v1);
  Assert.AreEqual(4, array1.Capacity);
  array1.AddValue(v3);
  Assert.AreEqual(4, array1.Capacity);
  array1.InsertValue(1, v2);
  Assert.AreEqual(4, array1.Capacity);

 	if GetTypeKind(TT) in FComparableTypes then
	begin
		Assert.AreEqual<TT>(CreateValue<TT>(111), array1[0]);
		Assert.AreEqual<TT>(CreateValue<TT>(222), array1[1]);
		Assert.AreEqual<TT>(CreateValue<TT>(333), array1[2]);
	end	else 	begin
		Assert.AreEqual<TT>(v1, array1[0]);
		Assert.AreEqual<TT>(v2, array1[1]);
		Assert.AreEqual<TT>(v3, array1[2]);
	end;
end;

procedure TGenericArrayTest<TT>.TestQuickFind(AValue1, AValue2, AValue3: string);
var
  arr: THArrayG<string>;
  I: Integer;
  FindVal: TT;
begin
  if NOT (GetTypeKind(tt) in FComparableTypes) then exit;

  arr := THArrayG<string>.Create;
  try
    HGetTokens(AValue1, ';', False, arr);
    for I := 1 to arr.Count do
      array1.AddValue(CreateValue<TT>(StrToInt(Trim(arr[i - 1]))));

    array1.QuickSort(nil);
    VerifySorting(array1);

    FindVal := CreateValue<TT>(StrToInt(Trim(AValue2)));
    Assert.AreEqual(StrToInt(Trim(AValue3)), array1.QuickFind(Compare2, FindVal));

  finally
    arr.Free;
  end;

end;

procedure TGenericArrayTest<TT>.TestQuickSort1;
begin
  if GetTypeKind(TT) in FComparableTypes then
  begin

    Assert.AreEqual(0, array1.Count);
    array1.QuickSort(nil);
    Assert.AreEqual(0, array1.Count);

    array1.QuickSort(Compare1);
    Assert.AreEqual(0, array1.Count);

    array1.AddValue(CreateValue<TT>(4));
    array1.AddValue(CreateValue<TT>(3));
    array1.AddValue(CreateValue<TT>(7));
    array1.AddValue(CreateValue<TT>(5));
    array1.QuickSort(nil);

    Assert.AreEqual<TT>(CreateValue<TT>(3), array1[0]);
    Assert.AreEqual<TT>(CreateValue<TT>(4), array1[1]);
    Assert.AreEqual<TT>(CreateValue<TT>(5), array1[2]);
    Assert.AreEqual<TT>(CreateValue<TT>(7), array1[3]);

    array1.Clear;
    array1.AddValue(CreateValue<TT>(4));
    array1.AddValue(CreateValue<TT>(3));
    array1.AddValue(CreateValue<TT>(7));
    array1.AddValue(CreateValue<TT>(5));
    array1.QuickSort(Compare1);

    Assert.AreEqual<TT>(CreateValue<TT>(3), array1[0]);
    Assert.AreEqual<TT>(CreateValue<TT>(4), array1[1]);
    Assert.AreEqual<TT>(CreateValue<TT>(5), array1[2]);
    Assert.AreEqual<TT>(CreateValue<TT>(7), array1[3]);
  end;
end;

procedure TGenericArrayTest<TT>.TestQuickSort2(AValue1, AValue2: string);
var
  arr: THArrayG<string>;
  I: Integer;
  cmp: IComparer<TT>;
begin
  arr := THArrayG<string>.Create;
  try
    cmp := TComparer<TT>.Default;

    HGetTokens(AValue1, ';', False, arr);
    for I := 1 to arr.Count do
      array1.AddValue(CreateValue<TT>(StrToInt(Trim(arr[i-1]))));

    array1.QuickSort(nil);
    for I := 1 to array1.Count - 1 do
      Assert.isTrue(cmp.Compare(array1[i-1], array1[i]) <= 0);

    Assert.Pass('TestQuickSort2: success.');
  finally
    arr.Free;
  end;
end;

procedure TGenericArrayTest<TT>.TestSort1;
begin
  if GetTypeKind(TT) in FComparableTypes then
  begin

    Assert.AreEqual(0, array1.Count);
    array1.SelectionSort(nil);
    Assert.AreEqual(0, array1.Count);

    array1.SelectionSort(Compare1);
    Assert.AreEqual(0, array1.Count);

    var v4 := CreateValue<TT>(4);
    var v3 := CreateValue<TT>(3);
    var v7 := CreateValue<TT>(7);
    var v5 := CreateValue<TT>(5);
    array1.AddValue(v4);
    array1.AddValue(v3);
    array1.AddValue(v7);
    array1.AddValue(v5);
    array1.SelectionSort(nil);

    Assert.AreEqual<TT>(v3, array1[0]);
    Assert.AreEqual<TT>(v4, array1[1]);
    Assert.AreEqual<TT>(v5, array1[2]);
    Assert.AreEqual<TT>(v7, array1[3]);

    array1.Clear;
    array1.AddValue(v4);
    array1.AddValue(v3);
    array1.AddValue(v7);
    array1.AddValue(v5);
    array1.SelectionSort(Compare1);

    Assert.AreEqual<TT>(v3, array1[0]);
    Assert.AreEqual<TT>(v4, array1[1]);
    Assert.AreEqual<TT>(v5, array1[2]);
    Assert.AreEqual<TT>(v7, array1[3]);
  end;
end;

procedure TGenericArrayTest<TT>.TestSort2(AValue1, AValue2: string);
var
  arr: THArrayG<string>;
  I: Integer;
  cmp: IComparer<TT>;
begin
  arr := THArrayG<string>.Create;
  try
    cmp := TComparer<TT>.Default;

    HGetTokens(AValue1, ';', False, arr);
    for I := 1 to arr.Count do
      array1.AddValue(CreateValue<TT>(StrToInt(Trim(arr[i-1]))));

    array1.SelectionSort(nil);
    for I := 1 to array1.Count - 1 do
      Assert.isTrue(cmp.Compare(array1[i-1], array1[i]) <= 0);

    // if Avalue1 and Avalue2 contain only one value then no Assert.isTrue called in for loop above.
    // and DUnitX generates a warning "No assertion were made duringhe test"
    // to eliminate such warning Assert.Pass is added below
    Assert.Pass('TestSort2: success.');
  finally
    arr.Free;
  end;
end;

procedure TGenericArrayTest<TT>.VerifySorting(arr: THarrayG<TT>);
var
  i: Cardinal;
  cmp: IComparer<TT>;
begin
  cmp := TComparer<TT>.Default;

  for i := 1 to arr.Count - 1 do
    Assert.IsTrue(cmp.Compare(arr[i - 1], arr[i]) <= 0, '*** Array is NOT sorted ***');

  //Assert.Pass('VerifySorting: Array is properly sorted.');
end;

procedure TGenericArrayTest<TT>.TestBubbleSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(TT) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<TT>(r));
    end;

    var start := GetTickCount;
    array1.BubbleSort(nil);
    TDUnitX.CurrentRunner.Log(Format('BubbleSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;
{
procedure TGenericArrayTest<TT>.TestSelectionSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(TT) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<TT>(r));
    end;

    var start := GetTickCount;
    array1.SelectionSort(nil);
    TDUnitX.CurrentRunner.Log(Format('SelectionSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;
  }
procedure TGenericArrayTest<TT>.TestShakerSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(TT) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<TT>(r));
    end;

    var start := GetTickCount;
    array1.ShakerSort(nil);
    TDUnitX.CurrentRunner.Log(Format('ShakerSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;

{
procedure TGenericArrayTest<TT>.TestQuickSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(TT) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<TT>(r));
    end;

    var start := GetTickCount;
    array1.QuickSort(nil);
    TDUnitX.CurrentRunner.Log(Format('QuickSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;

procedure TGenericArrayTest<TT>.TestInsertSortPerformance;
var
  i: Integer;
begin
  if GetTypeKind(TT) in FComparableTypes then begin
    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(10_000);
      array1.AddValue(CreateValue<TT>(r));
    end;

    var start := GetTickCount;
    array1.InsertSort(nil);
    TDUnitX.CurrentRunner.Log(Format('InsertSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));

    VerifySorting(array1);
  end;
end;
}
procedure TGenericArrayTest<TT>.CopyArray(arr1, arr2: TTestingArrayType);
var
  i: Cardinal;
begin
  arr2.Clear;
  arr2.SetCapacity(arr1.Capacity);

  for i := 0 to arr1.Count - 1 do arr2.AddValue(arr1[i]);
end;

procedure TGenericArrayTest<TT>.TestAllSortAlgsPerformance;
var
  i: Integer;
begin

  if GetTypeKind(TT) in FComparableTypes then begin

    array1.SetCapacity(ARRAYSIZE_FOR_SORTING);

    // then same initial array for all algs
    for i := 0 to ARRAYSIZE_FOR_SORTING - 1 do begin
      var r := random(32_767);
      array1.AddValue(CreateValue<TT>(r));
    end;

    // we will be sorting array2
    CopyArray(array1, array2);
    var start := GetTickCount;
    array2.BubbleSort(nil);
    TDUnitX.CurrentRunner.Log(Format('BubbleSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

    CopyArray(array1, array2);
    start := GetTickCount;
    array2.SelectionSort(nil);
    TDUnitX.CurrentRunner.Log(Format('SelectingSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

    CopyArray(array1, array2);
    start := GetTickCount;
    array2.InsertSort(nil);
    TDUnitX.CurrentRunner.Log(Format('InsertSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

    CopyArray(array1, array2);
    start := GetTickCount;
    array2.ShakerSort(nil);
    TDUnitX.CurrentRunner.Log(Format('ShakerSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

    CopyArray(array1, array2);
    start := GetTickCount;
    array2.QuickSort(nil);
    TDUnitX.CurrentRunner.Log(Format('QuiuckSort(%d) = %s', [ARRAYSIZE_FOR_SORTING, MillisecToStr(GetTickCount - start)]));
    VerifySorting(array2);

  end;
end;

procedure TGenericArrayTest<TT>.TestCapacity1;
begin
  Assert.AreEqual(0, array1.Capacity);

  array1.SetCapacity(1);
  Assert.AreEqual(1, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(0);
  Assert.AreEqual(0, array1.Capacity);

  array1.SetCapacity(2);
  Assert.AreEqual(2, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(3);
  Assert.AreEqual(3, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(100_000_000);
  Assert.AreEqual(100_000_000, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(0);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(100);
  Assert.AreEqual(100, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  //array1.Zero;
  //Assert.AreEqual(100, array1.Capacity);

  array1.Clear();
  Assert.AreEqual(100, array1.Capacity);

  array1.Hold;
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(0, array1.Count);

  array1.SetCapacity(200);
  array1.ClearMem();
  Assert.AreEqual(0, array1.Capacity);

end;

procedure TGenericArrayTest<TT>.TestCapacity2;
var
  i: Integer;
begin
	for i := 0 to 499 do
		array1.AddValue(CreateValue<TT>(i));

	Assert.AreEqual(556, array1.Capacity);

	array1.Hold();
	Assert.AreEqual(500, array1.Capacity);

	array1.SetCapacity(600);
	Assert.AreEqual(600, array1.Capacity);
	Assert.AreEqual(500, array1.Count);
	array1.SetCapacity(400);
	Assert.AreEqual(400, array1.Capacity);
	Assert.AreEqual(400, array1.Count);
	array1.SetCapacity(0);
	Assert.AreEqual(0, array1.Capacity);
	Assert.AreEqual(0, array1.Count);

end;

function TGenericArrayTest<TT>.Compare1(arr: THArrayG<TT>; i, j: Cardinal): integer;
const
  cmp: IComparer<TT> = nil; // analogue of static variable which saves its value between function calls
begin
  if NOT Assigned(cmp) then
    cmp := TComparer<TT>.Default; // should be called only once

  Result := cmp.Compare(arr[i], arr[j]);
end;

function TGenericArrayTest<TT>.Compare2(Item1, Item2: TT): Integer;
const
  cmp: IComparer<TT> = nil; // analogue of static variable which saves its value between function calls
begin
  if NOT Assigned(cmp) then
    cmp := TComparer<TT>.Default; // should be called only once

  Result := cmp.Compare(Item1, Item2);
end;

function TGenericArrayTest<TT>.IsTTBoolean: Boolean;
var
    TypeInfoPtr: PTypeInfo;
begin
  TypeInfoPtr := TypeInfo(TT);
  Result := (GetTypeKind(TT) = tkEnumeration) AND (TypeInfoPtr.Name = 'Boolean');
end;

procedure TGenericArrayTest<TT>.Setup;
begin
  //FComparableTypes := [tkInteger, tkInt64, tkFloat, tkChar, tkWChar, tkString, tkUString, tkLString, tkWString, tkPointer, tkEnumeration];

	array1 := THArrayG<TT>.Create;
  array2 := THArrayG<TT>.Create;
end;

procedure TGenericArrayTest<TT>.TearDown;
begin
  FreeAndNil(array1);
  FreeAndNil(array2);
//  val := nil;
//  val2 := nil;
end;

procedure TGenericArrayTest<TT>.TestAddClearAdd1;
var
  I: Integer;
  v: TT;
begin
	for I := 5 to 9 do begin
    v := CreateValue<TT>(i);
		array1.InsertValue(array1.Count, v);
    array2.InsertValue(array2.Count, v);
  end;

  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);

	Assert.AreEqual(5, array1.Count);
  Assert.AreEqual(5, array2.Count);
  array1.ClearMem;
  array2.ClearMem;
  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array2.Count);
  Assert.AreEqual(0, array1.Capacity);
  Assert.AreEqual(0, array2.Capacity);

	for I := 15 to 19 do begin
    v := CreateValue<TT>(i);
    array1.InsertValue(array1.Count, v);
		array2.InsertValue(array2.Count, v);
  end;

  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);

	for I := 0 to 4 do
		if GetTypeKind(TT) in FComparableTypes then
        Assert.AreEqual<TT>(CreateValue<TT>(i + 15), array2.GetValue(i))
  	else
    	  Assert.AreEqual<TT>(array2[i], array1.GetValue(i));
end;

procedure TGenericArrayTest<TT>.TestAddClearAdd2;
var
  I: Integer;
  v: TT;
begin

	for I := -9 to -5 do begin
    v := CreateValue<TT>(i);
		array1.InsertValue(array1.Count, v);
    array2.InsertValue(array2.Count, v);
  end;

  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);
	Assert.AreEqual(5, array1.Count);
  Assert.AreEqual(5, array2.Count);
  array1.Clear;
  array2.Clear;
  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);
  Assert.AreEqual(0, array1.Count);
 	Assert.AreEqual(0, array2.Count);

	for I := -19 to -15 do begin
    v := CreateValue<TT>(i);
    array1.InsertValue(array1.Count, v);
		array2.InsertValue(array2.Count, v);
  end;
  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual(8, array2.Capacity);

  for I := 0 to 4 do
  	if GetTypeKind(TT) in FComparableTypes then
  	 //	Assert.isTrue(CreateValue(I - 19) = arr2.GetValue(i))
    //  Assert.isTrue(comparer.Compare(CreateValue(I - 19), arr2[i]) = 0)
      Assert.AreEqual<TT>(CreateValue<TT>(I - 19), array2[i])
  else
      Assert.AreEqual<TT>(array2[i], array1.GetValue(i));
end;

procedure TGenericArrayTest<TT>.TestAddMany;
var
  val: TT;
begin
  val := CreateValue<TT>(999);
  array1.AddMany(val, 0);

  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);

  array1.AddMany(val, 1);

  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.AreEqual<TT>(val, array1[0]);
  array1.Clear;

  array1.AddMany(val, 4);

  Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.AreEqual<TT>(val, array1[0]);
  Assert.AreEqual<TT>(val, array1[1]);
  Assert.AreEqual<TT>(val, array1[2]);
  Assert.AreEqual<TT>(val, array1[3]);
  array1.Clear;

  var val1 := CreateValue<TT>(44);
  var val2 := CreateValue<TT>(105);
  var val3 := CreateValue<TT>(909);
  array1.AddValue(val1);
  array1.AddValue(val2);
  array1.AddValue(val3);
  Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  array1.AddMany(val, 5);

  Assert.AreEqual(8, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual<TT>(val1, array1[0]);
  Assert.AreEqual<TT>(val2, array1[1]);
  Assert.AreEqual<TT>(val3, array1[2]);
  Assert.AreEqual<TT>(val, array1[3]);
  Assert.AreEqual<TT>(val, array1[4]);
  Assert.AreEqual<TT>(val, array1[5]);
  Assert.AreEqual<TT>(val, array1[6]);
  Assert.AreEqual<TT>(val, array1[7]);
  array1.Clear;

end;

procedure TGenericArrayTest<TT>.TestInsertMany;
var
  val: TT;
begin
  val := CreateValue<TT>(999);
  array1.InsertMany(0, val, 0);
  array1.InsertMany(1, val, 0);

  Assert.AreEqual(0, array1.Count);
  Assert.AreEqual(0, array1.Capacity);

  array1.InsertMany(0, val, 1);

  Assert.AreEqual(1, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.AreEqual<TT>(val, array1[0]);
  array1.Clear;

  array1.InsertMany(0, val, 4);

  Assert.AreEqual(4, array1.Count);
  Assert.AreEqual(4, array1.Capacity);
  Assert.AreEqual<TT>(val, array1[0]);
  Assert.AreEqual<TT>(val, array1[1]);
  Assert.AreEqual<TT>(val, array1[2]);
  Assert.AreEqual<TT>(val, array1[3]);
  array1.Clear;

  var val1 := CreateValue<TT>(44);
  var val2 := CreateValue<TT>(105);
  var val3 := CreateValue<TT>(909);
  array1.AddValue(val1);
  array1.AddValue(val2);
  array1.AddValue(val3);
  Assert.AreEqual(3, array1.Count);
  Assert.AreEqual(4, array1.Capacity);

  array1.InsertMany(1, val, 5);

  Assert.AreEqual(8, array1.Count);
  Assert.AreEqual(8, array1.Capacity);
  Assert.AreEqual<TT>(val1, array1[0]);
  Assert.AreEqual<TT>(val, array1[1]);
  Assert.AreEqual<TT>(val, array1[2]);
  Assert.AreEqual<TT>(val, array1[3]);
  Assert.AreEqual<TT>(val, array1[4]);
  Assert.AreEqual<TT>(val, array1[5]);
  Assert.AreEqual<TT>(val2, array1[6]);
  Assert.AreEqual<TT>(val3, array1[7]);

  array1.InsertMany(0, val, 5);
  array1.InsertMany(1, val, 5);
  array1.InsertMany(array1.Count, val, 5);
  Assert.WillRaise(procedure begin array1.InsertMany(array1.Count + 1, val, 5) end, ERangeError, 'insertMany1');

end;

procedure TGenericArrayTest<TT>.TestAddTwoDiff(const AValue1, AValue2 : Integer);
var
  v, vv, vvv: TT;
  cmp: IComparer<TT>;
begin
	Assert.AreNotEqual(AValue1, AValue2, 'AValue1 and AValue2 must be not equal in TestAddTwoDiff()!');

  v   := CreateValue<TT>(AValue1);
  vv  := CreateValue<TT>(AValue2);

  array1.AddValue(v);
  array1.AddValue(vv);

  Assert.AreEqual<TT>(v, array1.GetValue(0));
  Assert.AreEqual<TT>(vv, array1.GetValue(1));
  Assert.AreEqual<TT>(v, array1.GetValue(0));

  Assert.WillRaise(procedure begin array1.GetValue(2) end, ERangeError, 'get2');
  Assert.WillRaise(procedure begin array1.GetValue(3) end, ERangeError, 'get3');
  Assert.WillRaise(procedure begin array1.GetValue(11) end, ERangeError, 'get11');

  Assert.AreEqual(0, array1.IndexOf(v));
  Assert.AreEqual(1, array1.IndexOf(vv));

  Assert.AreEqual(0, array1.IndexOfFrom(v, 0));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 1));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 3));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 555));

  Assert.AreEqual(1, array1.IndexOfFrom(vv, 0));
  Assert.AreEqual(1, array1.IndexOfFrom(vv, 1));

  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 3));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 555));

  cmp := TComparer<TT>.Default;
  if (AValue1 <> 0) AND (AValue2 <> 0) then
	begin
		vvv := CreateValue<TT>(AValue1 + AValue2);
		if (cmp.Compare(vvv, v) <> 0) AND (cmp.Compare(vvv, vv) <> 0) then
			Assert.AreEqual(-1, array1.IndexOf(vvv));

		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 555));

		vvv := CreateValue<TT>(AValue1 - AValue2);
		if (cmp.Compare(vvv, v) <> 0) AND (cmp.Compare(vvv, vv) <> 0) then
			Assert.AreEqual(-1, array1.IndexOf(vvv));

		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 555));
	end;
end;

procedure TGenericArrayTest<TT>.TestAddTwoEqual(const AValue1, AValue2: Integer);
var
    v,vv,vvv: TT;
    s: string absolute v;
    ss: string absolute vv;
   // i: Integer;
    cmp: IComparer<TT>;
begin
	Assert.AreEqual(AValue1, AValue2, 'AValue1 and AValue2 must be equal in TestAddTwoEqual()!');

  v   := CreateValue<TT>(AValue1);
  vv  := CreateValue<TT>(AValue2);

  array1.AddValue(v);
  array1.AddValue(vv);

  if GetTypeKind(TT) in FComparableTypes then begin
    Assert.AreEqual<TT>(v, vv);
    Assert.AreEqual<TT>(v, array1.GetValue(1));
    Assert.AreEqual<TT>(vv, array1.GetValue(0));
    Assert.AreEqual(0, array1.IndexOf(v));
    Assert.AreEqual(0, array1.IndexOf(vv));
  end else begin
    Assert.AreEqual(0, array1.IndexOf(v));
    Assert.AreEqual(1, array1.IndexOf(vv));
  end;

  Assert.AreEqual<TT>(v, array1.GetValue(0));
  Assert.AreEqual<TT>(vv, array1.GetValue(1));

  Assert.WillRaise(procedure begin array1.GetValue(2) end, ERangeError, 'get2');
  Assert.WillRaise(procedure begin array1.GetValue(3) end, ERangeError, 'get3');
  Assert.WillRaise(procedure begin array1.GetValue(66) end, ERangeError, 'get66');


 	if GetTypeKind(TT) in FComparableTypes then begin
    Assert.AreEqual(0, array1.IndexOfFrom(v, 0));
    Assert.AreEqual(1, array1.IndexOfFrom(v, 1));
    Assert.AreEqual(0, array1.IndexOfFrom(vv, 0));
    Assert.AreEqual(1, array1.IndexOfFrom(vv, 1));
  end else begin
    Assert.AreEqual(0, array1.IndexOfFrom(v, 0));
    Assert.AreEqual(-1, array1.IndexOfFrom(v, 1));
	  Assert.AreEqual(1, array1.IndexOfFrom(vv, 0));
    Assert.AreEqual(1, array1.IndexOfFrom(vv, 1));
  end;

  Assert.AreEqual(-1, array1.IndexOfFrom(v, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 3));
  Assert.AreEqual(-1, array1.IndexOfFrom(v, 555));

  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 2));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 3));
  Assert.AreEqual(-1, array1.IndexOfFrom(vv, 555));

  cmp := TComparer<TT>.Default;

	if AValue1 <> 0 then begin
		vvv := CreateValue<TT>(AValue1 + AValue2);
		if (cmp.Compare(vvv, v) <> 0) AND (cmp.Compare(vvv, vv) <> 0) then begin
			Assert.AreEqual(-1, array1.IndexOf(vvv));
  		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
  		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
    end;

		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 555));

		vvv := CreateValue<TT>(AValue1 - AValue2);
		if (cmp.Compare(vvv, v) <> 0) AND (cmp.Compare(vvv, vv) <> 0) then begin
			Assert.AreEqual(-1, array1.IndexOf(vvv));
  		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 0));
	  	Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 1));
    end;

		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 2));
		Assert.AreEqual(-1, array1.IndexOfFrom(vvv, 555));
	end;
end;

procedure TGenericArrayTest<TT>.TestGrow;
var
  i: Integer;
begin
  Assert.AreEqual(0, array1.Capacity);
	array1.AddValue(CreateValue<TT>(11));
	Assert.AreEqual(4, array1.Capacity);
	array1.AddValue(CreateValue<TT>(22));
	array1.AddValue(CreateValue<TT>(33));
	Assert.AreEqual(4, array1.Capacity);
	array1.AddValue(CreateValue<TT>(44));
	Assert.AreEqual(8, array1.Capacity);
	array1.AddValue(CreateValue<TT>(11));
	array1.AddValue(CreateValue<TT>(55));
	array1.AddValue(CreateValue<TT>(66));
	array1.AddValue(CreateValue<TT>(77));
	Assert.AreEqual(12, array1.Capacity);
	array1.AddValue(CreateValue<TT>(22));
	Assert.AreEqual(12, array1.Capacity);
	Assert.AreEqual(9, array1.Count);

	for i := 0 to 2 do
		array1.AddValue(CreateValue<TT>(i+11));

    Assert.AreEqual(28, array1.Capacity);

 	for i := 0 to 15 do
		array1.AddValue(CreateValue<TT>(i+11));

	Assert.AreEqual(28, array1.Count);
	Assert.AreEqual(44, array1.Capacity);

  for i := 0 to 15 do
		array1.AddValue(CreateValue<TT>(i+11));

	Assert.AreEqual(44, array1.Count);
	Assert.AreEqual(60, array1.Capacity);

  for i := 0 to 15 do
		array1.AddValue(CreateValue<TT>(i+11));

	Assert.AreEqual(60, array1.Count);
	Assert.AreEqual(76, array1.Capacity);

  for i := 0 to 15 do
		array1.AddValue(CreateValue<TT>(i+11));

	Assert.AreEqual(76, array1.Count);
 	Assert.AreEqual(95, array1.Capacity);

  for i := 0 to 18 do
		array1.AddValue(CreateValue<TT>(i+11));

	Assert.AreEqual(95, array1.Count);
	Assert.AreEqual(118, array1.Capacity);

	array1.Clear();
	Assert.AreEqual(0, array1.Count);
	Assert.AreEqual(118, array1.Capacity);

	array1.GrowTo(100);
	Assert.AreEqual(0, array1.Count);
	Assert.AreEqual(118, array1.Capacity);
end;


procedure TGenericArrayTest<TT>.TestHGetToken1;
var
   flag: Boolean;
   arr: THArrayG<string>;
begin
  arr := THArrayG<string>.Create;

  for flag := False to true do
  begin
    arr.Clear;
    HGetTokens('', '', flag, arr);
    Assert.AreEqual(0, arr.Count);

    arr.Clear;
    HGetTokens('a', '', flag, arr);
    Assert.AreEqual(1, arr.Count);
    Assert.AreEqual('a', arr[0]);

    arr.Clear;
    HGetTokens('a', ',', flag, arr);
    Assert.AreEqual(1, arr.Count);
    Assert.AreEqual('a', arr[0]);

    arr.Clear;
    HGetTokens('abc', '', flag, arr);
    Assert.AreEqual(1, arr.Count);
    Assert.AreEqual('abc', arr[0]);

    arr.Clear;
    HGetTokens('', '.', flag, arr);
    Assert.AreEqual(0, arr.Count);

    arr.Clear;
    HGetTokens('', '..', flag, arr);
    Assert.AreEqual(0, arr.Count);

    arr.Clear;
    HGetTokens('', 'a', flag, arr);
    Assert.AreEqual(0, arr.Count);

    arr.Clear;
    HGetTokens('a c', ' ', flag, arr);
    Assert.AreEqual(2, arr.Count);
    Assert.AreEqual('a', arr[0]);
    Assert.AreEqual('c', arr[1]);

    arr.Clear;
    HGetTokens('a c', '  ', flag, arr);
    Assert.AreEqual(2, arr.Count);
    Assert.AreEqual('a', arr[0]);
    Assert.AreEqual('c', arr[1]);

    arr.Clear;
    HGetTokens(';a c;', ';', flag, arr);
    Assert.AreEqual(1, arr.Count);
    Assert.AreEqual('a c', arr[0]);

    arr.Clear;
    HGetTokens(';a;c;', ';;', flag, arr);
    Assert.AreEqual(2, arr.Count);
    Assert.AreEqual('a', arr[0]);
    Assert.AreEqual('c', arr[1]);

    arr.Clear;
    HGetTokens('auto;moto velo foto;', ';', flag, arr);
    Assert.AreEqual(2, arr.Count);
    Assert.AreEqual('auto', arr[0]);
    Assert.AreEqual('moto velo foto', arr[1]);

    arr.Clear;
    HGetTokens('auto;moto velo foto;.', ';', flag, arr);
    Assert.AreEqual(3, arr.Count);
    Assert.AreEqual('auto', arr[0]);
    Assert.AreEqual('moto velo foto', arr[1]);
    Assert.AreEqual('.', arr[2]);
  end;

  arr.Clear;
  HGetTokens(';;a;;c;;', ';', False, arr);
  Assert.AreEqual(2, arr.Count);
  Assert.AreEqual('a', arr[0]);
  Assert.AreEqual('c', arr[1]);

  arr.Clear;
  HGetTokens(';;a;;c;;', ';', True, arr);
  Assert.AreEqual(4, arr.Count);
  Assert.AreEqual('a', arr[0]);
  Assert.AreEqual('', arr[1]);
  Assert.AreEqual('c', arr[2]);
  Assert.AreEqual('', arr[3]);

  arr.Free;
end;

initialization

   Randomize; // need for testing sorting algs performance

   TDUnitX.RegisterTestFixture(TGenericArrayTest<String>);
   TDUnitX.RegisterTestFixture(TGenericArrayTest<AnsiString>);
   TDUnitX.RegisterTestFixture(TGenericArrayTest<UnicodeString>);
  // TDUnitX.RegisterTestFixture(TGenericArrayTest<Boolean>);

   TDUnitX.RegisterTestFixture(TGenericArrayTest<TObject>);

   TDUnitX.RegisterTestFixture(TGenericArrayTest<THArrayG<Integer>>);
   //TDUnitX.RegisterTestFixture(TGenericArrayTest<array of Integer>);
end.
