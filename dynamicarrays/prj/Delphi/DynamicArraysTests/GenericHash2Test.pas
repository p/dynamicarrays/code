unit GenericHash2Test;

interface

uses
  DUnitX.TestFramework, Hash2, TestBase;

type
  [TestFixture]
  TGenericHash2Test<I1,I2,V:Constructor> = class(TTestBase)
  private
    hash1: THash2<I1,I2,V>;
  public
    [TearDownFixture]
    procedure TearDownFixture; // this method is called when all tests of this fixture are executed. so we have to free objects from FObjects
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    [TestCase('TestA','1,2')]
    [TestCase('TestB','34, 56')]
    [TestCase('TestC','-1, 0')]
    [TestCase('TestD','0, -3333333')]
    procedure TestEmpty1(AValue1, AValue2: Integer);
    [Test]
    [TestCase('TestA','1,2, 12')]
    [TestCase('TestB','3,4, 34')]
    [TestCase('TestC','1,4, 14')]
    procedure TestSetValue1(const Index1, Index2, Value : Integer);
    [Test]
    [TestCase('TestA','0,2, 54')]
    [TestCase('TestB','3,-4, 666')]
    [TestCase('TestC','100,-4, 444')]
    procedure TestSetValue2(const Index1, Index2, Value : Integer);
    [Test]
    procedure TestDelete1();
    [Test]
    procedure TestDelete2();
    procedure TestIfExists1();

  end;

implementation

uses SysUtils, DynamicArray;

procedure TGenericHash2Test<I1,I2,V>.Setup;
begin
   hash1 := THash2<I1,I2,V>.Create;
end;

procedure TGenericHash2Test<I1,I2,V>.TearDown;
begin
  FreeAndNil(hash1);
end;

procedure TGenericHash2Test<I1,I2,V>.TearDownFixture;
begin
  FreeTObjects;
end;

procedure TGenericHash2Test<I1,I2,V>.TestEmpty1(AValue1, AValue2: Integer);
var val: I1;
begin
  Assert.AreEqual(0, hash1.Count);
  val := CreateValue<I1>(0);
  Assert.AreEqual(0, hash1.Count(val));
  Assert.AreEqual(0, hash1.Count(CreateValue<I1>(AValue1)));
  Assert.AreEqual(0, hash1.Count(CreateValue<I1>(AValue2)));

  hash1.Clear;
  Assert.AreEqual(0, hash1.Count);
  Assert.AreEqual(0, hash1.Count(CreateValue<I1>(0)));
  Assert.AreEqual(0, hash1.Count(CreateValue<I1>(AValue1)));
  Assert.AreEqual(0, hash1.Count(CreateValue<I1>(AValue2)));

  hash1.ClearMem;
  Assert.AreEqual(0, hash1.Count);
  Assert.AreEqual(0, hash1.Count(CreateValue<I1>(0)));
  Assert.AreEqual(0, hash1.Count(CreateValue<I1>(AValue1)));
  Assert.AreEqual(0, hash1.Count(CreateValue<I1>(AValue2)));

  Assert.WillRaise(procedure begin hash1.GetValue(CreateValue<I1>(AValue1), CreateValue<I2>(AValue2)); end, ERangeError, '11111');
  Assert.WillRaise(procedure begin hash1[CreateValue<I1>(AValue1), CreateValue<I2>(AValue2)]; end, ERangeError, '2222222');

  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<I1>(AValue1)); end, nil, '3333333');
  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<I1>(AValue1), CreateValue<I2>(AValue2)); end, nil, '4444444');

  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<I1>(0), CreateValue<I2>(0)); end, nil, 'delete 0 and 0');
  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<I1>(1), CreateValue<I2>(0)); end, nil, 'delete 1 and 0');
  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<I1>(0), CreateValue<I2>(1)); end, nil, 'delete 0 and 1');
  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<I1>(0)); end, nil, 'delete 0');
  Assert.WillNotRaise(procedure begin hash1.Delete(CreateValue<I1>(1)); end, nil, 'delete 1');
end;

procedure TGenericHash2Test<I1, I2, V>.TestIfExists1;
begin

end;

procedure TGenericHash2Test<I1, I2, V>.TestDelete1;
var
  key11, key12 : I1;
  key21, key22, key23:I2;
  val1, val2: V;
begin
  key11 := CreateValue<I1>(5);
  key12 := CreateValue<I1>(8);
  key21 := CreateValue<I2>(244);
  key22 := CreateValue<I2>(344);
  key23 := CreateValue<I2>(444);
  val1  := CreateValue<V>(222);
  val2  := CreateValue<V>(999);

  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key11, key21, val1);
  hash1.SetValue(key11, key22, val1);
  Assert.AreEqual(2, hash1.Count(Key11));
  hash1.SetValue(key11, key23, val1);
  Assert.AreEqual(3, hash1.Count(Key11));

  hash1.SetValue(key12, key21, val2);
  hash1.SetValue(key12, key22, val2);
  Assert.AreEqual(2, hash1.Count(Key12));
  hash1.SetValue(key12, key23, val2);
  Assert.AreEqual(3, hash1.Count(Key12));

  Assert.AreEqual(6, hash1.Count);

  hash1.Delete(key11, key22);
  Assert.AreEqual(5, hash1.Count);
  Assert.AreEqual(2, hash1.Count(Key11));
  Assert.AreEqual(3, hash1.Count(Key12));

  hash1.Delete(key12, key22);
  Assert.AreEqual(4, hash1.Count);
  Assert.AreEqual(2, hash1.Count(Key11));
  Assert.AreEqual(2, hash1.Count(Key12));

  hash1.Delete(key12);
  Assert.AreEqual(2, hash1.Count);
  Assert.AreEqual(2, hash1.Count(Key11));
  Assert.AreEqual(0, hash1.Count(Key12));

  hash1.SetValue(key12, key21, val2);
  hash1.Delete(key11);
  Assert.AreEqual(1, hash1.Count);
  Assert.AreEqual(0, hash1.Count(Key11));
  Assert.AreEqual(1, hash1.Count(Key12));
end;

procedure TGenericHash2Test<I1, I2, V>.TestDelete2;
var
  key11, key12: I1;
  key21, key22: I2;
 // val1, val2: V;
  I: Integer;
  arr1, arr2: THArrayG<I2>;
begin
  key11 := CreateValue<I1>(5);
  key12 := CreateValue<I1>(8);

  arr1 := THArrayG<I2>.Create;
  arr2 := THArrayG<I2>.Create;


  for I := 0 to 10 do begin
    key21 := CreateValue<I2>(i - 244);
    arr1.AddValue(key21);
    hash1[key11, key21] := CreateValue<V>(i + 222);
  end;

  Assert.AreEqual(11, hash1.Count);

  for I := 0 to 10 do begin
    key22 := CreateValue<I2>(i + 333);
    arr2.AddValue(key22);
    hash1[key12, key22] := CreateValue<V>(i + 999);
  end;

  Assert.AreEqual(22, hash1.Count);

  for I := 0 to 10 do begin
    key21 := arr1[i];
    Assert.AreEqual<V>(CreateValue<V>(i + 222), hash1[key11, key21]);
  end;

  for I := 0 to 10 do begin
    key22 := arr2[i];
    Assert.AreEqual<V>(CreateValue<V>(i + 999), hash1[key12, key22]);
  end;

  hash1.Delete(key11, arr1[0]);
  Assert.AreEqual(21, hash1.Count);
  Assert.AreEqual(10, hash1.Count(key11));
  Assert.AreEqual(11, hash1.Count(key12));
  for I := 1 to 10 do begin
    key21 := arr1[i];
    Assert.AreEqual<V>(CreateValue<V>(i + 222), hash1[key11, key21]);
  end;
  hash1.Delete(key11, arr1[10]);
  Assert.AreEqual(20, hash1.Count);
  Assert.AreEqual(9, hash1.Count(key11));
  Assert.AreEqual(11, hash1.Count(key12));
  for I := 1 to 9 do begin
    key21 := arr1[i];
    Assert.AreEqual<V>(CreateValue<V>(i + 222), hash1[key11, key21]);
  end;

  hash1.Delete(key12, arr2[5]);
  Assert.AreEqual(19, hash1.Count);
  Assert.AreEqual(9, hash1.Count(key11));
  Assert.AreEqual(10, hash1.Count(key12));
  for I := 0 to 4 do begin
    key22 := arr2[i];
    Assert.AreEqual<V>(CreateValue<V>(i + 999), hash1[key12, key22]);
  end;
  for I := 6 to 10 do begin
    key22 := arr2[i];
    Assert.AreEqual<V>(CreateValue<V>(i + 999), hash1[key12, key22]);
  end;

  arr1.Free;
  arr2.Free;
end;

procedure TGenericHash2Test<I1,I2,V>.TestSetValue1(const Index1, Index2, Value : Integer);
var
  key1: I1;
  key2:I2;
  val: V;
begin
  key1 := CreateValue<I1>(Index1);
  key2 := CreateValue<I2>(Index2);
  val  := CreateValue<V>(Value);

  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key1, key2, val);
  Assert.AreEqual(1, hash1.Count);

  Assert.AreEqual<V>(val, hash1.GetValue(key1, key2));
  Assert.AreEqual(1, hash1.Count);

  hash1.Delete(key1);
  Assert.AreEqual(0, hash1.Count);
  Assert.WillRaise(procedure begin hash1.GetValue(key1, key2); end, ERangeError, 'ind1 and ind2');

  hash1.SetValue(key1, key2, val);
  Assert.AreEqual(1, hash1.Count);
  hash1.Clear;
  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key1, key2, val);
  Assert.AreEqual(1, hash1.Count);
  hash1.ClearMem;
  Assert.AreEqual(0, hash1.Count);

end;

procedure TGenericHash2Test<I1,I2,V>.TestSetValue2(const Index1, Index2, Value : Integer);
var
  key1: I1;
  key2:I2;
  val1, val2: V;
begin
  key1 := CreateValue<I1>(Index1);
  key2 := CreateValue<I2>(Index2);
  val1  := CreateValue<V>(Value);
  val2  := CreateValue<V>(Value + 1);

  Assert.AreEqual(0, hash1.Count);

  hash1.SetValue(key1, key2, val1);
  Assert.AreEqual(1, hash1.Count);

  hash1.SetValue(key1, key2, val2);
  Assert.AreEqual(1, hash1.Count);

  Assert.AreEqual<V>(val2, hash1.GetValue(key1, key2));
  Assert.AreEqual(1, hash1.Count);

  hash1.Delete(key1);
  Assert.AreEqual(0, hash1.Count);
  Assert.WillRaise(procedure begin hash1.GetValue(key1, key2); end, ERangeError, 'ind1 and ind2');

  hash1.SetValue(key1, key2, val1);
  Assert.AreEqual(1, hash1.Count);
  hash1.Delete(key1, key2);
  Assert.AreEqual(0, hash1.Count);

end;

initialization
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Integer, Cardinal, Int64>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Byte, UInt64, Integer>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Byte, Byte, Integer>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Cardinal, Integer, Byte>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Byte, Cardinal, Integer>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Byte, Char, Pointer>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<AnsiChar, WideChar, Integer>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<TObject, Integer, Char>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<AnsiString, TObject, Char>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<string, string, string>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<string, Char, string>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<string, string, Word>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Single, string, Word>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Extended, AnsiString, AnsiChar>);

  TDUnitX.RegisterTestFixture(TGenericHash2Test<Single, string, Double>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Single, Real, Double>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Real, TObject, Currency>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<Double, Char, Single>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<ShortString, AnsiString, Double>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<TObject, AnsiString, Int64>);
  TDUnitX.RegisterTestFixture(TGenericHash2Test<TObject, THarrayG<Integer>, PCardinal>);


end.
