unit SortedArray;

interface

uses Classes, System.Generics.Defaults, DynamicArray;

type
  THArraySorted<T:constructor> = class(THArrayG<T>)
  private
    FComparer: IComparer<T>;
    
    procedure AddMany(Value: T; Cnt: Cardinal); override;
    function InsertValue(Index: Cardinal; Value: T): Cardinal; override;
    procedure InsertMany(Index: Cardinal; Value: T; Cnt: Cardinal); override;
    procedure SetValue(Index:Cardinal; Value:T); override;
    procedure UpdateMany(Index: Cardinal; Value: T; Cnt: Cardinal); override;
    procedure Swap(Index1, Index2: Cardinal); override;
    procedure BubbleSort(CompareProc: THArrayG<T>.TCompareProc); override;
    procedure SelectionSort(CompareProc: THArrayG<T>.TCompareProc); override;
    procedure InsertSort(CompareProc: THArrayG<T>.TCompareProc2); override;
    procedure QuickSort(CompareProc: THArrayG<T>.TCompareProc); override;
    procedure ShakerSort(CompareProc: THArrayG<T>.TCompareProc2); override;
  protected
    function InternalIndexOfFrom(Value: T; Start: Cardinal): Integer;
  public
    constructor Create; overload;
    constructor Create(Comparer: IComparer<T>); overload;
    function AddValue(Value: T): Cardinal; override;
    function IndexOfFrom(Value: T; Start: Cardinal): Integer; override;
    property Value[Index: Cardinal]: T read GetValue; default;
 end;


implementation

 uses SysUtils;

{ THArraySorted<T> }

function THArraySorted<T>.AddValue(Value: T): Cardinal;
var n: Integer;
begin
  n := InternalIndexOfFrom(Value, 0);
  if n < 0
    then Result := -(n + 1)
    else Result := n;

  inherited InsertValue(Result, Value);
end;

function THArraySorted<T>.IndexOfFrom(Value: T; Start: Cardinal): Integer;
var n: Integer;
begin
  n := InternalIndexOfFrom(Value, Start);

  if n < 0
    then Result := -1
    else Result := n;
end;

function THArraySorted<T>.InternalIndexOfFrom(Value: T; Start: Cardinal): Integer;
var
  left, cnt: Cardinal;
  step, middle: Cardinal;
//  cmp: IComparer<T>;
begin
  if (Start >= FCount) AND (FCount <> 0) then raise ERangeError.Create(SItemNotFound);

  Result := -1;
  if FCount = 0 then exit;

  left := Start;
  cnt := FCount - Start;
  //cmp := TComparer<T>.Default;

  // binary search
  while cnt > 0 do begin
    step := cnt div 2;
    middle := left + step;
    if FComparer.Compare(Value, FValues[middle]) > 0 then begin
      left := middle + 1;
      cnt := cnt - (step + 1);
    end
    else
    if FComparer.Compare(Value, FValues[middle]) < 0 then
      cnt := step
    else begin
      // look for lowest index in case several elements =Value exist in the array
      while (True) do
       if FComparer.Compare(Value, FValues[middle]) = 0 then begin
         if middle > Start
           then Dec(middle)
           else break; // stop if we arrived to Start index
       end else begin
         Inc(middle);
         break;
      end;

      Result := middle;
      exit;
    end;
  end;

  if (left < FCount) AND (Value = FValues[left]) then begin
    Result := left;
    exit;
  end;

  Result := -Integer(left + 1);  // return position (with negative sign) where element is going to be according to sorting
end;

procedure THArraySorted<T>.AddMany(Value: T; Cnt: Cardinal);
begin
  raise EInvalidInsert.Create(SOperationNotSupportedBySortedArray);
end;

procedure THArraySorted<T>.BubbleSort(CompareProc: THArrayG<T>.TCompareProc);
begin
  raise EInvalidOpException.Create(SOperationNotSupportedBySortedArray);
end;

constructor THArraySorted<T>.Create;
begin
  inherited Create;
  FComparer := TComparer<T>.Default;
end;

constructor THArraySorted<T>.Create(Comparer: IComparer<T>);
begin
  inherited Create;
  FComparer := Comparer;  
end;

procedure THArraySorted<T>.SelectionSort(CompareProc : THArrayG<T>.TCompareProc);
begin
  raise EInvalidOpException.Create(SOperationNotSupportedBySortedArray);
end;

procedure THArraySorted<T>.ShakerSort(CompareProc: THArrayG<T>.TCompareProc2);
begin
  raise EInvalidOpException.Create(SOperationNotSupportedBySortedArray);
end;

procedure THArraySorted<T>.QuickSort(CompareProc: THArrayG<T>.TCompareProc);
begin
  raise EInvalidOpException.Create(SOperationNotSupportedBySortedArray);
end;

procedure THArraySorted<T>.InsertSort(CompareProc: THArrayG<T>.TCompareProc2);
begin
  raise EInvalidOpException.Create(SOperationNotSupportedBySortedArray);
end;

procedure THArraySorted<T>.InsertMany(Index: Cardinal; Value: T; Cnt: Cardinal);
begin
  raise EInvalidInsert.Create(SCannotInsertIntoSortedArray);
end;

function THArraySorted<T>.InsertValue(Index: Cardinal; Value: T): Cardinal;
begin
  raise EInvalidInsert.Create(SCannotInsertIntoSortedArray);
end;

procedure THArraySorted<T>.SetValue(Index: Cardinal; Value: T);
begin
  raise EInvalidInsert.Create(SOperationNotSupportedBySortedArray);
end;

procedure THArraySorted<T>.Swap(Index1, Index2: Cardinal);
begin
   raise EInvalidOpException.Create(SOperationNotSupportedBySortedArray);
end;

procedure THArraySorted<T>.UpdateMany(Index: Cardinal; Value: T; Cnt: Cardinal);
begin
   raise EInvalidInsert.Create(SOperationNotSupportedBySortedArray);
end;

end.
