unit Hash2;

interface

uses DynamicArray, Hash;

  type
  THash2<K1:constructor; K2:constructor; V:constructor> = class
  private type
   THashChildType = THash<K2, V>;
   THashValuesType = THash<K1, THashChildType>;
  public type PointerV = THashChildType.PointerV;
  private
   FValues: THashValuesType;
   function GetKey(Index: Cardinal): K1;
   function GetChildHash(Key: K1): THashChildType;
  public
   constructor Create; virtual;
   destructor Destroy; override;
   procedure Clear; virtual;      // Creares hash. Allocated memory does not free (remains allocated).
   procedure ClearMem; virtual;   // Cleares hash. Allocated memory frees too.
   procedure SetValue(Key1: K1; Key2: K2; Value: V); virtual; // creates new record with keys Key1, Key2 and value Value
   function  GetValue(Key1: K1; Key2: K2): V; virtual;         // Gets Value by keys Key1, Key2
   function  GetValuePointer(Key1: K1; Key2: K2): PointerV; virtual;  // returns nil if value does not exist
   //procedure Inc(Index1: I1; Index2: I2; Value: V);     // increases exists/create new record with keys MainIndex, Index
   function  CountTotal: Cardinal; overload;
   function  Count(Key: K1): Cardinal; overload;
   function  Count: Cardinal; overload;
   procedure Delete(Key1: K1; Key2: K2); overload; virtual;
   procedure Delete(Key1: K1); overload; virtual;
   function  IfExist(Key1: K1): Boolean; overload;
   function  IfExist(Key1: K1; Key2: K2): Boolean; overload;

   property Keys[Index: Cardinal]: K1 read GetKey;
   property Value[Key1: K1; Key2: K2]: V read GetValue write SetValue; default;
  end;

implementation

uses SysUtils;

constructor THash2<K1,K2,V>.Create;
begin
  FValues := THashValuesType.Create;
end;

procedure THash2<K1,K2,V>.Clear;
var i: Integer;
begin
  if FValues.Count > 0 then
    for i := 0 to FValues.Count - 1 do begin
      FValues.AValues[i].Clear;
    end;
 FValues.Clear;
end;

procedure THash2<K1,K2,V>.ClearMem;
var i: Integer;
begin
  if FValues.Count > 0 then
    for i := 0 to FValues.Count - 1 do begin
      FValues.AValues[i].Free;
    end;
  FValues.ClearMem;
end;

procedure THash2<K1,K2,V>.Delete(Key1: K1);
begin
  FValues.Delete(Key1);
end;

destructor THash2<K1,K2,V>.Destroy;
var i: Integer;
begin
  for i := 1 to FValues.AValues.Count do FValues.AValues[i - 1].Free;
  FreeAndNil(FValues);
  inherited Destroy;
end;

procedure THash2<K1,K2,V>.Delete(Key1: K1; Key2: K2);
var
  n: Integer;
  h: THashChildType;
begin
  n := FValues.IndexOf(Key1);
  if n = -1 then exit;   // nothing to delete
  h := FValues.AValues[n];
  h.Delete(Key2);
  if h.Count = 0  then begin
    FValues.Delete(Key1);  {TODO: double indexof called, might desrease performance}
    h.free;
  end;
end;

function THash2<K1;K2;V>.GetKey(Index: Cardinal): K1;
begin
  Result := FValues.Keys[Index];
end;

function THash2<K1, K2, V>.Count: Cardinal;
begin
  Result := FValues.Count;
end;

function THash2<K1,K2,V>.Count(Key: K1): Cardinal;
var
  h: THashChildType;
begin
  Result := 0;
  h := GetChildHash(Key);
  if h <> nil then Result := h.Count;
end;

function THash2<K1,K2,V>.CountTotal: Cardinal;
var i: Integer;
begin
  Result := 0;
  if FValues.Count = 0 then exit;

  for i := 0 to FValues.Count - 1 do
    Result := Result + Count(FValues.AIndexes[i]);
end;

function THash2<K1,K2,V>.GetChildHash(Key: K1): THashChildType;
var n: Integer;
begin
  n := FValues.IndexOf(Key);
  if n = -1
    then Result := nil
    else Result := FValues.AValues[n];
end;

function THash2<K1,K2,V>.IfExist(Key1: K1): Boolean;
begin
  Result := FValues.IndexOf(Key1) <> -1;
end;

function THash2<K1,K2,V>.IfExist(Key1: K1; Key2: K2): Boolean;
var h: THashChildType;
begin
  Result := False;
  h := GetChildHash(Key1);
  if h <> nil then begin
    Result := h.IfExist(Key2);
  end;
end;

procedure THash2<K1,K2,V>.SetValue(Key1: K1; Key2: K2; Value: V);
var h: THashChildType;
begin
  h := GetChildHash(Key1);
  if h = nil then begin
    h := THashChildType.Create;
   // h.SetValue(Key2, Value);
    FValues.SetValue(Key1, h);
  end;
  h[Key2] := Value;
end;

{
  procedure THash2<I1;I2;V>.Inc(Index1: I1; Index2: I2; Value: V);
  var c: V;
  begin
   c := GetValue(Index1, Index2);
   SetValue(Index1, Index2, c + Value);
  end;
}

function THash2<K1,K2,V>.GetValue(Key1: K1; Key2: K2): V;
var h: THashChildType;
begin
  h := GetChildHash(Key1);
  if h = nil
    then raise ERangeError.Create(SKeyNotFound)
    else Result := h[Key2];
end;


function THash2<K1, K2, V>.GetValuePointer(Key1: K1; Key2: K2): PointerV;
var h: THashChildType;
begin
  Result := nil;
  h := GetChildHash(Key1);
  if Assigned(h) then Result := h.GetValuePointer(Key2);
end;

end.
