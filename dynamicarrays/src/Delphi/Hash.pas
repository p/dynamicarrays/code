unit Hash;

interface

uses DynamicArray;

resourcestring
  SKeyNotFound = 'Element is not found in Hash!';
  SCannotModifyReadOnly  = 'Cannot modify Read-only hash!';

(***********************************************************)
(*  Hash                                                  *)
(***********************************************************)

 type
  THash<K; V> = class
  public type PointerV = THArrayG<V>.PointerT; // just new name of existing type for better understanding
  Pair = record First: K; Second: V; end;
  protected
   FReadOnly: Boolean;
   FAIndexes: THArrayG<K>;  //TODO: is it better to have THArraySorted here for fast IndexOf?
   FAValues : THArrayG<V>;
   function GetKey(Index: Cardinal): K;
   function GetCount: Cardinal;
  public
   constructor Create; virtual;
//   constructor CreateFromHArrays(IndexHArray:THArraySorted<K>; ValueHArray:THArrayG<V>);
   destructor Destroy; override;
   procedure Clear; virtual;
   procedure ClearMem; virtual;
   procedure SetValue(Key: K; Value: V);
   function GetValue(Key: K): V;
   function GetValuePointer(Key: K): PointerV;
   function GetPair(Index: Cardinal): Pair;
   function IfExist(const Key: K): Boolean;  // check if value with key Key exists in hash
   function IndexOf(const Key: K): Integer;  // returns index of value with key Key or -1 if Key does not exist
   procedure Delete(const Key: K); virtual;  // deletes value with key=Key, does nothing if Key is not found

   property Count: Cardinal read GetCount;
   property Keys[Index: Cardinal]: K read GetKey;
   property Values[Key: K]: V read GetValue write SetValue; default;
   property AIndexes: THArrayG<K> read FAIndexes;
   property AValues: THArrayG<V> read FAValues;
  end;

implementation

uses SysUtils;

constructor THash<K,V>.Create;
begin
  inherited Create;
  FReadOnly := False;
  FAIndexes := THArrayG<K>.Create;
  FAValues  := THArrayG<V>.Create;
end;

{
  constructor THash<K,V>.CreateFromHArrays(IndexHArray:THArraySorted<K>; ValueHArray:THArrayG<V>);
  begin
    inherited Create;
    FAIndexes := IndexHArray;
    FAValues  := ValueHArray;
    FReadOnly := True;
  end;
}

procedure THash<K,V>.Delete(const Key: K);
var n: Integer;
begin
  n := FAIndexes.IndexOf(Key);
  if n >= 0 then begin
    FAIndexes.DeleteValue(Cardinal(n));
    FAValues.DeleteValue(Cardinal(n));
  end;
end;

destructor THash<K,V>.Destroy;
begin
  if not FReadOnly then begin
    FAIndexes.Free;
    FAValues.Free;
  end;

  inherited Destroy;
end;

procedure THash<K;V>.Clear;
begin
  FAIndexes.Clear;
  FAValues.Clear;
end;

procedure THash<K,V>.ClearMem;
begin
  FAIndexes.ClearMem;
  FAValues.ClearMem;
end;

function THash<K,V>.GetCount:Cardinal;
begin
  Assert(FAIndexes.Count = FAValues.Count);
  Result := FAIndexes.Count;
end;

function THash<K,V>.GetKey(Index: Cardinal): K;
begin
  Result := FAIndexes[Index];
end;

function THash<K, V>.GetPair(Index: Cardinal): Pair;
begin
  Result.First := FAIndexes[Index];
  Result.Second := FAValues[Index];
end;

function THash<K,V>.GetValue(Key: K): V;
var n: Integer;
begin
  n := FAIndexes.IndexOf(Key);
  if n >= 0
    then Result := FAValues[Cardinal(n)]
    else raise ERangeError.Create(SKeyNotFound);
end;

function THash<K,V>.GetValuePointer(Key: K): PointerV;
var n: Integer;
begin
  n := FAIndexes.IndexOf(Key);
  if n >= 0
    then Result := FAValues.GetValuePointer(Cardinal(n))
    else Result := nil;
end;

function THash<K,V>.IfExist(const Key: K): Boolean;
begin
  Result := FAIndexes.IndexOf(Key) <> -1;
end;

function THash<K,V>.IndexOf(const Key: K): Integer;
begin
  Result := FAIndexes.IndexOf(Key);
end;

procedure THash<K,V>.SetValue(Key: K; Value: V);
var
  n: Integer;
  ind: Cardinal;
begin
  if FReadOnly then raise ERangeError.Create(SCannotModifyReadOnly);

  n := FAIndexes.IndexOf(Key);
  if n >= 0 then begin
    FAValues[Cardinal(n)] := Value;
  end else begin
   ind := FAIndexes.AddValue(Key);
   FAValues.InsertValue(ind, Value);
  end;
end;


end.